<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('homepage');
});
Route::get('/Jhomepage', function () {
    return view('homepage_junior');
});
Route::get('/Javaibility', function () {
    return view('junior_avaibility');
});
Route::get('/Jform', function () {
    return view('junior_form');
});
Route::get('/Jcompetences', function () {
    return view('junior_knowledges');
});
Route::get('/Jsummary', function () {
    return view('junior_summary');
});
Route::get('/Jconfirmation', function () {
    return view('junior_confirmation');
});
Route::get('/JconfirmationPostulation', function () {
    return view('junior_confirmationPostulation');
});

Route::get('/Sconfirmation', function () {
    return view('senior_confirmation');
});

Route::get('/Sformulaire', function () {
    return view('senior_formulaire');
});

Route::get('/Shomepage', function () {
    return view('senior_homepage');
});

Route::get('/Connexion', function () {
    return view('connexion');
})->name('login');

Route::get('/Contact', function () {
    return view('contact');
});

/*----------------------------SECURIZED ROUTE-------------------------*/
Route::middleware('auth')->group(function () {
    Route::get('/logout', 'AuthController@logout');

    /*----------------------------SENIOR BACKEND-------------------------*/
    Route::get('/SprofilPrivate', function () {
        return view('senior_private_profil');
    });

    /*----------------------------JUNIOR BACKEND-------------------------*/
    Route::get('/Jprivate', function () {
        return view('junior_private');
    });

    /*----------------------------SEC BACKEND-------------------------*/
    Route::get('/Adashboard', function () {
        return view('admin_dashboard');
    });
    Route::get('/Aconnexion', function () {
        return view('admin_connexion');
    });
    Route::get('/AseniorInfo', function () {
        return view('admin_senior_info');
    });
    Route::get('/AnewSenior', function () {
        return view('admin_newSenior');
    });
    Route::get('/AseniorsList', function () {
        return view('admin_seniorsList');
    });
    Route::get('/AassignJunior', function () {
        return view('admin_assignJunior');
    });
    Route::get('/AdetailDemande', function () {
        return view('admin_detail_demande');
    });
    Route::get('/JprivateRapport', function () {
        return view('junior_private_rapport');
    });
});

/*----------------------------API ROUTE-------------------------*/
Route::prefix('/api/v1')->group(function () {

    Route::post('/login', 'AuthController@login');
    Route::get('/junior/getMySkills', 'JuniorController@getMySkills');
    Route::apiResource('/junior', 'JuniorController');
    Route::apiResource('/senior', 'SeniorController');
    Route::get('/senior/findByUserID/{id}', 'SeniorController@findByUserID');

    /*----------------------------SECURIZED ROUTE-------------------------*/
    Route::middleware('auth')->group(function () {
        /*------------------------JUNIOR------------------------------*/
        Route::apiResource('/secretariatmember', 'SecretariatMemberController');
        Route::put('/junior/{id}/addSkills', 'JuniorController@addSkills');
        Route::apiResource('/payment', 'PaymentController');
        Route::apiResource('/intervention', 'InterventionController');
        Route::apiResource('/request', 'RequestController');
        Route::apiResource('/availability', 'AvailabilityController');
        Route::apiResource('/price', 'PriceController');
        Route::apiResource('/skill', 'SkillController');
        Route::get('/junior/getByUserId/{id}', 'JuniorController@getByUserId');
    });
});
