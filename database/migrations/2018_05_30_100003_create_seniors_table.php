<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeniorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seniors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('remainingBalance');
            $table->enum('subscriptionPlan', ['gold', 'silver', 'bronze', 'none']);
            $table->enum('inscriptionStep', ['1', '2', '3', '4', '5']);
            $table->integer('userId')->unsigned();

            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seniors');
    }
}
