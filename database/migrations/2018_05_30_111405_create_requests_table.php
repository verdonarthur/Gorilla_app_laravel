<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table)
        {
            $table->increments('id');
            $table->text('description');
            $table->enum('state', ['created', 'sent', 'accepted', 'not accepted','done']);
            $table->timestamp('startDate')->nullable();
            $table->timestamp('endDate')->nullable();
            $table->integer('seniorId')->unsigned();
            $table->integer('requestId')->nullable()->unsigned();
            $table->integer('juniorId')->nullable()->unsigned();
            $table->enum('type',['simple','multiple']);
            
            $table->foreign('seniorId')->references('id')->on('seniors')->onDelete('cascade');
            $table->foreign('requestId')->references('id')->on('requests')->onDelete('cascade');//Todo: Vérifier que Laravel aime cette notation pour une self référence <3
            $table->foreign('juniorId')->references('id')->on('juniors')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
