<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupResourceTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles',
            function (Blueprint $table) {
            $table->integer('groupId')->unsigned();
            $table->integer('resourceId')->unsigned();
            $table->string('role');
            
            $table->foreign('groupId')->references('id')->on('groups')->onDelete('cascade');
            $table->foreign('resourceId')->references('id')->on('resources')->onDelete('cascade');
            $table->primary(['groupId', 'resourceId']);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}