<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterventionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interventions',
            function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ratingJunior')->nullable();
            $table->string('commentsRatingJunior')->nullable();
            $table->integer('ratingSenior')->nullable();
            $table->string('commentsRatingSenior')->nullable();

            $table->timestamp('from')->nullable();
            $table->timestamp('to')->nullable();
            $table->string('report')->nullable();
            $table->boolean('isComplete');
            $table->integer('interventionTotalPrice');
            $table->boolean('isValidatedBySenior');
            $table->integer('requestId')->unsigned();
            $table->integer('seniorId')->unsigned();
            $table->integer('juniorId')->unsigned();
            $table->integer('interventionPriceId')->unsigned();

            $table->foreign('requestId')->references('id')->on('requests')->onDelete('cascade');
            $table->foreign('seniorId')->references('id')->on('seniors')->onDelete('cascade');
            $table->foreign('juniorId')->references('id')->on('juniors')->onDelete('cascade');
            $table->foreign('interventionPriceId')->references('id')->on('interventionPrices')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interventions');
    }
}