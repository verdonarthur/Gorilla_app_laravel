<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailabilityJuniorsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('availabilityJuniors',
            function (Blueprint $table) {
            $table->increments('id');
            $table->time('from');
            $table->time('to');
            $table->enum('day', ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']);
            $table->integer('addressId')->unsigned();
            $table->integer('juniorId')->unsigned();

            $table->foreign('addressId')->references('id')->on('addresses')->onDelete('cascade');
            $table->foreign('juniorId')->references('id')->on('juniors')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('availabilityJuniors');
    }
}