<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJuniorsSkillsJuniorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('juniors_skillsJuniors', function (Blueprint $table) {
            $table->integer('juniorId')->unsigned();
            $table->integer('skillsJuniorId')->unsigned();
            
            $table->foreign('juniorId')->references('id')->on('juniors')->onDelete('cascade');
            $table->foreign('skillsJuniorId')->references('id')->on('skillsJuniors')->onDelete('cascade');
            $table->primary(['juniorId', 'skillsJuniorId']);
            $table->timestamps();
            $table->softDeletes();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('juniors_skills_juniors');
    }
}
