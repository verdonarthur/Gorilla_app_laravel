<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsSkillsJuniorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests_skillsJuniors', function (Blueprint $table) {
            $table->integer('requestId')->unsigned();
            $table->integer('skillsJuniorId')->unsigned();
            
            $table->foreign('requestId')->references('id')->on('requests')->onDelete('cascade');
            $table->foreign('skillsJuniorId')->references('id')->on('skillsJuniors')->onDelete('cascade');
            $table->primary(['requestId', 'skillsJuniorId']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests_skillsJuniors');
    }
}
