<?php

use Illuminate\Database\Seeder;

class JuniorSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $addressTim = new \App\Address([
            'street' => 'Avenue du Joli Pin',
            'streetNumber' => '22',
            'NPA' => 1001,
            'city' => 'Lausanne',
        ]);


        $userJ1 = new \App\User([
            'email' => "thimothy@example.com",
            'password' => bcrypt("pomme"),
            'lastName' => 'Bongars',
            'firstName' => 'Thimothy',
            'phone' => '+4179 555 66 77',
            'birth' => '1994-02-24',
            'gender' => 'F',
        ]);

        $userJ1->save();
        $userJ1->address()->save($addressTim);

        $tim = new \App\Junior([
            'isEmployed' => false,
            'inscriptionStep' => 1,
        ]);

        $userJ1->junior()->save($tim);
        $userJ1->groups()->save(\App\Group::where('name','Junior')->first());
        
        
        $tim->skillsJuniors()->save(\App\SkillsJunior::find(1));
        $tim->skillsJuniors()->save(\App\SkillsJunior::find(3));
        $tim->skillsJuniors()->save(\App\SkillsJunior::find(5));

        $userJ2 = new \App\User([
            'email' => "userJ2@example.com",
            'password' => bcrypt("pomme"),
            'lastName' => 'Bombom',
            'firstName' => 'Tim',
            'phone' => '+4179 555 66 73',
            'birth' => '1994-02-25',
            'gender' => 'M',
        ]);
        $userJ2->save();

        $tim2 = new \App\Junior([
            'isEmployed' => false,
            'inscriptionStep' => 1,
        ]);

        $userJ2->junior()->save($tim2);
        $userJ2->groups()->save(\App\Group::where('name','Junior')->first());
        
        $tim2->skillsJuniors()->save(\App\SkillsJunior::find(2));
        $tim2->skillsJuniors()->save(\App\SkillsJunior::find(4));
        $tim2->skillsJuniors()->save(\App\SkillsJunior::find(6));
    }
}