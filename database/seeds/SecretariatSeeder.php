<?php

use Illuminate\Database\Seeder;

class SecretariatSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userAdmin1 = new \App\User([
            'email' => "admin@example.com",
            'password' => bcrypt("pomme"),
            'lastName' => 'DiMarino',
            'firstName' => 'Huguette',
            'phone' => '+4179 555 66 88',
            'birth' => '1940-02-24',
            'gender' => 'F',
        ]);

        $userAdmin1->save();

        $admin1 = new \App\SecretariatMember();

        $userAdmin1->secretariatMember()->save($admin1);
        $userAdmin1->groups()->save(\App\Group::where('name','Sec. Mem.')->first());

        $userAdmin1 = new \App\User([
            'email' => "admin2@example.com",
            'password' => bcrypt("pomme"),
            'lastName' => 'DiMarino2',
            'firstName' => 'Huguedeux',
            'phone' => '+4179 555 66 88',
            'birth' => '1940-02-24',
            'gender' => 'M',
        ]);

        $userAdmin1->save();

        $admin1 = new \App\SecretariatMember();

        $userAdmin1->secretariatMember()->save($admin1);
        $userAdmin1->groups()->save(\App\Group::where('name','Sec. Mem.')->first());
    }
}