<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RightsSeeder::class);
        $this->call(SecretariatSeeder::class);
        $this->call(SkillsSeeder::class);
        $this->call(JuniorSeeder::class);
        $this->call(SeniorSeeder::class);
        $this->call(DivSeeder::class);
        $this->call(InterventionSeeder::class);
       
    }
}
