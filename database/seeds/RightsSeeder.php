<?php

use Illuminate\Database\Seeder;

class RightsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tabGroup = array(
            'All'       => array(
                'junior' => 'C',
                'senior' => 'C',
            ),
            'Junior'    => array(
                'junior'       => 'RU',
                'avaiability'  => 'CRUD',
                'senior'       => 'R',
                'skill'        => 'R',
                'request'      => 'RU',
                'intervention' => 'CRU',
            ),
            'Senior'    => array(
                'junior'       => 'R',
                'senior'       => 'RU',
                'request'      => 'CRD',
                'skill'        => 'R',
                'intervention' => 'RU',
                'skill'        => 'R',
                'payment'      => 'R',
                'price'        => 'R',
            ),
            'Sec. Mem.' => array(
                'junior'            => 'CRUD',
                'avaiability'       => 'R',
                'senior'            => 'CRUD',
                'payment'           => 'CR',
                'secretariatmember' => 'R',
                'skill'             => 'CRUD',
                'request'           => 'CRUD',
                'intervention'      => 'R',
                'price'             => 'CRU',
            ),
            'Admin'     => array(
                'junior'            => 'CRUD',
                'avaiability'       => 'CRUD',
                'senior'            => 'CRUD',
                'payment'           => 'CRUD',
                'secretariatmember' => 'CRUD',
                'skill'             => 'CRUD',
                'request'           => 'CRUD',
                'intervention'      => 'CRUD',
                'price'             => 'CRUD',
            ),
        );

        foreach ($tabGroup as $group => $tabRes) {
            $createdGroup = \App\Group::firstOrCreate(['name' => $group]);
            foreach ($tabRes as $resName => $rights) {
                $createdRes
                    = \App\Resource::firstOrCreate(['name' => $resName]);
                $createdGroup->resources()
                    ->attach($createdRes->id, ['role' => $rights]);
            }
        }

    }
}
