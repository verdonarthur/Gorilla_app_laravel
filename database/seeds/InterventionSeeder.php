<?php

use Illuminate\Database\Seeder;

class InterventionSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $interventionPrice1 = new \App\InterventionPrice([
            'priceByHour' => 15.20,
            'description' => 'Prix de l\'heure pour int avec skill A',
            'datePriceSet' => '2017-12-31 12:00:00',
        ]);
        
        $interventionPrice1->save();


        $interventionTimothy = new \App\Intervention([
            'ratingJunior' => 4,
            'commentsRatingJunior' => 'C\'etait bieeen',
            'ratingSenior' => 3,
            'commentsRatingSenior' => 'Lalalaaaa',
            'from' => '2018-10-08 10:05:00',
            'to' => '2018-10-08 10:55:00',
            'report' => 'L\'intervention s est bien passée, le problème a ete resolue en moins d une heure',
            'isComplete' => true,
            'interventionTotalPrice' => 200.00,
            'isValidatedBySenior' => true,
        ]);


        $juniorInt1  = App\Junior::first();
        $seniorInt1  = App\Senior::first();
        $requestInt1 = App\Request::first();

        $interventionPrice1->save();
        $interventionTimothy->interventionPrice()->associate($interventionPrice1);
        $interventionTimothy->junior()->associate($juniorInt1);
        $interventionTimothy->senior()->associate($seniorInt1);
        $interventionTimothy->request()->associate($requestInt1);
        $interventionTimothy->save();

        $interventionPrice2 = new \App\InterventionPrice([
            'priceByHour' => 18.20,
            'description' => 'Prix de l\'heure pour int avec skill B',
            'datePriceSet' => '2018-05-30 12:00:00',
        ]);

        $interventionPrice2->save();

        $interventionPrice3 = new \App\InterventionPrice([
            'priceByHour' => 20.20,
            'description' => 'Prix de l\'heure pour int avec skill C',
            'datePriceSet' => '2016-05-30 10:00:00',
        ]);

        $interventionPrice3->save();
    }
}