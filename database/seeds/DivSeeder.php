<?php

use Illuminate\Database\Seeder;

class DivSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $avail1       = new \App\AvailabilityJunior([
            'from' => '2018-10-08 10:00:00',
            'to' => '2018-10-08 18:00:00',
            'day' => 'saturday',
        ]);
        $juniorAvail1 = App\Junior::first();
        $addressAvail1 = App\Address::first();
        

        $avail1->junior()->associate($juniorAvail1);
        $avail1->address()->associate($addressAvail1);
        $avail1->save();


        $avail2 = new \App\AvailabilityJunior([
            'from' => '2018-10-09 11:00:00',
            'to' => '2018-10-09 19:00:00',
            'day' => 'monday',
        ]);

        $avail2->junior()->associate($juniorAvail1);
        $avail2->address()->associate($addressAvail1);
        $avail2->save();

        $avail3 = new \App\AvailabilityJunior([
            'from' => '2018-10-18 09:00:00',
            'to' => '2018-10-18 20:00:00',
            'day' => 'wednesday',
        ]);

        $avail3->junior()->associate($juniorAvail1);
        $avail3->address()->associate($addressAvail1);
        $avail3->save();
    }
}