<?php

use Illuminate\Database\Seeder;

class SeniorSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Arielle*******************************
        //  Adresse
        $addresse_arielle = new \App\Address([
            'street' => 'Avenue de la tour 7',
            'streetNumber' => 22,
            'NPA' => '1007',
            'city' => 'Lausanne',
        ]);

        //  user
        $user_arielle = new \App\User([
            'email' => "arielle@example.com",
            'password' => bcrypt("pomme"),
            'lastName' => 'Arielle',
            'firstName' => 'Tomballe',
            'phone' => '+4179 555 66 77',
            'birth' => '1914-02-24',
            'gender' => 'F',
        ]);
        $user_arielle->save();
        $user_arielle->address()->save($addresse_arielle);
        $user_arielle->groups()->save(\App\Group::where('name','Senior')->first());



        //  Senior
        $arielle          = new \App\Senior([
            'remainingBalance' => 150500,
            'subscriptionPlan' => 'gold',
        ]);
        $user_arielle->senior()->save($arielle);
        //  Paiement
        $paiement_arielle = new \App\Payment([
            'amount' => 1000000,
            'date' => '2018-05-31',
            'isAutomated' => false,
        ]);
        $arielle->payments()->save($paiement_arielle);
        //  Requête

        $requete_arielle = new \App\Request([
            'description' => "SVP aidez moi j'ai plus internet",
            'state' => 'sent',
            'startDate' => '2018-06-15 12:00:00',
            'endDate' => '2018-06-15 14:00:00',
            'type' => 'simple',
        ]);
        $arielle->requests()->save($requete_arielle);

        $requete_arielle->skillsJuniors()->save(\App\SkillsJunior::find(4));
        $requete_arielle->junior()->associate(\App\Junior::find(1));        
        $requete_arielle->save();
        //marianne*******************************
        //  Adresse
        $addresse_marianne = new \App\Address([
            'street' => 'Avenue de la tour',
            'streetNumber' => 14,
            'NPA' => '1010',
            'city' => 'Lausanne',
        ]);

        //  user
        $user_marianne = new \App\User([
            'email' => "marianne@example.com",
            'password' => bcrypt("pomme"),
            'lastName' => 'marianne',
            'firstName' => 'Flow',
            'phone' => '+4179 555 66 77',
            'birth' => '1945-02-24',
            'gender' => 'F',
        ]);
        $user_marianne->save();
        $user_marianne->address()->save($addresse_marianne);
        $user_marianne->groups()->save(\App\Group::where('name','Senior')->first());

        //  Senior
        $marianne          = new \App\Senior([
            'remainingBalance' => 100,
            'subscriptionPlan' => 'none',
        ]);
        $user_marianne->senior()->save($marianne);
        //  Paiement
        $paiement_marianne = new \App\Payment([
            'amount' => 100,
            'date' => '2018-05-31',
            'isAutomated' => false,
        ]);
        $marianne->payments()->save($paiement_marianne);
        //  Requête

        $requete_marianne = new \App\Request([
            'description' => "jaime le wifi mais jarrive pas a aller dessus",
            'state' => 'sent',
            'startDate' => '2018-06-15 00:00:00',
            'endDate' => '2018-06-15 08:00:00',
            'type' => 'simple',
        ]);
        $marianne->requests()->save($requete_marianne);
        
        $requete_marianne->skillsJuniors()->save(\App\SkillsJunior::find(1));
        $requete_marianne->junior()->associate(\App\Junior::find(1));        
        $requete_marianne->save();

        //claude*******************************
        //  Adresse
        $addresse_claude = new \App\Address([
            'street' => 'Avenue de la tour',
            'streetNumber' => 14,
            'NPA' => '1010',
            'city' => 'Lausanne',
        ]);

        //  user
        $user_claude     = new \App\User([
            'email' => "claude@example.com",
            'password' => bcrypt("pomme"),
            'lastName' => 'claude',
            'firstName' => 'Flow',
            'phone' => '+4179 555 66 77',
            'birth' => '1945-02-24',
            'gender' => 'M',
        ]);

        $user_claude->save();
        $user_claude->address()->save($addresse_claude);
        $user_claude->groups()->save(\App\Group::where('name','Senior')->first());
        
        //  Senior
        $claude          = new \App\Senior([
            'remainingBalance' => 10000,
            'subscriptionPlan' => 'none',
        ]);
        $user_claude->senior()->save($claude);
        //  Paiement
        $paiement_claude = new \App\Payment([
            'amount' => 100,
            'date' => '2018-05-31',
            'isAutomated' => false,
        ]);
        $claude->payments()->save($paiement_claude);
        //  Requête

        $requete_claude = new \App\Request([
            'description' => "jaime le wifi mais jarrive pas a aller dessus",
            'state' => 'sent',
            'startDate' => '2018-09-15 00:00:00',
            'endDate' => '2018-09-16 08:00:00',
            'type' => 'simple',
        ]);
        $claude->requests()->save($requete_claude);

        $requete_claude->skillsJuniors()->save(\App\SkillsJunior::find(2));
        $requete_claude->junior()->associate(\App\Junior::find(2));        
        $requete_claude->save();
    }
}