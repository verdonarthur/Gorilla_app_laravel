<?php

use Illuminate\Database\Seeder;

class SkillsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\SkillsJunior::create(['name'=>'Basic IT']);
        \App\SkillsJunior::create(['name'=>'Backup']);
        \App\SkillsJunior::create(['name'=>'Mac OS']);
        \App\SkillsJunior::create(['name'=>'Windows']);
        \App\SkillsJunior::create(['name'=>'Linux']);
        \App\SkillsJunior::create(['name'=>'Microsoft Office']);
        \App\SkillsJunior::create(['name'=>'Web']);
    }
}