let mix = require('laravel-mix');
mix.webpackConfig({
    module: {
        rules: [
            {
                test: /\.handlebars?$/,
                loader: 'handlebars-loader'
            }
        ]
    }
});
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
    'resources/assets/js/jsonStorage.js',
    'resources/assets/js/junior_private.js',
    'resources/assets/js/login.js',
    'resources/assets/js/register',
    'resources/assets/js/header.js',
    'resources/assets/js/junior_registration.js',
    'resources/assets/js/route.js',

], 'public/js/all.js')
    .js([
        'resources/assets/js/backendSec.js',
    ],'public/js/backendSec.js')
    .styles([
        /*'resources/assets/css/junior_accueil.css',
        'resources/assets/css/junior_confirmation.css',
        'resources/assets/css/junior_connaissances.css',
        'resources/assets/css/junior_connexion.css',
        'resources/assets/css/junior_profil.css',
        'resources/assets/css/senior_confirmation.css',
        'resources/assets/css/senior_formulaire.css',
        'resources/assets/css/senior.css',
        'resources/assets/css/homepage.css',
        'resources/assets/css/junior_disponibilite.css',
        'resources/assets/css/layout_public.css',
        'resources/assets/css/progress_bar.css',
        'resources/assets/css/junior_private.css',
        'resources/assets/css/junior_senregistrer.css',
        'resources/assets/css/contact.css',
        'resources/assets/css/junior_summary.css'*/
        'resources/assets/css/main.css'
    ], 'public/css/all.css')

    .styles([
        'resources/assets/css/senior_private_profil.css'
    ], 'public/css/privateSenior.css')

    .styles([
        'resources/assets/css/junior_private_profile.css',

    ], 'public/css/privateJunior.css')

    .styles([
        'resources/assets/css/admin_dashboard.css',
        'resources/assets/css/admin_connexion.css',
        'resources/assets/css/admin_seniors_infos.css',
        'resources/assets/css/admin.css',
    ], 'public/css/admin.css');
