---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->

# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/gorilla/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_8c0e48cd8efa861b308fc45872ff0837 -->
## Login a user

> Example request:

```bash
curl -X POST "http://localhost/gorilla/api/v1/login" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/login",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/login`


<!-- END_8c0e48cd8efa861b308fc45872ff0837 -->

<!-- START_2255b94c982689ebdb4d34cb469084b0 -->
## Display a listing of the junior

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/junior" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/junior",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/junior`

`HEAD api/v1/junior`


<!-- END_2255b94c982689ebdb4d34cb469084b0 -->

<!-- START_e82a8a5a20d1e50d5dcbd70472174923 -->
## Store a newly created Junior in storage.

contains all the datas from the client
Those datas must contain:
email, lastName, firstName, phone, password, birth, gender, 'street',
'NPA', 'city', 'streetNumber',

> Example request:

```bash
curl -X POST "http://localhost/gorilla/api/v1/junior" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/junior",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/junior`


<!-- END_e82a8a5a20d1e50d5dcbd70472174923 -->

<!-- START_9bd84e5a3780c5de83e490e5eac0a1f2 -->
## Display the specified Junior.

A Junior can only show its own infos.

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/junior/{junior}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/junior/{junior}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/junior/{junior}`

`HEAD api/v1/junior/{junior}`


<!-- END_9bd84e5a3780c5de83e490e5eac0a1f2 -->

<!-- START_6d6bfd80ff929f322deeeab979dc1310 -->
## update a Junior in storage and its linked user.

contains all the datas from the client and the id of the Junior to update
Those datas must contain:
email, lastName, firstName, phone, password, birth, gender, street,
'NPA', 'city', 'streetNumber'

> Example request:

```bash
curl -X PUT "http://localhost/gorilla/api/v1/junior/{junior}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/junior/{junior}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/junior/{junior}`

`PATCH api/v1/junior/{junior}`


<!-- END_6d6bfd80ff929f322deeeab979dc1310 -->

<!-- START_e36df92b312d7761ed5545b58df17300 -->
## Remove the specified Junior from storage, and its linked user and address.

> Example request:

```bash
curl -X DELETE "http://localhost/gorilla/api/v1/junior/{junior}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/junior/{junior}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/junior/{junior}`


<!-- END_e36df92b312d7761ed5545b58df17300 -->

<!-- START_97cec36059bbf7b717bafbfb40513ca2 -->
## return all the senior in DB in JSON format with the user information

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/senior" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/senior",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/senior`

`HEAD api/v1/senior`


<!-- END_97cec36059bbf7b717bafbfb40513ca2 -->

<!-- START_d6cd2bf52109496d086eb05e01c69800 -->
## Store a new senior in the DB.

The needed field are 'email', 'lastName', 'firstName',
'phone', 'password', 'birth', 'gender', 'street', 'streetNumber', 'NPA', 'city'

The created senior is return in a JSON format.

> Example request:

```bash
curl -X POST "http://localhost/gorilla/api/v1/senior" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/senior",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/senior`


<!-- END_d6cd2bf52109496d086eb05e01c69800 -->

<!-- START_6b77c9fd7b35cf8738fd2a606524c085 -->
## give a senior in JSON format with the user information by the ID asked

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/senior/{senior}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/senior/{senior}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/senior/{senior}`

`HEAD api/v1/senior/{senior}`


<!-- END_6b77c9fd7b35cf8738fd2a606524c085 -->

<!-- START_b13460a892af668be61b77f70f203520 -->
## Update a senior in the DB.

The needed field are 'email', 'lastName', 'firstName',
'phone', 'password', 'birth', 'gender', 'street', 'streetNumber', 'NPA', 'city'

The updated senior is return in a JSON format.

> Example request:

```bash
curl -X PUT "http://localhost/gorilla/api/v1/senior/{senior}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/senior/{senior}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/senior/{senior}`

`PATCH api/v1/senior/{senior}`


<!-- END_b13460a892af668be61b77f70f203520 -->

<!-- START_d83d6af79af8bd01174ee0f4c0aed787 -->
## Delete a senior by his ID

> Example request:

```bash
curl -X DELETE "http://localhost/gorilla/api/v1/senior/{senior}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/senior/{senior}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/senior/{senior}`


<!-- END_d83d6af79af8bd01174ee0f4c0aed787 -->

<!-- START_bb91789748d3d79a75f88778285d7f24 -->
## Display a listing of the secretariatMembers

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/secretariatmember" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/secretariatmember",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/secretariatmember`

`HEAD api/v1/secretariatmember`


<!-- END_bb91789748d3d79a75f88778285d7f24 -->

<!-- START_192a35780fcc07f56764ce66ef55768e -->
## Store a newly created secretariatMember in storage.

> Example request:

```bash
curl -X POST "http://localhost/gorilla/api/v1/secretariatmember" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/secretariatmember",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/secretariatmember`


<!-- END_192a35780fcc07f56764ce66ef55768e -->

<!-- START_8bcef284d3251ef38041148226e5707f -->
## Display the specified secretariatMember.

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/secretariatmember/{secretariatmember}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/secretariatmember/{secretariatmember}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/secretariatmember/{secretariatmember}`

`HEAD api/v1/secretariatmember/{secretariatmember}`


<!-- END_8bcef284d3251ef38041148226e5707f -->

<!-- START_88e084703b7b76e6f8dde7e6dd66e153 -->
## update a secretariatMember in storage and its linked user.

> Example request:

```bash
curl -X PUT "http://localhost/gorilla/api/v1/secretariatmember/{secretariatmember}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/secretariatmember/{secretariatmember}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/secretariatmember/{secretariatmember}`

`PATCH api/v1/secretariatmember/{secretariatmember}`


<!-- END_88e084703b7b76e6f8dde7e6dd66e153 -->

<!-- START_950c86ef29cc9a479336ebfb1e649902 -->
## Remove the specified secretariatMember from storage, and its linked user.

> Example request:

```bash
curl -X DELETE "http://localhost/gorilla/api/v1/secretariatmember/{secretariatmember}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/secretariatmember/{secretariatmember}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/secretariatmember/{secretariatmember}`


<!-- END_950c86ef29cc9a479336ebfb1e649902 -->

<!-- START_818c5511e4475df18fd3a92cd66a71b8 -->
## return all the payment in DB in JSON format with the linked senior

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/payment" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/payment",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/payment`

`HEAD api/v1/payment`


<!-- END_818c5511e4475df18fd3a92cd66a71b8 -->

<!-- START_37a89876598453b357de96d828842c6d -->
## Store a new payment in the DB.

The needed field are amount, isAutomated, seniorId

The created payment is return in a JSON format with linked senior

> Example request:

```bash
curl -X POST "http://localhost/gorilla/api/v1/payment" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/payment",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/payment`


<!-- END_37a89876598453b357de96d828842c6d -->

<!-- START_399cdd5ac0fd904f421350db276df4df -->
## give a payment in JSON format with the senior
information by the ID asked

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/payment/{payment}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/payment/{payment}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/payment/{payment}`

`HEAD api/v1/payment/{payment}`


<!-- END_399cdd5ac0fd904f421350db276df4df -->

<!-- START_e71797b25d7be97ec0e17951b5a9e7e7 -->
## Update a payment in the DB.

The needed field are amount, isAutomated, seniorId

The created intervention is return in a JSON format.

> Example request:

```bash
curl -X PUT "http://localhost/gorilla/api/v1/payment/{payment}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/payment/{payment}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/payment/{payment}`

`PATCH api/v1/payment/{payment}`


<!-- END_e71797b25d7be97ec0e17951b5a9e7e7 -->

<!-- START_cc3788c39fd162b87ab2ff8bb18f781f -->
## Delete a payment by his ID

> Example request:

```bash
curl -X DELETE "http://localhost/gorilla/api/v1/payment/{payment}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/payment/{payment}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/payment/{payment}`


<!-- END_cc3788c39fd162b87ab2ff8bb18f781f -->

<!-- START_cca21227985a90c3c8830e3bc6158d2e -->
## return all the intervention in DB in JSON format with the linked junior, senior, request
and interventionPrice

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/intervention" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/intervention",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/intervention`

`HEAD api/v1/intervention`


<!-- END_cca21227985a90c3c8830e3bc6158d2e -->

<!-- START_44573c2cf919f2a1ed52d90f5be838aa -->
## Store a new intervention in the DB.

The needed field are 'from', 'to', 'isComplete',
'seniorId', 'juniorId', 'requestId'

The optional field are ratingJunior, ratingSenior,
commentsRatingJunior, commentsRatingSenior, report

The created intervention is return in a JSON format.

> Example request:

```bash
curl -X POST "http://localhost/gorilla/api/v1/intervention" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/intervention",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/intervention`


<!-- END_44573c2cf919f2a1ed52d90f5be838aa -->

<!-- START_58d3c49e76668344dbebb466858438fc -->
## give an intervention in JSON format with the with the linked junior, senior, request
and interventionPrice by the ID asked

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/intervention/{intervention}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/intervention/{intervention}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/intervention/{intervention}`

`HEAD api/v1/intervention/{intervention}`


<!-- END_58d3c49e76668344dbebb466858438fc -->

<!-- START_b0a4afe5cccfe6fe2bd9f24ff1b3a5ce -->
## Update an intervention in the DB.

The needed field are 'from', 'to', 'isComplete',
'isValidatedBySenior', 'seniorId', 'juniorId', 'requestId'

The optional field are ratingJunior, ratingSenior,
commentsRatingJunior, commentsRatingSenior, report

The created intervention is return in a JSON format.

> Example request:

```bash
curl -X PUT "http://localhost/gorilla/api/v1/intervention/{intervention}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/intervention/{intervention}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/intervention/{intervention}`

`PATCH api/v1/intervention/{intervention}`


<!-- END_b0a4afe5cccfe6fe2bd9f24ff1b3a5ce -->

<!-- START_96cbcef52af19aeb038c2167dc35d02b -->
## Delete an intervention by his ID

> Example request:

```bash
curl -X DELETE "http://localhost/gorilla/api/v1/intervention/{intervention}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/intervention/{intervention}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/intervention/{intervention}`


<!-- END_96cbcef52af19aeb038c2167dc35d02b -->

<!-- START_ed289ee46160182afadd2c0c0e86b46e -->
## listing the requests
juniors and seniors can only access to their own requests

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/request" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/request",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/request`

`HEAD api/v1/request`


<!-- END_ed289ee46160182afadd2c0c0e86b46e -->

<!-- START_2f482a5c49bae9d44ea37f51e7548b18 -->
## Store a new Request in the DB.

The needed field are 'description', 'state', 'startDate', 'endDate', 'type', 'seniorId'
optionnal: 'juniorId','requestId'

> Example request:

```bash
curl -X POST "http://localhost/gorilla/api/v1/request" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/request",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/request`


<!-- END_2f482a5c49bae9d44ea37f51e7548b18 -->

<!-- START_88c0940b5485831fe60a033213f2459a -->
## retrieve a request
juniors and seniors can only access to their own requests

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/request/{request}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/request/{request}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/request/{request}`

`HEAD api/v1/request/{request}`


<!-- END_88c0940b5485831fe60a033213f2459a -->

<!-- START_680ba6b5cb09ae411e2a938e606a0234 -->
## Store a new Request in the DB.

The needed field are : 'description', 'state', 'startDate', 'endDate', 'type', 'seniorId'

> Example request:

```bash
curl -X PUT "http://localhost/gorilla/api/v1/request/{request}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/request/{request}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/request/{request}`

`PATCH api/v1/request/{request}`


<!-- END_680ba6b5cb09ae411e2a938e606a0234 -->

<!-- START_808e6cefc9a878d50fa8543f2734e50e -->
## soft deleting a request

> Example request:

```bash
curl -X DELETE "http://localhost/gorilla/api/v1/request/{request}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/request/{request}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/request/{request}`


<!-- END_808e6cefc9a878d50fa8543f2734e50e -->

<!-- START_f458b05ea52df9a743a967092508f873 -->
## listing of the availabilities
juniors can only consult their own availabilities

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/availability" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/availability",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/availability`

`HEAD api/v1/availability`


<!-- END_f458b05ea52df9a743a967092508f873 -->

<!-- START_06d350adccd6c26165081b7245914baf -->
## inserting a new availability

> Example request:

```bash
curl -X POST "http://localhost/gorilla/api/v1/availability" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/availability",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/availability`


<!-- END_06d350adccd6c26165081b7245914baf -->

<!-- START_3acea4c7850004aaeffab6d15e848997 -->
## retrieving an availability

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/availability/{availability}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/availability/{availability}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/availability/{availability}`

`HEAD api/v1/availability/{availability}`


<!-- END_3acea4c7850004aaeffab6d15e848997 -->

<!-- START_42b93e0b692cb36885512cf417b42d36 -->
## updating an availability

> Example request:

```bash
curl -X PUT "http://localhost/gorilla/api/v1/availability/{availability}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/availability/{availability}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/availability/{availability}`

`PATCH api/v1/availability/{availability}`


<!-- END_42b93e0b692cb36885512cf417b42d36 -->

<!-- START_50a60e72e99175f6fbec7f87a405ba93 -->
## soft deleting an availability

> Example request:

```bash
curl -X DELETE "http://localhost/gorilla/api/v1/availability/{availability}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/availability/{availability}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/availability/{availability}`


<!-- END_50a60e72e99175f6fbec7f87a405ba93 -->

<!-- START_b1fd7be27c44b56795b72063712b4176 -->
## Display a listing of all the intervention Prices

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/price" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/price",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/price`

`HEAD api/v1/price`


<!-- END_b1fd7be27c44b56795b72063712b4176 -->

<!-- START_f07220d63ddc844212a2e0bda7ad916a -->
## Store a newly created interventionPrice in storage.

contains all the datas from the client
Those datas must contain:
priceByHour, description

> Example request:

```bash
curl -X POST "http://localhost/gorilla/api/v1/price" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/price",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/price`


<!-- END_f07220d63ddc844212a2e0bda7ad916a -->

<!-- START_bfa4716e9249aa695ea4e342e0d465d2 -->
## Display the specified interventionPrice.

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/price/{price}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/price/{price}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/price/{price}`

`HEAD api/v1/price/{price}`


<!-- END_bfa4716e9249aa695ea4e342e0d465d2 -->

<!-- START_4318d629a4ddbab9890f5efb23e67da3 -->
## contains all the datas from the client, and the id of the price to update

> Example request:

```bash
curl -X PUT "http://localhost/gorilla/api/v1/price/{price}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/price/{price}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/price/{price}`

`PATCH api/v1/price/{price}`


<!-- END_4318d629a4ddbab9890f5efb23e67da3 -->

<!-- START_0cca86b5f7e86e4bdb15639c2bcb2e02 -->
## the id of the price to delete

> Example request:

```bash
curl -X DELETE "http://localhost/gorilla/api/v1/price/{price}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/price/{price}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/price/{price}`


<!-- END_0cca86b5f7e86e4bdb15639c2bcb2e02 -->

<!-- START_81a79e6f63e4f7cb6f954e586411638e -->
## listing the skills

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/skill" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/skill",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/skill`

`HEAD api/v1/skill`


<!-- END_81a79e6f63e4f7cb6f954e586411638e -->

<!-- START_744214951769f083f5e0806080093266 -->
## inserting a new skill

> Example request:

```bash
curl -X POST "http://localhost/gorilla/api/v1/skill" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/skill",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/skill`


<!-- END_744214951769f083f5e0806080093266 -->

<!-- START_f38897e12b6a3d6a00804c9111f41975 -->
## retriving a skill

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/skill/{skill}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/skill/{skill}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/skill/{skill}`

`HEAD api/v1/skill/{skill}`


<!-- END_f38897e12b6a3d6a00804c9111f41975 -->

<!-- START_6b82a592c165279b1d407b0fc07c2402 -->
## update a skill in the DB

> Example request:

```bash
curl -X PUT "http://localhost/gorilla/api/v1/skill/{skill}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/skill/{skill}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/skill/{skill}`

`PATCH api/v1/skill/{skill}`


<!-- END_6b82a592c165279b1d407b0fc07c2402 -->

<!-- START_5ab482c23a47c66905d68907ae7f2982 -->
## id of the skill
soft deleting a skill

> Example request:

```bash
curl -X DELETE "http://localhost/gorilla/api/v1/skill/{skill}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/skill/{skill}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/skill/{skill}`


<!-- END_5ab482c23a47c66905d68907ae7f2982 -->

<!-- START_394d402f1e299237fa88b4466e18226b -->
## logout the user currently logged

> Example request:

```bash
curl -X GET "http://localhost/gorilla/api/v1/logout" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/gorilla/api/v1/logout",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/logout`

`HEAD api/v1/logout`


<!-- END_394d402f1e299237fa88b4466e18226b -->

