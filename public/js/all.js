/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 16);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var WEBSITE_URL = "http://localhost/gorilla/";

exports.WEBSITE_URL = WEBSITE_URL;
exports.URL_API = WEBSITE_URL + "api/v1/";
exports.JUNIOR_DATA = WEBSITE_URL + "api/v1/junior";
exports.REQUEST_DATA = WEBSITE_URL + "api/v1/request";
exports.SKILL_DATA = WEBSITE_URL + "api/v1/skill";
exports.SENIOR_BACKEND = WEBSITE_URL + "SprofilPrivate";
exports.JUNIOR_BACKEND = WEBSITE_URL + "Jprivate";
exports.JUNIOR_BACKEND = WEBSITE_URL + "SprofilPrivate";
exports.AVAILABILITY_DATA = WEBSITE_URL + "api/v1/availability";

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

var CONST = __webpack_require__(0);
var JUNIOR_DATA = CONST.JUNIOR_DATA;
var REQUEST_DATA = CONST.REQUEST_DATA;
var AVAILABILITY_DATA = CONST.AVAILABILITY_DATA;
var SKILL_DATA = CONST.SKILL_DATA;

exports.loadJuniorRegistration = function (WEBSITE_URL, URL_API) {
    console.log("registrationjunior");

    $("#birthJ").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });

    //$("#phoneJ").intlTelInput();

    $('#registrationFormJ').on('submit', function (event) {

        event.preventDefault();
        var form_data = $(this).serialize();
        console.log(form_data);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhrFields: {
                withCredentials: true
            }
        });
        $.ajax({
            method: "POST",
            url: $(this).attr('action'),
            data: {
                gender: $("#genderJ option:selected").val(),
                lastName: $('#lastNameJ').val(),
                firstName: $('#firstNameJ').val(),
                birth: $('#birthJ').val(),
                street: $('#streetJ').val(),
                streetNumber: $('#streetNumberJ').val(),
                NPA: $('#NPAJ').val(),
                city: $('#cityJ').val(),
                phone: $('#phoneJ').val(),
                email: $('#emailJ').val(),
                password: $('#passwordJ').val()
            },
            dataType: "json",
            success: function success(data) {
                console.log(data);
            },
            error: function error(data) {
                console.log(data);
            }

        }).done(function (data) {

            console.log($('#emailJ').val());
            console.log($('#passwordJ').val());
            var login = __webpack_require__(8);
            var logResult = login.login($('#emailJ').val(), $('#passwordJ').val(), URL_API);
            logResult.done(function (data) {
                var ls = __webpack_require__(6);
                localUserStorage = new ls.storageJSON('USER');
                var user = data;
                localUserStorage.setItem('userID', user.id);
                var urlGetByID = URL_API + 'junior/getByUserId/' + user.id;
                var getJuniorId = $.getJSON(urlGetByID, function (data) {
                    var juniorID = data.id;
                    localUserStorage.setItem('juniorId', juniorID);
                    localUserStorage.setItem('groupName', user.groups[0].name);
                    localUserStorage.setItem('address', user.address);
                    window.location.href = WEBSITE_URL + "Jconfirmation";
                });
            });
        }).fail(function (data) {

            console.log(data);
            $.each(data.responseJSON.error, function (key, value) {

                console.log(value[0]);
                console.log(key);
                var input = '#registrationForm input[name=' + key + ']';
                console.log(input);
                $(input + '+span').text(value["0"]).css('font-size', '0.9rem');
            });
        });
    });
};

exports.loadJuniorAvailability = function (WEBSITE_URL, URL_API) {
    console.log("availabilityjunior");
    $(".fa-times").click(function () {
        $(this).toggleClass("fa-times");
        $(this).toggleClass("fa-check");
    });
    $(".fa-check").click(function () {
        $(this).toggleClass("fa-check");
        $(this).toggleClass("fa-times");
    });

    var infoJunior = JSON.parse(localStorage.getItem('USER'));
    console.log(infoJunior);
    var juniorID = infoJunior.juniorId;
    var addressId = infoJunior.address.id;
    console.log(juniorID);
    $("#sendAvailabilities").click(function () {
        var checkedDispo = $(".fa-check");
        var availabilities = {};
        checkedDispo.each(function (index) {
            var splitValue = $(this).attr("title").split(" ");
            var day = splitValue[0];
            var hour = splitValue[1];
            var to;
            var from;

            if (hour === "am") {
                from = "08:00:00";
                to = "12:00:00";
            } else {
                from = "13:00:00";
                to = "18:00:00";
            }

            availabilities[index] = {
                from: from,
                to: to,
                day: day,
                juniorId: juniorID,
                addressId: addressId
            };
        });

        var postAllAvail = $.each(availabilities, function (index) {
            console.log($(this)[0]);
            console.log($(this)[0].day);
            $.ajax({
                method: "POST",
                url: URL_API + 'availability',
                dataType: "json",
                data: {
                    day: $(this)[0].day,
                    from: $(this)[0].from,
                    to: $(this)[0].to,
                    juniorId: juniorID,
                    addressId: addressId
                },
                success: function success(data) {
                    console.log(juniorID + " updated");
                },
                error: function error(data) {
                    console.log(data);
                }
            });
        });
        $.when(postAllAvail).done(function (data) {
            window.location.href = WEBSITE_URL + "Jcompetences";
        });
    });
};

exports.loadJuniorConfirmation = function (WEBSITE_URL, URL_API) {
    console.log("confirmationJunior");
};

exports.loadJuniorCompetences = function (WEBSITE_URL, URL_API) {
    var SKILL_TEMPLATE = $(".template_skill").clone().removeClass("template");
    $("#template_skill").empty();

    var allSkill = $.getJSON(SKILL_DATA);
    $.when(allSkill).done(function (data) {

        var allSkillObj = allSkill.responseJSON;
        console.log(allSkillObj);

        var allSkillsName = $.each(allSkillObj, function (index) {
            console.log($(this)[0].name);
            var skill_dom = SKILL_TEMPLATE.clone();

            skill_dom.find(".skill_name").text($(this)[0].name);
            skill_dom.find(".skill_name").attr('id', $(this)[0].id);
            $("#template_skill").append(skill_dom);
        });
        $(".pro-chx").click(function () {
            console.log("lala");
            $(this).toggleClass("checkedSkill");
            $(this).toggleClass("btn-primary");
        });
    }).fail(function (data) {
        console.log(data);
    });

    console.log("competencesJunior");

    $("#sendSkills").click(function () {
        var checkedSkills = $(".checkedSkill");
        var tabIDSkills = [];
        checkedSkills.each(function (index) {
            var idCheckedSkill = $(this).attr('id');

            tabIDSkills.push(idCheckedSkill);
        });
        console.log(tabIDSkills);
        var infoJunior = JSON.parse(localStorage.getItem('USER'));
        var juniorID = infoJunior.juniorId;

        $.ajax({
            method: "PUT",
            url: URL_API + 'junior/' + juniorID + '/addSkills',
            data: {
                skills: tabIDSkills
            },
            success: function success(data) {
                console.log("sucessssss");
            },
            error: function error(data) {
                console.log(data);
            }
        }).done(function (data) {
            console.log("lala");
            window.location.href = WEBSITE_URL + "Jsummary";
        });
    });
};

exports.loadJuniorSummary = function (WEBSITE_URL, URL_API) {
    console.log("Récapitulatif Junior");
    var infoJunior = JSON.parse(localStorage.getItem('USER'));
    var juniorID = infoJunior.juniorId;
    var juniorData;
    var skills;
    $.ajax({
        type: "GET",
        url: URL_API + "junior/" + juniorID
    }).done(function (data) {
        console.log(data);
        juniorData = data;
        var source = $("#api-junior-summary").html();
        var template = Handlebars.compile(source);
        $("#recapHandlebars").html(template(juniorData));
    });

    var mySkills = $.getJSON(JUNIOR_DATA + "/getMySkills");
    $.when(mySkills).done(function (allSkillResponse, mySkillsResponse) {
        console.log(mySkills.responseJSON);
        skills = mySkills.responseJSON;
        var source2 = $("#api-skills").html();

        var template2 = Handlebars.compile(source2);
        $("#skills-handlebar").html(template2(skills));
    });

    var myDispo = $.getJSON(AVAILABILITY_DATA, function (getAvailabilities) {
        var availabilities = getAvailabilities;
        var source3 = $("#api-dispos").html();
        var template3 = Handlebars.compile(source3);
        $("#dispos-handlebar").html(template3(availabilities));
    });
};

/***/ }),
/* 5 */,
/* 6 */
/***/ (function(module, exports) {

exports.storageJSON = function (name) {

    this.length = 0;

    var storageName = name;
    var storage = localStorage.getItem(storageName);
    var data = storage == null ? {} : JSON.parse(storage);

    for (var key in data) {
        this.length++;
    }

    this.getItem = function (key) {
        if (!key) return null;
        if (typeof data[key] == 'undefined') return null;
        return data[key];
    };

    this.setItem = function (key, val) {
        if (typeof key != 'string') return;
        if (typeof val == 'undefined') return;
        if (typeof data[key] == 'undefined') {
            this.length++;
        }
        data[key] = val;
        localStorage.setItem(storageName, JSON.stringify(data));
    };

    this.addItem = function (val) {
        if (typeof val == 'undefined') return;
        var id = 'uniqid_' + Math.random().toString(36).substr(2, 9);
        while (typeof data[id] != 'undefined') {
            var id = 'uniqid_' + Math.random().toString(36).substr(2, 9);
        }
        this.setItem(id, val);
        return id;
    };

    this.removeItem = function (key) {
        if (!key) return null;
        if (typeof data[key] == 'undefined') return null;
        delete data[key];
        this.length--;
        localStorage.setItem(storageName, JSON.stringify(data));
    };

    this.clear = function () {
        data = {};
        this.length = 0;
        localStorage.removeItem(storageName);
    };

    this.key = function (ind) {
        var i = 0;
        for (var key in data) {
            if (i == ind) return key;
            i++;
        }
        return null;
    };

    this.foreach = function (fct) {
        if (typeof fct != 'function') return;
        for (var key in data) {
            fct(key, this.getItem(key));
        }
    };

    this.itemsToArray = function () {
        var items = [];
        this.foreach(function (key, item) {
            items.push(item);
        });
        return items;
    };

    this.keysToArray = function () {
        var keys = [];
        this.foreach(function (key, item) {
            keys.push(key);
        });
        return keys;
    };

    this.toArray = function () {
        var all = [];
        for (var key in data) {
            var item = this.getItem(key);
            all.push({ key: key, item: item });
        }
        return all;
    };

    this.toJson = function () {
        return localStorage.getItem(storageName);
    };
};

/***/ }),
/* 7 */,
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

function login(username, password, urlAPI) {
    return $.ajax({
        type: "POST",
        url: urlAPI + "login",
        data: {
            email: username,
            password: password
        },
        xhrFields: {
            withCredentials: true
        }
    });
}
function logout(urlAPI) {
    return $.ajax({
        type: "GET",
        url: urlAPI + "logout",
        xhrFields: {
            withCredentials: true
        }
    });
}

exports.login = function (username, password, urlAPI) {
    return login(username, password, urlAPI);
};

exports.logout = function (urlAPI) {
    return logout(urlAPI);
};

exports.loadPageLogin = function (WEBSITE_URL, URL_API) {
    $('#login-form').submit(function (e) {
        var _this = this;

        e.preventDefault();

        localStorage.clear();
        var email = $("#email", this).val();
        var password = $("#password", this).val();
        console.log("username :" + email, "password :" + password);

        var logResult = login(email, password, URL_API);

        logResult.done(function (data) {
            var ls = __webpack_require__(6);

            localUserStorage = new ls.storageJSON('USER');
            $("#email", _this).removeClass('is-invalid');
            $("#password", _this).removeClass('is-invalid');

            var HOMEPAGES = {
                'Junior': { 'homepage': WEBSITE_URL + "Jprivate" },
                'Senior': { 'homepage': WEBSITE_URL + "SprofilPrivate" },
                'Sec. Mem.': { 'homepage': WEBSITE_URL + "Adashboard" }
            };

            var user = data;
            console.log(data);
            localUserStorage.setItem('userID', user.id);
            localUserStorage.setItem('groupName', user.groups[0].name);
            localUserStorage.setItem('address', user.address);
            var homepageToRedirect = HOMEPAGES[user.groups[0].name];
            window.location.href = homepageToRedirect.homepage;
        });
        logResult.fail(function (data) {
            $("#email", _this).addClass('is-invalid');
            $("#password", _this).addClass('is-invalid');
        });
    });
};

/***/ }),
/* 9 */,
/* 10 */,
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

var CONST = __webpack_require__(0);
var JUNIOR_DATA = CONST.JUNIOR_DATA;
var REQUEST_DATA = CONST.REQUEST_DATA;
var AVAILABILITY_DATA = CONST.AVAILABILITY_DATA;
var SKILL_DATA = CONST.SKILL_DATA;
exports.loadJuniorPrivate = function () {
    var REQUEST_TEMPLATE = $(".template_request").clone().removeClass("template");
    var REQUEST_ACCEPTED_TEMPLATE = $(".template_request_accepted").clone().removeClass("template");
    var REQUEST_DONE_TEMPLATE = $(".template_request_done").clone().removeClass("template");
    var SKILL_TEMPLATE = $(".template_skill").clone().removeClass("template");
    var SKILL_ALL_TEMPLATE = $(".template_all_skill").clone().removeClass("template");
    var availabilities = [];

    $("#requestList").empty();
    $("#requestList_accepted").empty();
    $("#requestList_done").empty();
    $("#template_skill").empty();
    $("#template_all_skill").empty();
    var juniorId = -1;

    var infoJunior = JSON.parse(localStorage.getItem('USER'));

    //Arthur's Logic
    var urlGetByID = JUNIOR_DATA + '/getByUserId/' + infoJunior.userID;
    $.getJSON(urlGetByID, function (data) {
        juniorId = data.id;
        // SUPER DIVISION JUNIOR_DATA / juniorId
        $.ajax({
            method: "GET",
            dataType: "json",
            url: JUNIOR_DATA + "/" + juniorId
        }).done(function (data) {
            var junior = data;
            var source = $("#api-junior").html();
            var template = Handlebars.compile(source);
            $("#profilHandlebar").html(template(junior));
        }).fail(function (data) {
            console.log(data);
            $("#profilHandlebar").html(data.responseJSON);
        });
    });
    menuHandler();

    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus');
    });

    //modif profile ***************************
    $('#modificationFormJ').on('submit', function (event) {
        event.preventDefault();
        //console.log(juniorId);
        $.ajax({
            method: $(this).attr('method'),
            url: '/gorilla/api/v1/junior/' + juniorId,
            data: {
                lastName: $('#lastNameJ').val(),
                firstName: $('#firstNameJ').val(),
                street: $('#streetJ').val(),
                streetNumber: $('#streetNumberJ').val(),
                NPA: $('#NPAJ').val(),
                city: $('#cityJ').val(),
                phone: $('#phoneJ').val(),
                email: $('#emailJ').val(),
                birth: '1995-06-30',
                gender: 'F',
                password: 'pomme'
            },

            dataType: "json",
            success: function success(data) {
                //console.log(data);
            },
            error: function error(data) {
                //console.log(data);
            }

        }).done(function (data) {

            //console.log(data);
        }).fail(function (data) {

            //console.log(data);
            $.each(data.responseJSON.error, function (key, value) {

                //console.log(value[0]);
                //console.log(key);

                var input = '#modificationForm input[name=' + key + ']';
                //console.log(input);

                $(input + '+span').text(value["0"]).css('font-size', '0.9rem');
            });
        });
    });
    //requêtes ******************************************
    $.getJSON(REQUEST_DATA, function (data) {
        $.each(data, function (i, entry) {
            switch (entry['state']) {
                case "created":
                    var request_dom = REQUEST_TEMPLATE.clone();

                    request_dom.find(".request_description").text(entry.description);
                    request_dom.find(".request_startDate").text(entry.startDate);
                    request_dom.find(".request_seniorName").text(entry.senior.user.firstName);
                    request_dom.find(".btn-accepte").attr('id', entry.id);
                    request_dom.find(".btn-rapport").attr('id', entry.id);

                    $("#requestList").append(request_dom);

                    request_dom.sort(function (a, b) {
                        var dateA = a.startDate;
                        var dateB = b.startDate;
                        var convertDateA = new Date($.trim(dateA));
                        var convertDateB = new Date($.trim(dateB));
                    });
                    break;
                case "accepted":
                    var request_accepted_dom = REQUEST_ACCEPTED_TEMPLATE.clone();

                    request_accepted_dom.find(".request_description").text(entry.description);
                    request_accepted_dom.find(".request_startDate").text(entry.startDate);
                    request_accepted_dom.find(".request_seniorName").text(entry.senior.user.firstName);

                    $("#requestList_accepted").append(request_accepted_dom);

                    request_accepted_dom.sort(function (a, b) {
                        var dateA = a.startDate;
                        var dateB = b.startDate;
                        var convertDateA = new Date($.trim(dateA));
                        var convertDateB = new Date($.trim(dateB));
                    });
                    break;
                case "done":
                    var request_done_dom = REQUEST_DONE_TEMPLATE.clone();
                    request_done_dom.find(".request_description").text(entry.description);
                    request_done_dom.find(".request_startDate").text(entry.startDate);
                    request_done_dom.find(".request_seniorName").text(entry.senior.user.firstName);

                    $("#requestList_done").append(request_accepted_dom);

                    request_done_dom.sort(function (a, b) {
                        var dateA = a.startDate;
                        var dateB = b.startDate;
                        var convertDateA = new Date($.trim(dateA));
                        var convertDateB = new Date($.trim(dateB));
                    });
                    break;
            }
        });
    });

    //Requêtes Accepted**************************

    $("#tabBord").on("click", ".btn-accepte", function (e) {
        var _this = this;

        /* $.getJSON(REQUEST_DATA, function(data){
            //console.log(data);
            var theRequest = data+"/"+this.id;
            console.log(theRequest);
        }) */
        $.ajax({
            method: "GET",
            dataType: "json",
            url: REQUEST_DATA + "/" + this.id
        }).done(function (data) {
            console.log(data);
            var req = data;
            req.state = 'accepted';
            $.ajax({
                method: "PUT",
                dataType: "json",
                data: req,
                url: REQUEST_DATA + "/" + _this.id
            }).done(function (data) {
                //$(e.trigger).closest(".template_request")
                window.location.reload();
            }).fail(function (data) {
                return console.log(data);
            });
        }).fail(function (data) {
            console.log(data);
        });
    });

    //Rapport d'intervention**************************
    $("#tabBord").on("click", ".btn-rapport", function (e) {
        console.log("ok");
    });

    //Disponnibilités**************************
    $.getJSON(AVAILABILITY_DATA, function (getAvailabilities) {
        availabilities = getAvailabilities;
        $("#addressList").append($('<option/>', {
            text: infoJunior.address.street + "/" + infoJunior.address.city
        }));
        $(availabilities).each(function (index, value) {
            var fromSub = value.from.substr(0, value.from.length - 3);
            var toSub = value.to.substr(0, value.to.length - 3);
            $('#' + value.day + '_from').attr('idBd', value.id);
            $('#' + value.day + '_from').val(fromSub);
            $("#" + value.day + '_to').val(toSub);
        });
    });
    $("#formAvailability").change(function () {
        $("#submitAvailabilities").attr('disabled', false);
    });
    $('#formAvailability').submit(function (event) {
        event.preventDefault();
        var method = "PUT";
        $("#tableAvailability").children('tbody').children('tr').each(function (index, value) //Parcours les jours de la semaine
        {
            var from = $('#' + value.id + '_from').val();
            var to = $('#' + value.id + '_to').val();
            if (from == "" && to == "") {
                method = "DELETE";
            }
            //     $.ajax({
            //         url: '/gorilla/api/v1/availability/'+$("#"+value.id+"_from").attr("idbd"),
            //         type: 'PUT',
            //         dataType: "json",
            //         data: {
            //             day: value.id,
            //             from: ""+$('#'+value.id+'_from').val()+":00",
            //             to: ""+$('#'+value.id+'_to').val()+":00",
            //             juniorId: juniorId,
            //             addressId: infoJunior.address.id,
            //         },
            //         success: function(data) {
            //             console.log(value.id + "updated");
            // },
            //         error: function (data) {
            //             console.log(data);
            //         }
            //     });
        });
        // location.reload();
        // changePage("#dipsonibilite");
    });
    //Compétences ******************************************
    var allSkill = $.getJSON(SKILL_DATA);
    var mySkills = $.getJSON(JUNIOR_DATA + "/getMySkills");

    $.when(allSkill, mySkills).done(function (allSkillResponse, mySkillsResponse) {
        var arrayAllSkill = allSkillResponse[0];
        var mySkillsBd = mySkillsResponse[0];
        var arrayMySkill = [];
        $(mySkillsBd).each(function (index, mySkill) {
            arrayMySkill[index] = mySkill.name;
        });
        $(arrayAllSkill).each(function (index, skill) {
            if ($.inArray(skill.name, arrayMySkill) != -1) {
                var skill_dom = SKILL_TEMPLATE.clone();
                skill_dom.find(".skill_name").text(skill.name);
                $("#template_skill").append(skill_dom);
            } else {
                var skill_all_dom = SKILL_ALL_TEMPLATE.clone();
                skill_all_dom.find(".skill_all_name").text(skill.name);
                $("#template_all_skill").append(skill_all_dom);
                console.log(skill_all_dom);
            }
        });
    }).fail(function (data) {
        console.log(data);
    });
};
/* *
 * Gestion du clique sur un élément créé dynamiquement
 */
$('#skill').on('click', '#skillName', function (event) {

    // $.ajax({
    //     url: '/gorilla/api/v1/availability/'+$("#"+value.id+"_from").attr("idbd"),
    //     type: 'PUT',
    //     dataType: "json",
    //     data: {
    //         day: value.id,
    //         from: ""+$('#'+value.id+'_from').val()+":00",
    //         to: ""+$('#'+value.id+'_to').val()+":00",
    //         juniorId: juniorId,
    //         addressId: infoJunior.address.id,
    //     },
    //     success: function(data) {
    //         console.log(value.id + "updated");
    //     },
    //     error: function (data) {
    //         console.log(data);
    //     }
    // });
    $(this).attr('id', 'skillAllName');
    $("#template_all_skill").append(this);
});
$('#skill').on('click', '#skillAllName', function (event) {
    $(this).attr('id', 'skillName');
    $("#template_skill").append(this);
});

function menuHandler() {

    $("nav.mainNavJunior a.nav-link").on("click", function (event) {

        var page = $(this).attr("href");

        changePage(page);

        return false;
    });

    $("nav.mainNavJunior a.nav-link:first").trigger("click");

    $(window).on("popstate", function (event) {
        var id = location.hash; //Return the anchor part of a URL

        if ($(id).length == 0) {
            changePage("#tabBord");
        } else {
            changePage(id);
        }
    });

    $(window).trigger("popstate");
}

function changePage(page) {

    $("div.app .page_junior").each(function () {

        $(this).hide();
        if ("#" + $(this).attr('id') == page) {
            $(this).show();
        }
    });

    $("nav.mainNavJunior a.nav-link").removeClass("active");
    $("a[href='" + page + "'").addClass("active");
}

/***/ }),
/* 12 */
/***/ (function(module, exports) {

exports.loadRegistration = function (WEBSITE_URL, URL_API) {

    console.log("registration");

    $("#birth").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });

    $('#registrationForm').on('submit', function (event) {

        event.preventDefault();

        var form_data = $(this).serialize();

        console.log(form_data);
        console.log("helo");
        console.log(event);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhrFields: {
                withCredentials: true
            }
        });

        $.ajax({
            method: $(this).attr('method'),
            url: $(this).attr('action'),
            data: {
                gender: $("#gender option:selected").val(),
                lastName: $('#lastName').val(),
                firstName: $('#firstName').val(),
                birth: $('#birth').val(),
                street: $('#street').val(),
                streetNumber: $('#streetNumber').val(),
                NPA: $('#NPA').val(),
                city: $('#city').val(),
                phone: $('#phone').val(),
                email: $('#email').val(),
                password: $('#password').val()
            },
            dataType: "json",
            success: function success(data) {
                console.log("success");
                console.log(data);
            }
        }).done(function (data) {
            console.log("Call to next part of form.");
            console.log(data);
            window.location.href = WEBSITE_URL + "Sconfirmation";
        }).fail(function (data) {
            console.log("fail");
            console.log(data);
            $.each(data.responseJSON.error, function (key, value) {

                console.log(value[0]);
                console.log(key);

                var input = '#registrationForm input[name=' + key + ']';
                console.log(input);

                $(input + '+span').text(value["0"]).css('font-size', '0.9rem');
            });
        });
    });
};

/***/ }),
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(6);
__webpack_require__(11);
__webpack_require__(8);
__webpack_require__(12);
__webpack_require__(17);
__webpack_require__(4);
module.exports = __webpack_require__(18);


/***/ }),
/* 17 */
/***/ (function(module, exports) {

function loadHeader() {
    var x = document.getElementById("myNavbar");
    if (x.className === "navbar") {
        x.className += " responsive";
    } else {
        x.className = "navbar";
    }
}

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

/********************** APP CONST ***************************/
CONST = __webpack_require__(0);
var WEBSITE_URL = CONST.WEBSITE_URL;
var URL_API = CONST.URL_API;
var JUNIOR_DATA = CONST.JUNIOR_DATA;
var REQUEST_DATA = CONST.REQUEST_DATA;
var SKILL_DATA = CONST.SKILL_DATA;
//var USERS = new JsonStorage("USERS");

/***************** HELPER FUNCTION *************************/
function isWantedPage(pageName) {
    var url = window.location.pathname;
    return url.indexOf(pageName) !== -1;
}

/******************* WHEN LOAD PAGE FINISH *****************/
$(function () {

    // test if the page has 'Connexion in his name'

    if (isWantedPage('Connexion')) {
        // load the function exported in login.js
        var login = __webpack_require__(8);

        // execute the loadLogin function in the file login.js
        login.loadPageLogin(WEBSITE_URL, URL_API);
    }
    if (isWantedPage('Jprivate')) {

        var juniorPrivate = __webpack_require__(11);
        // execute the loadLogin function in the file login.js
        juniorPrivate.loadJuniorPrivate();
    }
    if (isWantedPage('SprofilPrivate')) {
        var privateSenior = __webpack_require__(19);

        privateSenior.loadSeniorPrivate(URL_API);
    }

    if (isWantedPage('Sformulaire')) {
        var privateSenior = __webpack_require__(12);

        privateSenior.loadRegistration(WEBSITE_URL, URL_API);
    }
    if (isWantedPage('Jform')) {
        var juniorform = __webpack_require__(4);

        juniorform.loadJuniorRegistration(WEBSITE_URL, URL_API);
    }
    if (isWantedPage('Jconfirmation')) {
        var juniorConf = __webpack_require__(4);

        juniorConf.loadJuniorConfirmation(WEBSITE_URL, URL_API);
    }
    if (isWantedPage('Javaibility')) {
        var juniorConf = __webpack_require__(4);

        juniorConf.loadJuniorAvailability(WEBSITE_URL, URL_API);
    }
    if (isWantedPage('Jcompetences')) {
        var juniorComp = __webpack_require__(4);

        juniorComp.loadJuniorCompetences(WEBSITE_URL, URL_API);
    }
    if (isWantedPage('Jsummary')) {
        var juniorSummary = __webpack_require__(4);

        juniorSummary.loadJuniorSummary(WEBSITE_URL, URL_API);
    }
});

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

CONST = __webpack_require__(0);
var URL_API = CONST.URL_API;
var DATE_FORMAT = "Y-MM-DD HH:mm:ss";

var requests = __webpack_require__(20);
var Request = requests.Request;

var ls = __webpack_require__(6);
var localUserStorage = new ls.storageJSON('USER');

window.Handlebars.registerHelper('select', function (value, options) {
    var $el = $('<select />').html(options.fn(this));
    $el.find('[value="' + value + '"]').attr({ 'selected': 'selected' });
    return $el.html();
});

window.Handlebars.registerPartial('simpleInput', '<div class="form-group ">' + '<label for="{{fieldName}}" class="">{{fieldNameFR}}</label>' + '<div class="">' + '<input type="text" name="{{fieldName}}" class="form-control" id="{{fieldName}}" value="{{ fieldValue}}" placeholder="{{placeholder}}" {{#if required}} required{{/if}}>' + '</div>' + '</div>');

window.Handlebars.registerHelper('getRequestStateFR', function (stateRequest) {
    switch (stateRequest) {
        case "created":
            return "En cours d'attribution";
        case "sent":
            return "En attente d'acceptation";
        case "accepted":
            return "Acceptée";
        case "not accepted":
            return "Refusée";
        case "done":
            return "Terminée";
    }
});

function loadSenior(userID) {
    return $.ajax({
        type: "GET",
        url: URL_API + "senior/findByUserID/" + userID
    });
}

function loadSeniorRequest(seniorID) {
    return $.ajax({
        type: "GET",
        url: URL_API + "request" //+ userID
    });
}

function loadSkills() {
    return $.ajax({
        type: "GET",
        url: URL_API + "skill"
    });
}

function updateSenior(data, idSenior) {
    return $.ajax({
        type: "PUT",
        data: data,
        url: URL_API + "senior/" + idSenior
    });
}

function saveNewRequest() {
    var startDate = moment($('#newDemande-dateFrom').val() + " " + $('#newDemande-timeFrom').val() + ":00");
    var endDate = moment($('#newDemande-dateTo').val() + " " + $('#newDemande-timeTo').val() + ":00");

    if ($('#newDemande-description').val().length > 1000) {
        $("#newDemande-step4-errors").append($('<div class="alert alert-danger" role="alert">Déscription trop longue</div>'));
        return;
    }

    skills = [];
    $('input[name=newDemande-skills]').each(function () {
        if ($(this).is(':checked')) skills.push($(this).val());
    });

    //simple request
    if (startDate.dayOfYear() == endDate.dayOfYear()) {
        var request = new Request($('#newDemande-description').val(), startDate.format(DATE_FORMAT), endDate.format(DATE_FORMAT), skills, 'simple');
    }
    // multiple request
    else if (startDate.week() < endDate.week() && startDate.day() <= endDate.day()) {
            var nbrMinute = endDate.hour() * 60 + endDate.minute() - (startDate.hour() * 60 + startDate.minute());

            var request = new Request($('#newDemande-description').val(), startDate.format(DATE_FORMAT), startDate.add(nbrMinute, 'm').format(DATE_FORMAT), $('#newDemande-skills').val(), 'multiple');
        } else {
            $("#newDemande-step4-errors").append($('<div class="alert alert-danger" role="alert">Erreurs dans les dates</div>'));
            return;
        }

    request.save().done(function (data) {
        if (request.type == 'multiple') {
            var nbrWeek = endDate.week() - startDate.week();
            for (i = 0; i < nbrWeek; i++) {
                var childRequest = request;
                childRequest.startDate = moment(request.startDate).add(1, 'w').format(DATE_FORMAT);
                childRequest.endDate = moment(request.endDate).add(1, 'w').format(DATE_FORMAT);
                childRequest.requestId = data.id;
                childRequest.type = 'simple';
                childRequest.save().done(function (data) {
                    if (data.endDate = endDate.format(DATE_FORMAT)) {
                        window.location.href = CONST.SENIOR_BACKEND;
                    }
                }).fail(function (data) {
                    return console.log("FAIL : " + data.responseJSON);
                });
            }
        } else {
            window.location.href = CONST.SENIOR_BACKEND;
        }
    }).fail(function (data) {
        $("#newDemande-step4").text(JSON.stringify(data.responseJSON));
    });
}

function updateProfil() {
    var userID = localUserStorage.getItem('userID');
    var user = {};
    $("#profil-registrationForm input,select").each(function (i) {
        user[$(this).attr('name')] = $(this).val();
    });

    loadSenResult = loadSenior(userID);
    loadSenResult.done(function (data) {
        var updtSenRes = updateSenior(user, data.id);
        updtSenRes.done(function (data) {
            var source = $("#tmp-request-selectSkill").html();
            var template = Handlebars.compile(source);
            $("#newDemande-skills").html(template(data));
        }).fail(function (data) {
            $("#profil-registrationForm input,select").each(function (i) {
                if (data.responseJSON.error[$(this).attr('name')] !== undefined) {
                    $(this).addClass('is-invalid');
                }
            });
        });
    });
}

function makeSummaryRequest() {
    var startDate = moment($('#newDemande-dateFrom').val() + " " + $('#newDemande-timeFrom').val());
    var endDate = moment($('#newDemande-dateTo').val() + " " + $('#newDemande-timeTo').val());

    skills = [];
    $('.newDemande-skills').each(function () {
        if ($("input", this).is(':checked')) {
            var badge = $("<span class='badge badge-primary m-2 p-2'></span>");
            badge.text($(this).text().trim());
            console.log("badge");
            $("#step4-summary-skills").append(badge);
        }
    });

    var description = $('#newDemande-description').val();
    $('#step4-summary-description').text("Déscription : " + (description.length > 700 ? description.substr(0, 700) + "..." : description));
    $('#step4-summary-date').text("Du " + startDate.format(DATE_FORMAT) + " au " + endDate.format(DATE_FORMAT));
}

function nextStepNewRequest() {
    var step = Number($('#newDemande').attr('data-step'));
    $("#newDemande-previous").prop('disabled', false);
    if (step <= 4) {
        $('#newDemande').attr('data-step', step + 1);

        $("#newDemande .progress-bar").attr("aria-valuenow", step + 1).width(25 * (step + 1) + "%").html(step + 1);

        $("#newDemande-step" + (step + 1)).toggleClass('d-none');
        $("#newDemande-step" + step).toggleClass('d-none');
        if (step + 1 == 4) {
            $("#newDemande-next").text("Confirmer");
            $("#newDemande-next").addClass("btn-success");
            $("#newDemande-next").removeClass("btn-primary");
            $("#newDemande-next").off("click");
            $("#newDemande-next").on("click", saveNewRequest);

            $("#newDemande-step4-summary li").empty();
            makeSummaryRequest();
        }
    }
}

function previousStepNewRequest() {
    var step = Number($('#newDemande').attr('data-step'));
    if (step > 1) {
        if (step == 4) {
            $("#newDemande-next").text("Suivant");
            $("#newDemande-next").removeClass("btn-success");
            $("#newDemande-next").addClass("btn-primary");
            $("#newDemande-next").off("click");
            $("#newDemande-next").on("click", nextStepNewRequest);
        }
        Number($('#newDemande').attr('data-step', step - 1));

        $("#newDemande .progress-bar").attr("aria-valuenow", step - 1).width(25 * (step - 1) + "%").html(step - 1);

        $("#newDemande-step" + (step - 1)).toggleClass('d-none');
        $("#newDemande-step" + step).toggleClass('d-none');
    }
    if (step == 2) {
        $("#newDemande-previous").prop('disabled', true);
    }
}

exports.loadSeniorPrivate = function () {
    $.fn.datepicker.setDefaults({
        format: 'yyyy-mm-dd',
        autoHide: true
    });

    /******************DATA LOAD********************/
    var userID = localUserStorage.getItem('userID');
    var loadSenResult = loadSenior(userID);
    loadSenResult.done(function (data) {
        console.log(data);

        var source = $("#tmp-profil-senior").html();
        var template = Handlebars.compile(source);
        $("#profil-info").html(template(data));
    });
    loadSenResult.fail(function (data) {
        return $("#profil-info").text(JSON.stringify(data.responseJSON));
    });

    loadSenResult = loadSeniorRequest();
    loadSenResult.done(function (data) {
        console.log(data);
        var source = $("#tmp-request-requestCards").html();
        var template = Handlebars.compile(source);
        $(data).each(function (i) {
            switch (data[i].state) {
                case "created":
                case "sent":
                    $("#demande-created-cards").append(template(data[i]));
                    break;
                case "accepted":
                    $("#demande-accepted-cards").append(template(data[i]));
                    break;
                case "not accepted":
                case "done":
                    $("#demande-done-cards").append(template(data[i]));
                    break;
            }
        });
    });

    loadSenResult = loadSkills();
    loadSenResult.done(function (data) {
        console.log(data);
        var source = $("#tmp-request-selectSkill").html();
        var template = Handlebars.compile(source);
        $("#newDemande-skills").html(template(data));
    });
    loadSenResult.fail(function (data) {
        console.log(data);
    });

    /******************** BIND EVENTS ************************/
    $("#newDemande-next").on('click', nextStepNewRequest);
    $("#newDemande-previous").on('click', previousStepNewRequest);
    $("#profil-btnSaveProfil").on('click', updateProfil);
    $("#newDemande-dateFrom").on('change', function () {
        $("#newDemande-dateTo").val($(this).val());
    });
    $("input[name=newDemande-isSimpleOrMultiple]").on('change', function () {
        $("#newDemande-dateTo").fadeToggle();
        $("#newDemande-dateFrom").off('change');

        if ($(this).val() == 'simple') {
            $("#newDemande-dateTo").val($("#newDemande-dateFrom").val());
            $("#newDemande-dateFrom").on('change', function () {
                $("#newDemande-dateTo").val($(this).val());
            });
        }
    });

    /******************** Load GUI COMPONENT ****************/
    $("#newDemande-previous").prop('disabled', true);
    $("#newDemande-dateTo").toggle();

    $('#newDemande-timeFrom,#newDemande-timeTo').timepicker({
        timeFormat: 'H:mm',
        interval: 30,
        minTime: '8',
        maxTime: '8:00pm',
        defaultTime: '8',
        startTime: '8:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('#newDemande-dateFrom,#newDemande-dateTo').datepicker();
    $('input[value=simple]').prop("checked", true);
};

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

CONST = __webpack_require__(0);
var URL_API = CONST.URL_API;

function Request(description, startDate, endDate, skillsJuniorIds, type) {
    this.description = description;
    this.state = "created";
    this.startDate = startDate;
    this.endDate = endDate;
    this.skillsJuniorIds = skillsJuniorIds;
    this.type = type;
}
Request.prototype.getObject = function () {
    var request = {
        description: this.description,
        state: this.state,
        startDate: this.startDate,
        endDate: this.endDate,
        skillsJuniorIds: this.skillsJuniorIds,
        type: this.type
    };

    if (typeof this.requestId !== 'undefined') request.requestId = this.requestId;

    return request;
};
Request.prototype.save = function () {
    return $.ajax({
        type: "POST",
        data: this.getObject(),
        url: URL_API + "request"
    });
};

module.exports = { Request: Request };

/***/ })
/******/ ]);