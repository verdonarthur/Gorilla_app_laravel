@extends('layouts.layoutPublic')

@section('main')
<h6 class="title-seniorform">Je suis senior et je souhaite m'inscrire à Generation Connect</h6>
<p class="paragraphe">Si vous n'arrivez pas à vous inscrire appelez notre secrétariat au <b>021 323 40 50.</b> </p>

<form class="form-in center-div" id="registrationForm" method="post" action="{{ url('/api/v1/senior') }}">
    {{ csrf_field() }}
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Genre*</label>
        <div class="col-lg-4">
            <select class="form-control" id="gender">
                <option selected disabled>Genre</option>
                <option value="F">Femme</option>
                <option value="M">Homme</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="lastName" class="col-sm-2 col-form-label">Nom*</label>
        <div class="col-lg-4">
            <input type="text" name="lastName" class="form-control" id="lastName" value="{{ old('lastName') }}" placeholder="Nom" required>
            <span></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="firstName" class="col-sm-2 col-form-label">Prénom*</label>
        <div class="col-lg-4">
            <input type="text" name="firstName" class="form-control" id="firstName" value="{{ old('firstName') }}" placeholder="Prénom" required>
            <span></span>
        </div>
    </div>
    <div class="form-group row" data-provide="datepicker senior">
        <label for="birth" class="col-sm-2 col-form-label">Date de naissance*</label>
        <div class="col-lg-4">
            <input type="text" name="birth" class="date form-control" id="birth" value="{{ old('birth') }}" placeholder="aaaa-mm-jj" required>
            <span></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="street" class="col-sm-2 col-form-label">Adresse*</label>
        <div class="col-lg-4">
            <input type="text" name="street" class="form-control" id="street" value="{{ old('street') }}" placeholder="Adresse" required>
            <span></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="streetNumber" class="col-sm-2 col-form-label">Numéro de rue*</label>
        <div class="col-lg-1">
            <input type="text" name="streetNumber" class="form-control" id="streetNumber" value="{{ old('streetNumber') }}">
            <span></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="city" class="col-sm-2 col-form-label">Ville*</label>
        <div class="col-lg-4">
            <input type="text" name="city" class="form-control" id="city" value="{{ old('city') }}" placeholder="Ville" required>
            <span></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="NPA" class="col-sm-2 col-form-label">NPA*</label>
        <div class="col-sm-1">
            <input type="text" name="NPA" class="form-control" id="NPA" value="{{ old('NPA') }}" placeholder="NPA" required>
            <span></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="phone" class="col-sm-2 col-form-label">Téléphone*</label>
        <div class="col-lg-4">
            <input type="tel" name="phone" class="form-control" id="phone" value="{{ old('phone') }}" placeholder="Ex: +41 78 919 94 15" required>
            <span></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="email" class="col-sm-2 col-form-label">Email*</label>
        <div class="col-lg-4">
            <input type="email" name="email" class="form-control" id="email" value="{{ old('email') }}" placeholder="Email" required>
            <span></span>
        </div>
    </div>
    <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label">Mot de passe*</label>
        <div class="col-lg-4">
            <input type="password" name="password" class="form-control" id="password" value="{{ old('password') }}" placeholder="Mot de passe" required>
            <small id="passwordHelpInline" class="text-muted">
                Doit contenir entre 8 à 20 caractères.
            </small>
            <span></span>
        </div>
    </div>
    <div class="d-flex justify-content-center btn-pad">
            <button type="submit" class="btn btn-primary">S'enregistrer</button>
    </div>
</form>
<div class="alert alert-success" role="alert" hidden>
    upload successfully
</div>
@endsection
