@extends('layouts.layoutPrivate') @section('app_junior')
<div class="app">
    <section class="page_junior" id="tabBord">
        <h3 class="titlePrivJunior">Tableau des demandes</h3>
        <div class="container-fluid tabBordPage">
            <div class="row TD-row flex-row flex-sm-nowrap py-5">
                <div class="col-sm-4 col-md-4 carteJunior">
                    <div class="card card-junior-recu">
                        <div class="card-header card-header-recu">Reçues</div>
                        <div id="requestList">
                            <div class="template template_request card p-2 bg-faded">
                                <h5 class="request_seniorName card-title-junior">Marina Popov</h5>
                                <div class="row card-body card-body-junior">
                                    <div class="col-md-7">
                                        <label>Description:</label>
                                        <p class="request_description">Besoin d'installation de asalhvlh khakhk hkahvkh kakha </p>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn-accepte btn btn-primary">Accepter</button>
                                        <button id="btn-refuse" class="btn-refuse btn btn-primary">Refuser</button>
                                    </div>
                                </div>
                                <div class="card-footer card-footer-junior form-inline">
                                    <p class="date-re">
                                        <i>Date prévue :</i>
                                        <i class="request_startDate"></i>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-3 carteJunior">
                    <div class="card card-junior-acceptee">
                        <div class="card-header card-header-acceptee">Acceptées</div>
                        <div id="requestList_accepted">
                            <div class="template template_request_accepted card p-2 bg-faded">
                                <h5 class="request_seniorName card-title-junior"></h5>
                                <div class="row card-body card-body-junior">
                                    <div class="col-md-7">
                                        <label>Description:</label>
                                        <p class="request_description">Besoin d'installation de asalhvlh khakhk hkahvkh kakha </p>
                                    </div>
                                    <div class="col-md-2">
                                        <button class=" btn-rapport btn btn-primary" data-toggle="modal" data-target="#Modal" id="btnRemplirRapport">Rapport</button>
                                    </div>
                                </div>
                                <div class="card-footer card-footer-junior form-inline">
                                    <p class="date-re">
                                        <i>Date prévue :</i>
                                        <i class="request_startDate"></i>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 carteJunior">
                    <div class="card card-junior-effectuee">
                        <div class="card-header card-header-effectuee">Effectués</div>
                        <div id="requestList_done">
                            <div class="template template_request_done card p-2 bg-faded">
                                <h5 class="request_seniorName card-title-junior"></h5>
                                <div class="card-body card-body-junior">
                                    <div class="col-md-7">
                                        <label>Description:</label>
                                        <p class="request_description">Besoin d'installation de asalhvlh khakhk hkahvkh kakha </p>
                                    </div>
                                </div>
                                <div class="card-footer card-footer-junior form-inline">
                                    <p class="date-re">
                                        <i>Date prévue :</i>
                                        <i class="request_startDate"></i>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @verbatim
    <section id="profil" class="page_junior">
        <div id="profilHandlebar"></div>
        <script id="api-junior" type="text/x-handlebars-template">
                    <div class="card-boxer card-header d-flex justify-content-between">
                        <h3 class="title_privateJunior">{{ user.firstName }} {{ user.lastName }}</h3>
                    </div>
                    <div class="profilPage container-profil">
                        <div class="profilJunior">
                            <div class="row">
                                <div class="col-xs-5 col-sm-5 col-md-6 col-lg-6 ">
                                    <div class="card card-back" style="margin-bottom: 40px">
                                        <ul class="list-group mb-3">
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Genre</h5>
                                                    <small class="text-muted">{{ user.gender }}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Nom</h5>
                                                    <small class="text-muted">{{ user.lastName }}</small>
                                                </div>
                                                <span class="text-muted"></span>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Prénom</h5>
                                                    <small class="text-muted">{{ user.firstName }}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Date de naissance</h5>
                                                    <small class="text-muted">{{user.birth}}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Rue et numéro</h5>
                                                    <small class="text-muted">{{ user.address.street }} {{ user.addres.streetNumber }}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Ville</h5>
                                                    <small class="text-muted">{{ user.address.city }}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Code postale</h5>
                                                    <small class="text-muted">{{ user.address.NPA }}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Téléphone</h5>
                                                    <small class="text-muted">{{ user.phone }}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">E-mail</h5>
                                                    <small class="text-muted">{{ user.email }}</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn-pos btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                <div class="font">Modifier votre profil</div>
                            </button>
                        </div>
                    </div>
                </script>
    </section>
    <section id="profil" class="page_junior">
        <div id="profilHandlebar"></div>
        <script id="api-junior" type="text/x-handlebars-template">
                    <div class="card-boxer card-header d-flex justify-content-between">
                        <h2 class="card-title title_privateJunior">{{ user.firstName }} {{ user.lastName }}</h2>
                    </div>
                    <div class="container profilPage container-profil">
                        <div class="container profilJunior">
                            <div class="row">
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-5 ">
                                    <div class="card card-back" style="margin-bottom: 40px">
                                        <ul class="list-group mb-3">
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Genre</h5>
                                                    <small class="text-muted">{{ user.gender }}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Nom</h5>
                                                    <small class="text-muted">{{ user.lastName }}</small>
                                                </div>
                                                <span class="text-muted"></span>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Prénom</h5>
                                                    <small class="text-muted">{{ user.firstName }}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Date de naissance</h5>
                                                    <small class="text-muted">{{user.birth}}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Rue et numéro</h5>
                                                    <small class="text-muted">{{ user.address.street }} {{ user.addres.streetNumber }}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Ville</h5>
                                                    <small class="text-muted">{{ user.address.city }}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Code postale</h5>
                                                    <small class="text-muted">{{ user.address.NPA }}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">Téléphone</h5>
                                                    <small class="text-muted">{{ user.phone }}</small>
                                                </div>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                <div>
                                                    <h5 class="my-0">E-mail</h5>
                                                    <small class="text-muted">{{ user.email }}</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn-pos btn btn-primary" data-toggle="modal"
                                    data-target="#exampleModal">
                                <div class="font">Modifier votre profil</div>
                            </button>
                        </div>
                    </div>
                </script>
    </section>
    @endverbatim
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modification du profil</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-in" id="modificationFormJ" method='PUT' role="form" action="api/v1/junior">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Genre</label>
                            <div class="col-lg-3">
                                <select class="form-control" id="genderJ">
                                    <option selected disabled>Genre</option>
                                    <option value="F">Femme</option>
                                    <option value="H">Homme</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lastNameJ" class="label-font col-sm-2 col-form-label">Nom</label>
                            <div class="col-lg-7">
                                <input type="text" class="label-font form-control form-input" id="lastNameJ" name="lastNameJ" placeholder="Nom" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="firstNameJ" class="label-font col-sm-2 col-form-label">Prénom</label>
                            <div class="col-lg-7">
                                <input type="text" name="firstNameJ" class="form-control form-input" id="firstNameJ" placeholder="Prénom" required>
                            </div>
                        </div>
                        <div class="form-group row" data-provide="datepicker junior">
                            <label for="birthJ" class="col-sm-2 col-form-label">Age</label>
                            <div class="col-lg-2">
                                <input type="text" name="birthJ" class="form-control" id="birthJ" value="{{ old('birth') }}" placeholder="aaaa-mm-jj">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="streetJ" class="label-font col-sm-2 col-form-label">Adresse</label>
                            <div class="col-lg-7">
                                <input type="text" name="streetJ" class="form-control form-input" id="streetJ" placeholder="Adresse" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="streetNumberJ" class="label-font col-sm-2 col-form-label">Numéro de rue
                            </label>
                            <div class="col-lg-2">
                                <input type="text" name="streetNumberJ" class="form-control form-input" id="streetNumberJ">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cityJ" class="label-font col-sm-2 col-form-label">Ville</label>
                            <div class="col-lg-5">
                                <input type="text" name="cityJ" class="form-control form-input" id="cityJ" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="NPAJ" class="label-font col-sm-2 col-form-label">NPA</label>
                            <div class="col-sm-3">
                                <input type="text" name="NPAJ" class="form-control form-input" id="NPAJ" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phoneJ" class="label-font col-sm-2 col-form-label">Téléphone</label>
                            <div class="col-lg-7">
                                <input type="text" name="phoneJ" class="form-control form-input" id="phoneJ" placeholder="Téléphone" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="emailJ" class="label-font col-sm-2 col-form-label">Email</label>
                            <div class="col-lg-7">
                                <input type="email" name="emailJ" class="form-control form-input" id="emailJ" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="passwordJ" class="col-sm-2 col-form-label">Mot de passe</label>
                            <div class="col-lg-3">
                                <input type="password" name="passwordJ" class="form-control" id="passwordJ" value="{{ old('password') }}" placeholder="Mot de passe"
                                    required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn-save btn btn-primary">Valider les modifications
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <section class="page_junior" id="disponibilite">
        <div class="container dispoPage">
            <form class="form-inline">
                <div class="form-group row">
                    <label style="margin-right: 20px">Mes adresses</label>
                    <div class="input-group">
                        <select class="form-control" id="addressList">
                        </select>
                        <span class="input-group-btn">
                            <button class="btn btn-success" type="button" tabindex="-1">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </form>
            <form method="PUT" id="formAvailability" action="#">
                <table id="tableAvailability">
                    <tr id="monday">
                        <td>Lundi</td>
                        <td></td>
                        <td>de</td>
                        <td>
                            <input type="time" id="monday_from" step="1800" required>
                        </td>
                        <td>à</td>
                        <td>
                            <input type="time" id="monday_to" step="1800" required>
                        </td>
                        <td>
                            <input type="checkbox" class="monday_conge"> Congé</td>
                    </tr>
                    <tr id="tuesday">
                        <td>Mardi</td>
                        <td></td>
                        <td>de</td>
                        <td>
                            <input type="time" id="tuesday_from" step="1800">
                        </td>
                        <td>à</td>
                        <td>
                            <input type="time" id="tuesday_to" step="1800">
                        </td>
                    </tr>
                    <tr id="wednesday">
                        <td>Mercredi</td>
                        <td></td>
                        <td>de</td>
                        <td>
                            <input type="time" id="wednesday_from" step="1800">
                        </td>
                        <td>à</td>
                        <td>
                            <input type="time" id="wednesday_to" step="1800">
                        </td>
                    </tr>
                    <tr id="thursday">
                        <td>Jeudi</td>
                        <td></td>
                        <td>de</td>
                        <td>
                            <input type="time" id="thursday_from" step="1800">
                        </td>
                        <td>à</td>
                        <td>
                            <input type="time" id="thursday_to" step="1800">
                        </td>
                    </tr>
                    <tr id="friday">
                        <td>Vendredi</td>
                        <td></td>
                        <td>de</td>
                        <td>
                            <input type="time" id="friday_from" step="1800">
                        </td>
                        <td>à</td>
                        <td>
                            <input type="time" id="friday_to" step="1800">
                        </td>
                    </tr>
                    <tr id="saturday">
                        <td>Samedi</td>
                        <td></td>
                        <td>de</td>
                        <td>
                            <input type="time" id="saturday_from" step="1800">
                        </td>
                        <td>à</td>
                        <td>
                            <input type="time" id="saturday_to" step="1800">
                        </td>
                    </tr>
                    <tr id="sunday">
                        <td>Dimanche</td>
                        <td></td>
                        <td>de</td>
                        <td>
                            <input type="time" id="sunday_from" step="1800">
                        </td>
                        <td>à</td>
                        <td>
                            <input type="time" id="sunday_to" step="1800">
                        </td>
                    </tr>
                </table>
                <button id="submitAvailabilities" type="submit" disabled>Changer mes disponnibilités</button>
            </form>
        </div>
    </section>
    <section class="page_junior" id="connaissance">
        <div class="container skillPage">
            <div class="row" id="skill">
                <h2>Mes skills</h2>
                <br>
                <div class="col-sm" id="template_skill">
                    <p class="badgeSkill">
                        <ul>
                            <li id="skillName" class="template template_skill">
                                <button type="button" class="btn btn-primary skill_name"></button>
                            </li>
                        </ul>
                    </p>
                </div>
                <div id="template_all_skill" class="col-sm">
                    <h2>Skills possibles</h2>
                    <br>
                    <p class="badgeSkill">
                        <ul>
                            <li id="skillAllName" class="template template_all_skill">
                                <button type="button" class="btn btn-primary skill_all_name"></button>
                            </li>
                        </ul>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="page_junior" id="historique">
        <form class="form-inline my-2 my-lg-0 search_junior">
            <input class="form-control mr-sm-2" type="search" placeholder="Rechercher" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
        </form>
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nom du senior</th>
                    <th scope="col">Lieu d'intervention</th>
                    <th scope="col">Date</th>
                    <th scope="col">Durée</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Ottoo</td>
                    <td>@mdo</td>
                    <td>test</td>
                </tr>
            </tbody>
        </table>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="ModalLabel">RAPPORT D'INTERVENTION</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-in" id="RapportJ" method='PUT' role="form" action="api/v1/junior">
                        <div class="form-group row-r">
                            <label class="disp-mod col-form-label">Veuillez entrer la date d'intervention</label>
                            <div class="col-lg-3">
                                <input type="date" id="newDemande-dateFrom" class="form-control mb-2" placeholder="" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                                    value="2018-12-02">
                            </div>
                        </div>
                        <div class="form-group row-r">
                            <label class="disp-mod col-form-label">Durée effective de l'intervention</label>
                            <div class="col-lg-4 timeR">
                                <tr id="day">
                                    <td></td>
                                    <td>de</td>
                                    <td>
                                        <input type="time" id="day_from" step="1800" required>
                                    </td>
                                    <td>à</td>
                                    <td>
                                        <input type="time" id="day_to" step="1800" required>
                                    </td>
                                </tr>
                            </div>
                        </div>
                        <div class="form-group">
                            <div id="newDemande-step1" class="form-group">
                                <label class="disp-mod col-form-label">Commentaire :</label>
                                <div class="form-group col-md-8">
                                    <textarea class="form-control comment" id="newDemande-description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row-r">
                            <div class="d-flex btn-pad">
                                <button id="btn-rapport" type="submit" class="btn-val btn btn-primary">Valider le rapport</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<!--<div class="appSecond">
                <section class="page_profil" id="disponibilite">
                    <div class="container">
                        <form class="form-inline">
                            <div class="form-group row">
                                <label style="margin-right: 20px">Mes adresses</label>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Rue du bugnon 55</option>
                                        <option>Option 1</option>
                                        <option>Option 2</option>
                                    </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-success" type="button" tabindex="-1"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
                <section class="page_profil" id="connaissance">
                    <div class="container">
                        <div class="row" id="skill">
                            <div class="template template_skill">
                                <p class="badgeSkill"><span class="badge badge-dark styleBadge"><i class="skill_name fa fa-check"> Connect with Facebook</i></span></p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>-->
@endsection
