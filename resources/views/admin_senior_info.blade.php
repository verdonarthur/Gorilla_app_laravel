@extends('layouts.layoutAdmin') @section('main')
<!--details senior-->
<div class="container">
    <h5>Prochaines préstations</h5>
    <div class="list-group-infoS">
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <p class="mb-1 junior-match">
                    <b>Domaine d'intervention : </b> Installation</p>
                <div class="location">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <b>Renens</b>
                </div>
            </div>
            <p class="mb-1">
                <b>Nom de l'agent : </b> Marina Popov</p>
            <p class="mb-1">
                <b>E-mail : </b>thimo.bongars@gmail.com</p>
            <p class="mb-1">
                <i class="far fa-calendar-alt fa-2x"></i>30.05.2018</p>
        </div>
        <div class="list-group-item list-group-item-action flex-column align-items-starts border-success">
            <div class="d-flex w-100 justify-content-between">
                <p class="mb-1 junior-match">
                    <b>Domaine d'intervention : </b> Installation</p>
                <div>
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <b>Renens</b>
                </div>
            </div>
            <p class="mb-1">
                <b>Nom de l'agent : </b> Marina Popov</p>
            <p class="mb-1">
                <b>E-mail : </b>thimo.bongars@gmail.com</p>
            <p class="mb-1">
                <i class="far fa-calendar-alt fa-2x"></i>30.05.2018</p>
        </div>
        <div class="list-group-item list-group-item-action flex-column align-items-starts">
            <div class="d-flex w-100 justify-content-between">
                <p class="mb-1 junior-match">
                    <b>Domaine d'intervention : </b> Installation</p>
                <div>
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <b>Renens</b>
                </div>
            </div>
            <p class="mb-1">
                <b>Nom de l'agent : </b> Marina Popov</p>
            <p class="mb-1">
                <b>E-mail : </b>thimo.bongars@gmail.com</p>
            <p class="mb-1">
                <i class="far fa-calendar-alt fa-2x"></i>30.05.2018</p>
        </div>
    </div>
</div>
@endsection