@extends('layouts.layoutPublic') @section('main')
<div class="container">
    <div class="bar-progress">
        <div class="new_demande">
            <div class="paddingTop container-progreebar">
                <ul class="progressbarSenior">
                    <li id="active-progressbar">S'enregistrer</li>
                    <li id="active-progressbar">Disponibilités</li>
                    <li id="active-progressbar">Connaissances et compétences</li>
                    <li id="active-progressbar">Récapitulatif</li>
                    <li id="active-progressbar">Confirmation</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="confirmation">
        <h3 class="conf-title">Confirmation</h3>
        <div class="conf-text">
            Merci d'avoir validé votr e-mail! Vous avez bien été enregistré. <br/>
            Vous êtes maintenant connecté à votre profil, merci de finir toutes les étapes d'inscription suivantes afin
            de valider et d'envoyer votre postulation à Generation Connect. 
        </div>
        <a href="{{url('Javaibility')}}">
            <button id="btn-inscription" class="btn btn-lg btn-primary" type="button">Suivant</button>
        </a>
    </div>
</div>
@endsection