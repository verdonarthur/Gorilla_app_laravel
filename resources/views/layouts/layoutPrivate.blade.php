<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <title>Generation Connect - @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="{{ asset('css/privateJunior.css') }}" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('js/jsonStorage.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.11/handlebars.js"></script>
</head>

<body>
    <nav>
        <div class="bg-dark navbar-dark box-shadow fixed-top d-flex flex-column flex-md-row align-items-center px-md-4 navbar-expand-sm">
            <a href="{{url('')}}">
                <img class="my-0 mr-md-auto navbar-brand logo" src="img/LogoGenerationConnect.png" style="width:3.2em;" />
            </a>
            <div class="navbar my-2 my-md-0 mr-md-3 ml-auto align-items-center"></div>
            <a class="btn btn-outline-primary btn-connexion" href="{{url('logout')}}">Se déconnecter</a>
        </div>
    </nav>
    <nav class="mainNavJunior">
        <ul class="nav navbar-center nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tabBord" role="tab" aria-controls="tabBord" aria-selected="false">Tableau de bord</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="infos" href="#profil" aria-controls="infos" aria-selected="false">Mon profil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="disponibilite" href="#disponibilite" aria-controls="disponibilite" aria-selected="false">Disponibilités</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#connaissance" data-toggle="connaissance" aria-controls="connaissance" aria-selected="false">Connaissances</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#historique" role="tab" aria-controls="historique" aria-selected="false">Historique</a>
            </li>
        </ul>
    </nav>

    @yield("app_junior")

    <footer class="page-footer">
        <div class="d-flex justify-content-between">
                <div class="cand-position col-md-3">

                    </div>
            <div class="cont-position col-md-3">
                <h5 class="white-text">Generation Connect</h5>
                <div class='info'>Rue de Langallerie 1</div>
                <div class='info'>1003 Lausanne </div>
                <div class='info'>Tel: 021 323 40 50</div>
                <div class='info'>E-mail : generationconnect@info.com</div>
            </div>
            <div class="cand-position col-md-3">
                <h5 class="white-text">NOS RÉSEAUX</h5>
                <ul class="social-media-list justify-content-md-center">
                    <li>
                        <i class="fa1 fa-facebook-square fa-2x" aria-hidden="true"></i>
                    </li>
                    <li>
                        <i class="fp fa1 fa-linkedin fa-2x" aria-hidden="true"></i>
                    </li>
                    <li>
                        <i class="fp fa1 fa-twitter-square fa-2x" aria-hidden="true"></i>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
    <script src="{{ asset('js/all.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>

</html>