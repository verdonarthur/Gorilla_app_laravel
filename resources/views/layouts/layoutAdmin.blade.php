<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <title>Generation Connect - @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
        crossorigin="anonymous">
</head>

<body>
    <nav class="navbar bg-dark navbar-dark fixed-top p-3 px-md-4">
        <img class="navbar-brand" src="img/LogoGenerationConnect.png" style="width:3.2em;" />
        <ul class="navbar-nav navbar-expand-sm">
            <li class="nav-item">
                <a class="btn btn-outline-primary btn-connexion" href="{{url('logout')}}">Se déconnecter</a>

            </li>
        </ul>
    </nav>
    <nav >
        <ul class="nav nav-tabs nav-pills nav-fill" id="admin-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link" role="tab" data-toggle="tabBord" aria-controls="tabBord" href="#tabBord" aria-selected="false">Tableau de bord</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" role="tab" data-toggle="senior" aria-controls="senior" href="#senior" aria-selected="false">Seniors</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" role="tab" data-toggle="newSenior" aria-controls="newSenior" href="#newSenior" aria-selected="false">Nouveau senior</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" role="tab" data-toggle="junior" aria-controls="junior" href="#junior" aria-selected="false">Juniors</a>
            </li>
        </ul>
    </nav>
    <div class="container statsRequests" style="margin-top: 50px;">
        <section class="page_admin" id="tabBord">
            <div class="wrapper" id="page-wrapper">
                <div class="container statsRequests" id="content" tabindex="-1">
                    <div class="row">
                        <div id="stat1">

                        </div>
                        <div id="stat2">

                        </div>
                        <div id="stat3">

                        </div>
                    </div>
                </div>
            </div>
            <div class="container" id="requestsSenior">
            </div>
        </section>
        <div class="container">
            <section class="page_admin" id="senior">

            </section>
        </div>
        <div class="container">
            <section class="page_admin" id="newSenior">
            </section>
        </div>
        <div class="container">
            <section class="page_admin" id="junior">

            </section>
        </div>
        <div class="container" id="juniorsMatch">
            <section class="page_admin" id="rappel">

            </section>
            <section class="page_admin" id="listJuniorsMatch">

            </section>
        </div>

    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.3.3/backbone-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/fr-ch.js"></script>
    <script src="{{ asset('js/backendSec.js') }}"></script>
</body>

</html>