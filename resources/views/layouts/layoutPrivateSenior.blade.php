<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
  <title>Generation Connect - @yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css" />
  <link href="{{ asset('css/privateSenior.css') }}" rel="stylesheet">
</head>

<body>
  <nav>
    <div class="bg-dark navbar-dark fixed-top d-flex flex-column flex-md-row align-items-center p-3 px-md-4 navbar-expand-sm">
      <a href="{{url('')}}">
        <img class="my-0 mr-md-auto navbar-brand logo" src="img/LogoGenerationConnect.png" style="width:3.2em;" />
      </a>
      <div class="navbar my-2 my-md-0 mr-md-3 ml-auto align-items-center"></div>
      <a class="btn btn-outline-primary btn-connexion" href="{{url('logout')}}">Se déconnecter</a>
    </div>
  </nav>
  <nav>
    <ul class="nav nav-tabs nav-pills nav-fill" id="privateSenior-tab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="profil-tab" data-toggle="tab" href="#profil" role="tab" aria-controls="profil" aria-selected="true">Mon Profil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="demande-tab" data-toggle="tab" href="#demande" role="tab" aria-controls="demande" aria-selected="false">Mes demandes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="newDemande-tab" data-toggle="tab" href="#newDemande" role="tab" aria-controls="newDemande" aria-selected="false">Nouvelle demande</a>
      </li>
    </ul>
  </nav>
  @yield("app_senior")
  <footer class="mt-5">

  </footer>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.11/handlebars.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/fr-ch.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.0/multiple-select.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/i18n/datepicker.fr-FR.min.js"></script>
  <script src="{{ asset('js/all.js') }}"></script>

</body>

</html>