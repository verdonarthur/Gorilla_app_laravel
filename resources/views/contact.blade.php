@extends('layouts.layoutPublic') 
@section('main')
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2745.366213902998!2d6.635264215502447!3d46.52065467912768!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478c2e343ec18e35%3A0x92af983e94a876b1!2sRue+de+Langallerie+1%2C+1003+Lausanne!5e0!3m2!1sfr!2sch!4v1528454896985" width="100%" height="400px" id="googleMap" frameborder="0" style="border:0" allowfullscreen></iframe>
<div class="container-fluid container-fluid-contact bg-grey">
  <div class="row">
    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
    <h2 class="title_contact">Contact informations</h2>
      <p class="p_contact"><b>Generation Connect</b></br>Rue de Langallerie 1</br>1003 Lausanne</p>
      <p class="p_contact">Tel. :  021 323 40 50 </p>
      <p class="p_contact">E-mail : generationconnect@info.com</p>
    </div>
    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
    <h2 class="title_contact">Formulaire de contact</h2>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-group">
          <input class="form-control" id="name" name="name" placeholder="Nom" type="text" required>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-group">
          <input class="form-control" id="name" name="name" placeholder="Prénom" type="text" required>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-group">
          <input class="form-control" id="email" name="email" placeholder="E-mail" type="email" required>
        </div>
      </div>
      <textarea class="form-control" id="comments" name="comments" placeholder="Commentaire" rows="5"></textarea><br>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
          <button class="btn btn-default pull-right btn-contact" type="submit">Envoyer</button>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection