@extends('layouts.layoutPrivate')

@section('main')
<h6>Je souhaite postuler et rejoindre Generation Connect</h6>

<!-- Formulaire-->
<section>
<form class="form-in" id="registrationFormJ" method="post" action="{{ url('/api/v1/junior') }}">
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Genre*</label>
        <div class="col-lg-3">
            <select class="form-control" id="genderJ">
                <option selected disabled>Genre</option>
                <option value="F">Femme</option>
                <option value="H">Homme</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="lastNameJ" class="col-sm-2 col-form-label">Nom*</label>
        <div class="col-lg-3">
            <input type="text" class="form-control" id="lastNameJ" name="lastNameJ" value="{{ old('lastName') }}" placeholder="Nom" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="firstNameJ" class="col-sm-2 col-form-label">Prénom*</label>
        <div class="col-lg-3">
            <input type="text" name="firstNameJ" class="form-control" id="firstNameJ" value="{{ old('firstName') }}" placeholder="Prénom" required>
        </div>
    </div>
    <div class="form-group row" data-provide="datepicker junior">
        <label for="birthJ" class="col-sm-2 col-form-label">Age</label>
        <div class="col-lg-2">
            <input type="text" name="birthJ" class="form-control" id="birthJ" value="{{ old('birth') }}" placeholder="aaaa-mm-jj">
        </div>
    </div>
    <div class="form-group row">
        <label for="streetJ" class="col-sm-2 col-form-label">Adresse*</label>
        <div class="col-lg-3">
            <input type="text" name="streetJ" class="form-control" id="streetJ" value="{{ old('street') }}" placeholder="Adresse" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="streetNumberJ" class="col-sm-2 col-form-label">Numéro de rue*</label>
        <div class="col-lg-1">
            <input type="text" name="streetNumberJ" class="form-control" id="streetNumberJ" value="{{ old('streetNumber') }}">
        </div>
    </div>
    <div class="form-group row">
        <label for="cityJ" class="col-sm-2 col-form-label">Ville*</label>
        <div class="col-lg-2">
            <input type="text" name="cityJ" class="form-control" id="cityJ" value="{{ old('city') }}" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="NPAJ" class="col-sm-2 col-form-label">NPA*</label>
        <div class="col-sm-1">
            <input type="text" name="NPAJ" class="form-control" id="NPAJ" value="{{ old('NPA') }}" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="phoneJ" class="col-sm-2 col-form-label">Téléphone*</label>
        <div class="col-lg-3">
            <input type="text" name="phoneJ" class="form-control" id="phoneJ" value="{{ old('phone') }}" placeholder="Téléphone" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="emailJ" class="col-sm-2 col-form-label">Email*</label>
        <div class="col-lg-3">
            <input type="email" name="emailJ" class="form-control" id="emailJ" value="{{ old('email') }}" placeholder="Email" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="passwordJ" class="col-sm-2 col-form-label">Mot de passe*</label>
        <div class="col-lg-3">
            <input type="password" name="passwordJ" class="form-control" id="passwordJ" value="{{ old('password') }}" placeholder="Mot de passe" required>
        </div>
    </div>
<!--    <div class="form-group row">
        <label for="file" class="col-sm-2 col-form-label">Joindre votre CV*</br>(doc, docx, odt ou pdf)</label>
        <div class="col-lg-3">
            <div class="form-group">
                <input type="file" class="form-control-file" id="file">
            </div>
        </div>
    </div>-->
    <div class="d-flex justify-content-center btn-pad">
        <button type="submit" class="center-btn btn btn-primary">S'enregistrer</button>
    </div>
</form>
</section>
<!--<section>
    <div class="form-group row">
        <table class="table">
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Lundi</th>
                <th scope="col">Mardi</th>
                <th scope="col">Mercredi</th>
                <th scope="col">Jeudi</th>
                <th scope="col">Vendredi</th>
                <th scope="col">Samedi</th>
                <th scope="col">Dimanche</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Matin</th>
                <td><i class="fa fa-times fa-2x" aria-hidden="true"></i></td>
                <td><i class="fa fa-times fa-2x" aria-hidden="true"></i></td>
                <td><i class="fa fa-times fa-2x" aria-hidden="true"></i></td>
                <td><i class="fa fa-times fa-2x" aria-hidden="true"></i></td>
                <td><i class="fa fa-check fa-2x" aria-hidden="true"></i></td>
                <td><i class="fa fa-check fa-2x" aria-hidden="true"></i></td>
                <td><i class="fa fa-times fa-2x" aria-hidden="true"></i></td>
            </tr>
            <tr>
                <th scope="row">Après-midi</th>
                <td><i class="fa fa-times fa-2x" aria-hidden="true"></i></td>
                <td><i class="fa fa-times fa-2x" aria-hidden="true"></i></td>
                <td><i class="fa fa-times fa-2x" aria-hidden="true"></i></td>
                <td><i class="fa fa-check fa-2x" aria-hidden="true"></i></td>
                <td><i class="fa fa-times fa-2x" aria-hidden="true"></i></td>
                <td><i class="fa fa-times fa-2x" aria-hidden="true"></i></td>
                <td><i class="fa fa-times fa-2x" aria-hidden="true"></i></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="d-flex justify-content-center btn-pad">
        <button type="submit" class="center-btn btn btn-primary">S'enregistrer</button>
    </div>
</section>-->

</div>


@endsection


