@extends('layouts.layoutPublic') @section('main')
<!-- Image Junior + Senior-->
<div class="row1">
    <div class="column1">
        <img class="imgG" src="img/img-gauche.jpg" border="0" style="width:100%">
        <a href="{{url('Jhomepage')}}">
            <button class="btn btn-primary my-2" id="btn1">Je suis un Junior,</br> je veux aider !</button>
        </a>
    </div>
    <div class="column2">
        <img class="imgD" src="img/img-droite.jpg" border="0" style="width:100%">
        <a href="{{url('Shomepage')}}">
            <button class="btn btn-primary my-2" id="btn2">Je suis un Senior,</br> j'ai besoin d'aide !</button>
        </a>
    </div>
</div>



<!-- Qui sommes nous-->
<div class="container-fluid text-center shadow">
    <div class="row content this">
        <div class="text-center1">
            <h1 class="intro">Qui sommes-nous ?</h1>
            <p class="intro-text">Tout justement créé, l'entreprise Generation Connect s'installe sur le marché Suisse Romand. Notre objectif est
                de venir en aide aux Seniors dans l'utilisation d'outils technologiques à travers diverses prestations de
                services fournies par des Juniors de confiance. L’interaction humaine est au cœur de nos préoccupations dans
                les nombreux aspects de notre activité. Pour cette raison, tous nos Juniors ont suivi une formation sur les
                compétences sociales et les services proposés pour offrir des prestations de qualités aux Seniors.
                </br>
                </br>Notre approche repose sur les 3 fondamentaux suivants :
                </br>
                <strong>bienveillance, respect et écoute de l'autre.</strong>
            </p>
        </div>
    </div>
</div>

<!-- Nos prestations-->
<!--
<div class="card-box shadow">
    <h1 class="text-center text-center1 card-box-title">Nos prestations</h1>
    <div class="card-deck card-deck-h">
        <div class="shadow card">
            <img class="card-img-top" src="img/iconT.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Hotline</h5>
                <h5 class="card-text">Hotline pour nos Seniors avec une assistance suivie, nécessaire, d'une visite à domicile dans les 48h.</p>
            </div>
        </div>
        <div class="shadow card">
            <img class="card-img-top" src="img/iconH.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Visite à Domicile</h5>
                <h5 class="card-text">Selon le forfait de cotisation sélectionné, des visites régulières sont effectués par nos agents.</h5>
            </div>
        </div>
        <div class="shadow card">
            <img class="card-img-top" src="img/iconR.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Prestations Spécialisées</h5>
                <h5 class="card-text">Grâce à nos nombreux partenaires, vous profiterez d'un réseau de prestataires de services spécialisés à des
                    tarifs avantageux</h5>
            </div>
        </div>
    </div>
</div>
-->
<div class="box-shadow shadow">
    <div class="container prestations">
        <h1 class="intro text-center text-center1 card-box-title">Nos prestations</h1>
        <div class="card-deck mb-3 text-center ">
            <div class="card mb-4 box-shadow shadow">
                <div class="card-header hotline">
                    <img class="card-img-top" src="img/iconT.png" alt="Card image cap">
                </div>
                <div class="card-body">
                    <h5 class="card-title pricing-card-title">Hotline</h5>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Hotline pour nos Seniors avec une assistance suivie, nécessaire, d'une visite à domicile dans les
                            48h.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card mb-4 box-shadow shadow">
                <div class="card-header home">
                    <img class="card-img-top" src="img/iconH.png" alt="Card image cap">
                </div>
                <div class="card-body">
                    <h5 class="card-title pricing-card-title">Visite à domicile</h5>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Selon le forfait de cotisation sélectionné, des visites régulières sont effectués par nos agents.</li>
                    </ul>
                </div>
            </div>
            <div class="card mb-4 box-shadow shadow">
                <div class="card-header special">
                    <img class="card-img-top" src="img/iconR.png" alt="Card image cap">
                </div>
                <div class="card-body">
                    <h5 class="card-title pricing-card-title">Prestations Spécialisées</h5>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Grâce à nos nombreux partenaires, vous profiterez d'un réseau de prestataires de services spécialisés
                            à des tarifs avantageux</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Slideshow domaines-->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item carousel-img active">
            <img class="d-block w-100" src="img/technology-senior.jpg" alt="First slide">
            <div class="carousel-caption d-none d-md-block">
            </div>
        </div>
        <div class="carousel-item carousel-img">
            <img class="d-block w-100" src="img/senior-woman.jpg" alt="Second slide">
        </div>
        <div class="carousel-item carousel-img">
            <img class="d-block w-100" src="img/young-old.jpg" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- Nos Agents-->
<div class="box-shadow shadow">
    <div class="container ">
        <h1 class="intro text-center text-center1 card-box-title">Des agents de confiances</h1>
        <div class="card-deck mb-3 text-center ">
            <div class="card mb-4 box-shadow shadow">
                <div class="card-header agents-fond">
                    <span class="fa-stack fa-3x circle-icon-formation">
                        <i class="fa fa-graduation-cap fa-stack-1x icon_color"></i>
                    </span>
                </div>
                <div class="card-body">
                    <h5 class="card-title pricing-card-title">Formation spécialisée</h5>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Tous nos agents ont suivi une formation spécifique pour leurs donner les meilleurs outils afin de
                            vous venir en aide.</li>
                    </ul>
                </div>
            </div>
            <div class="card mb-4 box-shadow shadow">
                <div class="card-header agents-fond">
                    <span class="fa-stack fa-3x circle-icon-dispo">
                        <i class="fa colour fa-clock-o  fa-stack-1x icon_color"></i>
                    </span>
                </div>
                <div class="card-body">
                    <h5 class="card-title pricing-card-title">Disponibles</h5>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Dans les 48 h suivant votre appel, l’un de nos agents sera chez vous. </li>
                    </ul>
                </div>
            </div>
            <div class="card mb-4 box-shadow shadow">
                <div class="card-header agents-fond">
                    <span class="fa-stack fa-3x circle-icon-competent">
                        <i class="fa colour fa-cog fa-stack-1x icon_color"></i>
                    </span>
                </div>
                <div class="card-body">
                    <h5 class="card-title pricing-card-title">Compétents</h5>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Nous sélectionnons nos collaborateurs afin qu’ils aient toutes les compétences tant informatiques
                            que sociales.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- mettre les scripts-->
@endsection