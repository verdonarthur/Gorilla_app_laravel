@extends('layouts.layoutPublic') @section('main')
<div class="container">
    <div class="bar-progress">
        <div class="new_demande">
            <div class="paddingTop container-progreebar">
                <ul class="progressbarSenior">
                    <li id="active-progressbar">S'enregistrer</li>
                    <li id="active-progressbar">Disponibilités</li>
                    <li id="active-progressbar">Connaissances et compétences</li>
                    <li id="active-progressbar">Récapitulatif</li>
                    <li>Confirmation</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row" style="width: 100%"></div>
<div class="container">
    <div class="row">
    @verbatim
    <div id="recapHandlebars" class="col-6"></div>
    <!-- récapitulatif infos générales-->
    
<script id="api-junior-summary" type="text/x-handlebars-template">
    <div class="row profilJ">

        <div class="card-summaryJ">
            <h4 class="card-title">Informations générales</h4>
            <div class="card">
                <div class="card-header">
                    <h2 class="title_juniorSummary">{{user.firstName}} {{user.lastName}}</h2>
                </div>
                <div class="card-body">
                    <ul class="list-group mb-3">
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h7 class="my-0">Nom</h7>
                                <small class="text-infoG">{{user.lastName}}</small>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h7 class="my-0">Prénom</h7>
                                <small class="text-infoG">{{user.firstName}}</small>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h7 class="my-0">Rue et numéro</h7>
                                <small class="text-infoG">{{user.address.street}} {{user.address.streetNumber}}</small>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h7 class="my-0">Ville</h7>
                                <small class="text-infoG">{{user.address.city}}</small>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h7 class="my-0">Code postale</h7>
                                <small class="text-infoG">{{user.address.NPA}}</small>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h7 class="my-0">Téléphone</h7>
                                <small class="text-infoG">{{user.phone}}</small>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h7 class="my-0">E-mail</h7>
                                <small class="text-infoG">{{user.email}}</small>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        </script>
        @endverbatim
        @verbatim
        <!-- connaissances-->
        <div id="skills-handlebar" class="col-6" style="margin-top:50px"></div>
        
        <script id="api-skills" type="text/x-handlebars-template">
        <div id="sect3">
            <h4 class="card-title">Connaissances & compétences</h4>

            <ul class="chec-radio">
           {{#each this}}
                <li >
                {{name}}
                </li>
                {{/each}}
            </ul>
        </div>
</script>
@endverbatim
</div>
</div>
    <!-- récapitulatif disponibilité-->
    <div id="dispoHandlebars"></div>
    <div class="container">
    <div class="col-12" id="sect2">
        
       @verbatim
        <!-- connaissances-->
        <div id="dispos-handlebar"></div>

        <script id="api-dispos" type="text/x-handlebars-template">
        <div id="sect3">
            <h4 class="card-title">Disponibilités</h4>

            <ul class="chec-radio">
           {{#each this}}
                <li >
                Jour: {{day}} De: {{from}} A: {{to}}
                </li>
                {{/each}}
            </ul>
        </div>
</script>
@endverbatim
    </div>
    <!-- Bouton retour suivant-->
    <div class="d-flex pad justify-content-around">
        <a href="{{url('Jcompetences')}}">
        <button type="button" class=" btn btn-primary">Retour</button>
        </a>
        <a href="{{url('JconfirmationPostulation')}}">
        <button type="submit" class="btn btn-primary">Suivant</button>
        </a>
    </div>
</div>
@endsection