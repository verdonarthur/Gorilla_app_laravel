@extends('layouts.layoutPublic')

@section('main')
<div>
<h6 class="pad justify-content-center text-center1 ok">Confirmation</h6>
<div class="comp-text d-flex pad justify-content-center text-center1">
    Merci d'avoir validé votr e-mail! Vous avez bien été enregistré. <br/>
    Vous allez maintenant être redirigé vers le login.
    <br/>
    Pour accéder à votre profil utilisateur, merci de vous connecter.
</div>
<div id="btn" class="d-flex pad justify-content-center row-h ">
    <a href="{{url('Connexion')}}">
                <button id="btn-inscription" class="btn btn-lg btn-primary" type="button">Suivant</button>
            </a>
</div>
</div>
@endsection