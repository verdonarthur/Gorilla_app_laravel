@extends('layouts.layoutAdmin') @section('main')
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
    Demande
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Nouvelle demande</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="subtitle-modal">Décrivez le problème du Senior : </label>
                    <textarea class="form-control" rows="2" placeholder="Décrivez ici le problème..."></textarea>
                </div>
                <div class="form-group">
                    <label class="subtitle-modal">Type de demande : </label>
                            <select id="newDemande-skills" class="custom-select" style="width:100%;height: 80%;" multiple>
            
                            </select>
                </div>
                    <label class="subtitle-modal">Disponibilités : </label>
                <div class="form-row">
                        <div class="col-auto">
                            <label>De : </label>
                            <input type="date" id="newDemande-dateFrom" class="form-control mb-2" placeholder="" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                                value="2018-12-02">
                            <input type="time" id="newDemande-timeFrom" class="form-control mb-2" placeholder="" value="12:00">
                        </div>
                        <div class="col-auto">
                            <label>A : </label>
                            <input type="date" id="newDemande-dateTo" class="form-control mb-2" placeholder="" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="2018-12-02">
                            <input type="time" id="newDemande-timeTo" class="form-control mb-2" placeholder="" value="13:00">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Enregistrer la demande</button>
            </div>
        </div>
    </div>
</div>
@endsection