@extends('layouts.layoutAdmin') @section('main')

<!-- ONGLET SENIOR -->
<!-- filtre -->
<!-- <div class="filtre">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filtrer par</button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Nom</a>
                <a class="dropdown-item" href="#">Prénom</a>
                <a class="dropdown-item" href="#">Identifiant</a>
            </div>
        </div>
        <input type="text" class="form-control" aria-label="Text input with dropdown button">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="button">Rechercher</button>
        </div>
    </div>
</div> -->
<!--tableau senior-->
<table class="table tableau-admin">
    <caption class="caption-dashboard">Seniors inscrits</caption>
    <thead>
        <tr>
            <th scope="col">Identifiant</th>
            <th scope="col">Nom </th>
            <th scope="col">Prénom</th>
            <th scope="col">Adresse</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>123</td>
            <td>Tomballe</td>
            <td>Arielle</td>
            <td>Rue du Bugnon 55, 1020 Renens</td>
            <td>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-secondary">Demande</button>
                    <button type="button" class="btn btn-secondary">Détails</button>
                </div>
            </td>
        </tr>
    </tbody>
</table>
@endsection