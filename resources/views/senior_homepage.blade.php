@extends('layouts.layoutPublic') @section('main')
<!-- prestations-->
<div class="container marketing">
    <div>
        <h3 class="title-senior">Les services à domicile proposés</h3>
        <h5 class="title-text"> Installations & mise à jour</h5>
        <div class="row">
            <div class="col-lg-4">
                <span class="fa-stack fa-3x circle-icon">
                    <i class="fa colour fa-television fa-stack-1x icon_color"></i>
                </span>
                <h2 class="titre-service">Télévision digitale</h2>
                <ul>
                    <li>Installation ou dépannage</li>
                    <li>Swisscom box</li>
                    <li>Réglages chaînes sur le réseau Swissom</li>
                </ul>
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <span class="fa-stack fa-3x circle-icon">
                    <i class="fa colour fa-phone fa-stack-1x icon_color"></i>
                </span>
                <h2 class="titre-service">Téléphonie digital</h2>
                <ul>
                    <li>Installation routeur</li>
                </ul>
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <span class="fa-stack fa-3x circle-icon">
                    <i class="fa colour fa-wifi fa-stack-1x icon_color"></i>
                </span>
                <h2 class="titre-service">Connexion internet</h2>
                <ul>
                    <li>Installation Internet par routeur DSL</li>
                </ul>
            </div>
            <div class="col-lg-4">
                <span class="fa-stack fa-3x circle-icon">
                    <i class="fa fa-lock fa-stack-1x icon_color"></i>
                </span>
                <h2 class="titre-service">Installations de sécurité</h2>
                <ul>
                    <li>Robot d'accompagnement</li>
                    <li>Protection de données</li>
                </ul>
            </div>
            <div class="col-lg-4">
                <span class="fa-stack fa-3x circle-icon">
                    <i class="fa colour fa-mobile fa-stack-1x icon_color"></i>
                </span>
                <h2 class="titre-service">Smartphones</h2>
                <ul>
                    <li>Windows Mac Linux</li>
                    <li>Installation de programmes</li>
                    <li>Navigation internet</li>
                </ul>
            </div>
            <div class="col-lg-4">
                <span class="fa-stack fa-3x circle-icon">
                    <i class="fa colour fa-desktop fa-stack-1x icon_color"></i>
                </span>
                <h2 class="titre-service">Ordinateurs</h2>
                <ul>
                    <li>Mise à jour Smartphones et applications</li>
                    <li>Installation d'application</li>
                </ul>
            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->
    </div>
</div>
<!-- Nos agents-->
<div class="bg-light">
    <div class="container ">
        <h1 class="intro text-center text-center1 card-box-title">Des agents de confiances</h1>
        <div class="card-deck mb-3 text-center ">
            <div class="card mb-4 box-shadow shadow">
                <div class="card-header agents-fond">
                    <span class="fa-stack fa-3x circle-icon-formation">
                        <i class="fa fa-graduation-cap fa-stack-1x icon_color"></i>
                    </span>
                </div>
                <div class="card-body">
                    <h5 class="card-title-formation pricing-card-title">Formation spécialisée</h5>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Tous nos agents ont suivi une formation spécifique pour leurs donner les meilleurs outils afin de
                            vous venir en aide.</li>
                    </ul>
                </div>
            </div>
            <div class="card mb-4 box-shadow shadow">
                <div class="card-header agents-fond">
                    <span class="fa-stack fa-3x circle-icon-dispo">
                        <i class="fa colour fa-clock-o  fa-stack-1x icon_color"></i>
                    </span>
                </div>
                <div class="card-body">
                    <h5 class="card-title pricing-card-title">Disponibles</h5>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Dans les 48 h suivant votre appel, l’un de nos agents sera chez vous. </li>
                    </ul>
                </div>
            </div>
            <div class="card mb-4 box-shadow shadow">
                <div class="card-header agents-fond">
                    <span class="fa-stack fa-3x circle-icon-competent">
                        <i class="fa colour fa-cog fa-stack-1x icon_color"></i>
                    </span>
                </div>
                <div class="card-body">
                    <h5 class="card-title pricing-card-title">Compétents</h5>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Nous sélectionnons nos collaborateurs afin qu’ils aient toutes les compétences tant informatiques
                            que sociales.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- infos supplémentaires-->
<div class="container">
    <h3 class="title-senior-service">Un service sécurisé et individualisé</h3>
    <!-- Example row of columns -->
    <div class="row row-infos">
        <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6 text-center">
            <h2>Des services adaptés à vos besoins</h2>
            <p class="paragraphe-infos">Nos prestations permettent de vous adapter au monde digital celon vos besoins. Que ce soit pour des demandes
                concernant une incompréhention d'un outil, des demandes plus techniques ou l'installation d'équipements informatiques,
                nous nous tenons prêts à vous aider.</p>
        </div>
        <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6 text-center">
            <h2>Un agent unique</h2>
            <p class="paragraphe-infos">Nos agents sont tous de jeunes étudiants motivés à vous aider. Nous prenons très à coeur notre mission de soutien
                digital, c'est pourquoi nos Juniors sont tous et toutes spécialement choisis pour pouvoir répondre à votre
                demande.
            </p>
        </div>
        <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6 text-center">
            <h2>Des prestations de qualités</h2>
            <p class="paragraphe-infos">Les prestations fournis sont d'une qualité optimale. Il est important pour nous que votre expérience soit la
                plus plaisante possible c'est pourquoi nous formons nos Juniors dans cette optique.</p>
        </div>
        <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6 text-center">
            <h2>Evaluation de chaque préstation</h2>
            <p class="paragraphe-infos">Pour nous permettre de faire évoluer nos services et notre plateforme, nous avons besoin de vous ! Car vous pouvez
                faire évoluer nos prestations en évaluant chaque prestation effectué par nos Juniors. Ces retours nous permettre
                d'adapter notre offre pour qu'elle convienne à vos envies ! </p>
        </div>
    </div>
</div>


<div class="bg-light">
    <div class="container">
        <h3 class="title-senior-service">Nos tarifs</h3>
        <div class="card-deck text-center">
            <div class="card">
                <div class="card-header">A l'heure</div>
                <div class="card-body">
                    <h5 class="card-title">Hotline</h5>
                    <div class="card-text">
                        Service spécialisé à la demande</div>
                    <div class="card-text">Payez à la demande effectué.</div>
                </div>
                <div class="card-footer card-mod">XX.-</div>
            </div>
            <div class="card">
                <div class="card-header">Light</div>
                <div class="card-body">
                    <h5 class="card-title">Hotline</h5>
                    <div class="card-text">Service spécialisé à la demande</div>
                    <div class="card-text">
                        2 visites à domicile par an offertes
                    </div>
                </div>
                <div class="card-footer card-mod">XX.-</div>
            </div>
            <div class="card">
                <div class="card-header">Medium</div>
                <div class="card-body">
                    <h5 class="card-title">Hotline</h5>
                    <div class="card-text">Service spécialisé à la demande</div>
                    <div class="card-text">
                        1 visite à domicile par mois
                    </div>
                </div>
                <div class="card-footer card-mod">XX.-</div>
            </div>
            <div class="card">
                <div class="card-header">Premium</div>
                <div class="card-body">
                    <h5 class="card-title">Hotline</h5>
                    <div class="card-text">Service spécialisé à la demande</div>
                    <div class="card-text">
                        1 visite à domicile par semaine
                    </div>
                </div>
                <div class="card-footer card-mod">XX.-</div>
            </div>
        </div>
        <a href="{{url('Sformulaire')}}">
            <button id="btn-inscription" class="btn btn-lg btn-primary" type="button">Inscrivez-vous</button>
        </a>
    </div>
</div>
@endsection