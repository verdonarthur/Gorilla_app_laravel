@extends('layouts.app') @section('content')
<section class="container">
    <input id="valLogin" type="text" class="form-control" placeholder="login">
    <br>
    <input id="valPass" type="text" class="form-control" placeholder="password">
    <br>

    <input id="login" class="btn" type="button" value="login">

    <script>
        $(document).ready(function () {
            $("#login").click(function () {
                $.post("{{ url('/api/v1/login') }}",
                    { email: $("#valLogin").val(), password: $("#valPass").val() },
                    function (data) {
                        console.log(data);
                    }).fail(function (err) {
                        console.log(err.responseJSON);
                    });
            });
        });
    </script>
</section>
<br>
<section class="container">
    <input id="url" type="text" class="form-control" placeholder="url api" value="http://localhost/gorilla/">
    <br>
    <textarea id="body" class="form-control" placeholder="request body"></textarea>
    <br>
    <div class="input-group">
        <select class="custom-select" id="requestType">
            <option value="GET">GET</option>
            <option value="POST">POST</option>
            <option value="PUT">PUT</option>
            <option value="DELETE">DELETE</option>
        </select>
        <div class="input-group-append">
            <button id="send" class="btn" type="button">Send</button>
        </div>
    </div>
    <script>

        $(document).ready(function () {
            $("#send").click(function () {
                var newText = $('textarea').val();
                
                if(newText != ""){
                    var newText = JSON.parse(newText);
                    console.log(newText);
                }

                $.ajax({
                    method: $('#requestType').val(),
                    url: $('#url').val(),
                    data: newText
                }).done(function (msg) {
                    console.log(msg);
                }).fail(function(msg){
                    console.log(msg.responseJSON);
                });
            });
        });
    </script>
</section>
@endsection