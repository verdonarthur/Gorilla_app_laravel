@extends('layouts.layoutPublic') @section('main')
<!-- Progress Bar dispo-->
<div class="container">
    <div class="bar-progress">
        <div class="new_demande">
            <div class="paddingTop container-progreebar">
                <ul class="progressbarSenior">
                    <li id="active-progressbar">S'enregistrer</li>
                    <li id="active-progressbar">Disponibilités</li>
                    <li>Connaissances et compétences</li>
                    <li>Récapitulatif</li>
                    <li>Confirmation</li>
                </ul>
            </div>
        </div>
    </div>

    <h3 class="title-dispo">Emploi du temps</h6>
        <div class="conf-text-dispo">Indiquez les périodes de la semaine où vous êtes disponible.</div>
       
        <!-- Tableau dispo-->
        <table class="table">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Lundi</th>
                    <th scope="col">Mardi</th>
                    <th scope="col">Mercredi</th>
                    <th scope="col">Jeudi</th>
                    <th scope="col">Vendredi</th>
                    <th scope="col">Samedi</th>
                    <th scope="col">Dimanche</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">Matin</th>
                    <td>
                        <i class="fa fa-times fa-2x" aria-hidden="true" title="monday am"></i>
                    </td>
                    <td>
                        <i class="fa fa-times fa-2x" aria-hidden="true" title="tuesday am"></i>
                    </td>
                    <td>
                        <i class="fa fa-times fa-2x" aria-hidden="true" title="wednesday am"></i>
                    </td>
                    <td>
                        <i class="fa fa-times fa-2x" aria-hidden="true" title="thursday am"></i>
                    </td>
                    <td>
                        <i class="fa fa-check fa-2x" aria-hidden="true" title="friday am"></i>
                    </td>
                    <td>
                        <i class="fa fa-check fa-2x" aria-hidden="true" title="saturday am"></i>
                    </td>
                    <td>
                        <i class="fa fa-times fa-2x" aria-hidden="true" title="sunday am"></i>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Après-midi</th>
                    <td>
                        <i class="fa fa-times fa-2x" aria-hidden="true" title="monday pm"></i>
                    </td>
                    <td>
                        <i class="fa fa-times fa-2x" aria-hidden="true" title="tuesday pm"></i>
                    </td>
                    <td>
                        <i class="fa fa-times fa-2x" aria-hidden="true" title="wednesday pm"></i>
                    </td>
                    <td>
                        <i class="fa fa-check fa-2x" aria-hidden="true" title="thursday pm"></i>
                    </td>
                    <td>
                        <i class="fa fa-times fa-2x" aria-hidden="true" title="friday pm"></i>
                    </td>
                    <td>
                        <i class="fa fa-times fa-2x" aria-hidden="true" title="saturday pm"></i>
                    </td>
                    <td>
                        <i class="fa fa-times fa-2x" aria-hidden="true" title="sunday pm"></i>
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- Bouton retour suivant-->
        <div class="d-flex pad justify-content-around">
            <a href="{{url('Jconfirmation')}}">
                <button type="button" class=" btn btn-primary">Retour</button>
            </a>
            
            <button type="submit" class="btn btn-primary" id="sendAvailabilities">Suivant</button>
           
        </div>
        
</div>
@endsection