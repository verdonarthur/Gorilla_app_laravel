@extends('layouts.layoutPublic') @section('main')
<!-- Progress Bar dispo-->
<div class="container">
    <div class="bar-progress">
        <div class="new_demande">
            <div class="paddingTop container-progreebar">
                <ul class="progressbarSenior">
                    <li id="active-progressbar">S'enregistrer</li>
                    <li id="active-progressbar">Disponibilités</li>
                    <li id="active-progressbar">Connaissances et compétences</li>
                    <li>Récapitulatif</li>
                    <li>Confirmation</li>
                </ul>
            </div>
        </div>
    </div>

    <h3 class="title-knowledges">Connaissances et compétences</h6>
        <p class="comp-text">Ajouter vos connaissances informatiques en sélectionnant les tags ci-dessous :</p>
        <!-- Checkbox compétences-->
        <ul class="chec-radio" id="template_skill">
            <li class="pz template template_skill">
                <label class="radio-inline">
                    <button type="button" class="btn pro-chx skill_name"></button>
                </label>
            </li>
        </ul>
</div>

<!-- Bouton retour suivant-->
<div class="d-flex pad justify-content-around">
    <a href="{{url('Javaibility')}}">
        <button type="button" class=" btn btn-primary">Retour</button>
    </a>
    
    <button type="submit" class="btn btn-primary" id="sendSkills">Suivant</button>
    
</div>
@endsection