@extends('layouts.layoutAdmin') @section('main')
<!--Junior disponible PAGE MATCH-->
<div class="container">
<h5>Juniors disponibles pour la demande sélectionnée</h5>
<div class="rappel">
    <div class="row">
        <div class="col-md-8">
            <h2 class="title-rappel">Informations Senior</h2>
            <p>Nom du Senior : Arielle Domballe</p>
            <p>Problème : Installation routeur</p>
            <p>Description : j'aimerais bien que ça soit une fille qui vienne.</p>
            <p>Adresse : Rue du Bugnon 55, 1020 Renens </p>
            <p>Date : 12.06.2018</p>
        </div>
    </div>
</div>
<!-- <div class="filtre">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filtrer par</button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Nom</a>
                <a class="dropdown-item" href="#">Prénom</a>
                <a class="dropdown-item" href="#">Identifiant</a>
            </div>
        </div>
        <input type="text" class="form-control" aria-label="Text input with dropdown button">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="button">Rechercher</button>
        </div>
    </div>
</div> -->
    <div class="list-group">
        <div class="list-group-item list-group-item-action flex-column align-items-start active">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1 junior-match">Thimothé Bongars</h5>
                <b>Distance : 5 km</b>
            </div>
            <p class="mb-1">Age : 22</p>
            <p class="mb-1">E-mail : thimo.bongars@gmail.com</p>
            <p class="mb-1">Téléphone : 079 475 06 24</p>
            <small>
                <b>Adresse : </b> Rue du Lac 54, 1020 Renens</small>
                <button type="submit" class="btn btn-primary">Assigner Junior</button>
            </div>
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1 junior-match">Marina Popov</h5>
                <b>Distance : 8 km</b>
            </div>
            <p class="mb-1">Age : 24</p>
            <p class="mb-1">E-mail : popov.mar@gmail.com</p>
            <p class="mb-1">Téléphone : 079 548 47 00</p>
            <small>
                <b>Adresse : </b> Rue du Lac 54, 1020 Renens</small>
                <button type="submit" class="btn btn-primary">Assigner Junior</button>
            </div>
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1 junior-match">Julie Monay</h5>
                <b>Distance : 20 km</b>
            </div>
            <p class="mb-1">Age : 22</p>
            <p class="mb-1">E-mail : julie.monay@gmail.com</p>
            <p class="mb-1">Téléphone : 079 548 47 36</p>
            <small>
                <b>Adresse : </b> Avenue de la gare, 1000 Lausanne</small>
                <button type="submit" class="btn btn-primary">Assigner Junior</button>
            </div>
    </div>
</div>
@endsection