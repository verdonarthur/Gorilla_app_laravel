@extends('layouts.layoutPrivateSenior') @section('app_senior') @verbatim
<script id="tmp-profil-senior" type="text/x-handlebars-template">
    <form class="form-in" id="profil-registrationForm" method="post" onsubmit="return false;">
        <div class="form-group">
            <label class="">Genre*</label>
            <div class="">
                <select class="form-control" id="genderS" name="gender">
                    {{#select user.gender}}
                    <option selected disabled>Genre</option>
                    <option value="F">Femme</option>
                    <option value="M">Homme</option>
                    {{/select}}
                </select>
            </div>
        </div>
        {{> simpleInput fieldName='lastName' fieldValue=user.lastName fieldNameFR='Nom*' placeholder='' required='true'}}
        {{> simpleInput fieldName='firstName' fieldValue=user.firstName fieldNameFR='Prénom*' placeholder='' required='true'}}
        {{> simpleInput fieldName='birth' fieldValue=user.birth fieldNameFR='Date de naissance' placeholder='' required='true'}}
        {{> simpleInput fieldName='street' fieldValue=user.address.street fieldNameFR='Rue*' placeholder='' required='true'}}
        {{> simpleInput fieldName='streetNumber' fieldValue=user.address.streetNumber fieldNameFR='Numéro de rue*' placeholder='' required='true'}}
        {{> simpleInput fieldName='city' fieldValue=user.address.city fieldNameFR='Ville*' placeholder='' required='true'}}
        {{> simpleInput fieldName='NPA' fieldValue=user.address.NPA fieldNameFR='NPA*' placeholder='' required='true'}}
        {{> simpleInput fieldName='phone' fieldValue=user.phone fieldNameFR='Téléphone*' placeholder='Ex: +41 78 919 94 15' required='true'}}
        {{> simpleInput fieldName='email' fieldValue=user.email fieldNameFR='Email*' placeholder='' required='true'}}
    </form>
</script>
<script id="tmp-request-requestCards" type="text/x-handlebars-template">    
    <div class="card request-card-senior">
        <div class="card-header ">{{#getRequestStateFR state}}{{/getRequestStateFR}}</div>
        <div class="card-body">
            <div class="card-text">
                <p>{{description}}</p>
            </div>
        </div>
        <ul class="list-group list-group-flush">
            {{#if juniorId}}
            <li class="list-group-item">Junior : {{junior.user.firstName}} {{junior.user.lastName}}</li>
            <li class="list-group-item">Téléphone : {{junior.user.phone}}</li>
            {{/if}}
            <li class="list-group-item">Date : {{startDate}}</li>
        </ul>
    </div>    
</script>
<script id="tmp-request-selectSkill" type="text/x-handlebars-template">    
    {{#each this}}
    <div class="btn-group-toggle" data-toggle="buttons" style="min-width:30%;text-align: center">
            <label class="btn btn-outline-primary btn-lg newDemande-skills">
              <input type="checkbox" value="{{id}}" name="newDemande-skills" autocomplete="off"> {{name}}
            </label>
          </div>
    {{/each}}
  </script>

<script id="tmp-intervention-interventionCards" type="text/x-handlebars-template">    
    <div class="card request-card-senior">
        <div class="card-header"></div>
        <div class="card-body">
            <div class="card-text">
                <p></p>
            </div>
        </div>
        <ul class="list-group list-group-flush">
            
            <li class="list-group-item">Junior : {{junior.user.firstName}} {{junior.user.lastName}}</li>
            <li class="list-group-item">Téléphone : {{junior.user.phone}}</li>
            
            <li class="list-group-item">Date : {{startDate}}</li>
        </ul>
    </div>    
</script> @endverbatim

<div id="seniorPrivate-tab-content" class="tab-content container">
    <!-- Edit profile-->
    <div id="profil" class="profilSenior tab-pane fade show active">
        <div class="row justify-content-md-center">
            <div class="col-md-5">
                <h2>Informations personnelles</h2>
                <div id="profil-info"></div>
                <div class="d-flex btn-pad">
                    <button id="profil-btnSaveProfil" type="submit" class="btn btn-primary">Sauver</button>
                </div>
            </div>
            <div class="col-md-6 offset-md-1">
                <h2>Gestion du mot de passe</h2>
                <div class="form-group">
                    <label class="">Mot de passe*</label>
                    <div class="row">
                        <div class="col">
                            <input type="password" name="password" class="form-control" id="password" placeholder="Ancien mot de passe">
                        </div>
                        <div class="col">
                            <input type="password" name="password" class="form-control" id="password" placeholder="Nouveau mot de passe">
                        </div>
                    </div>
                </div>
                <div class="d">
                    <button id="profil-btnSavePassword" class="btn btn-primary">Sauver</button>
                </div>
                <div class="alert alert-primary alert-primary-senior" role="alert">
                    Vous pouvez changer vos données personnelles en cliquant directement sur vos informations. Oubliez pas de cliquer sur sauver !
                </div>
            </div>
            <div class="col-md-6 offset-md-1">
                <div class="form-group">

                </div>
            </div>
        </div>
    </div>

    <!-- Demandes-->
    <div id="demande" class="tab-pane fade">
        <div class="container-fluid">
            <!--<h3>Intervention à valider</h3>
            <div class="row d-flex justify-content-around flex-wrap" id="intervention-rate-cards"> </div>-->
            <h3>Intervention à venir</h3>
            <div class="row d-flex justify-content-around flex-wrap" id="demande-accepted-cards"> </div>
            <h3>Demande en cours d'attribution</h3>
            <div class="row d-flex justify-content-around flex-wrap" id="demande-created-cards"> </div>
            <h3>Historique</h3>
            <div class="row d-flex justify-content-around flex-wrap" id="demande-done-cards"> </div>
        </div>
    </div>

    <!-- nouvelle demande-->
    <div id="newDemande" class="tab-pane fade container" data-step="1">
        <div class="progress" style="height: 50px">
            <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width:25%">1</div>
        </div>
        <div id="newDemande-stepRow" class="row pt-5">
            <div id="newDemande-step1" class="container">
                <h3>Décrivez-nous votre problème (1000 caractères):</h3>
                <div class="form-group col-md-8 offset-md-2">
                    <textarea class="form-control" id="newDemande-description"></textarea>
                </div>
            </div>

            <div id="newDemande-step2" class="d-none container">
                <h3>Séléctionnez les compétences requises (laissez vide si vous n'êtes pas sûr):</h3>
                <div class="form-group col-md-8 offset-md-2">
                    <div id="newDemande-skills" style="width: 100%" class="d-flex justify-content-around flex-wrap align-items-center">

                    </div>
                </div>
            </div>

            <div id="newDemande-step3" class="d-none form-row container">
                <div class="col-md-8 offset-md-2">
                    <h3>Entrez la date d'intervention :</h3>
                    <div>
                        <label>Intervention chaque semaine :</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="newDemande-isSimpleOrMultiple" value="simple" checked>
                            <label class="form-check-label">Non</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="newDemande-isSimpleOrMultiple" value="multiple">
                            <label class="form-check-label">Oui</label>
                        </div>
                        <br>
                        <label>Date de l'intervention : </label>
                        <input type="text" id="newDemande-dateFrom" class="form-control mb-2" placeholder="" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                            value="">
                        <input type="text" id="newDemande-dateTo" class="form-control mb-2" placeholder="" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="">

                        <label>Heure d'intervention : </label>
                        <div class="form-inline">
                            <div class="input-group mb-2 mr-2" style="width: 40%">
                                <div class="input-group-prepend ">
                                    <span class="input-group-text form-control-lg">De </span>
                                </div>
                                <input type="text" id="newDemande-timeFrom" class="form-control  form-control-lg" placeholder="" value="12:00">
                            </div>
                            <div class="input-group mb-2" style="width: 40%">
                                <div class="input-group-prepend">
                                    <span class="input-group-text form-control-lg">A </span>
                                </div>
                                <input type="text" id="newDemande-timeTo" class="form-control  form-control-lg" placeholder="" value="13:00">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="newDemande-step4" class="d-none container">
                <h3>Résumé de la requête</h3>
                <div id="newDemande-step4-errors"></div>
                <ul id="newDemande-step4-summary" class="list-group list-group-flush">
                    <li class="list-group-item" id="step4-summary-skills"></li>
                    <li class="list-group-item" id="step4-summary-description"></li>
                    <li class="list-group-item" id="step4-summary-date"></li>
                </ul>
            </div>
        </div>

        <div class="btn-group btn-group-lg" style="width: 100%">
            <button class="btn btn-primary" id="newDemande-previous">Précédent</button>
            <button class="btn btn-primary" id="newDemande-next">Suivant</button>
        </div>
    </div>
</div>
@endsection