@extends('layouts.layoutPublic') @section('main')
<div class="container">
    <div class="bar-progress">
        <div class="new_demande">
            <div class="paddingTop container-progreebar">
                <ul class="progressbarSenior">
                    <li id="active-progressbar">S'enregistrer</li>
                    <li id="active-progressbar">Disponibilités</li>
                    <li id="active-progressbar">Connaissances et compétences</li>
                    <li id="active-progressbar">Récapitulatif</li>
                    <li id="active-progressbar">Confirmation</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="confirmation">
        <h3 class="conf-title">Confirmation</h3>
        <div class="conf-text">
            Vous êtes sur le point de confirmer votre postulation chez Generation Connect.
            </br>
            Vous allez recevoir un e-mail de confirmation dans lequel la suite du processus d'engagement
            chez Generation Connect sera décrit.
            </br>

            Toute l'équipe de Generation Connect vous remercie de l'intérêt que vous portez à cette plateforme!
        </div>
        <a href="{{url('Jprivate')}}">
            <button id="btn-inscription" class="btn btn-lg btn-primary" type="submit">Envoyer la postulation</button>
        </a>
    </div>
</div>
@endsection