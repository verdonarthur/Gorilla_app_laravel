@extends('layouts.layoutPublic')

@section('main')
<!-- Image homepage-->
<div id="img_homepage"></div>
<!-- Texte intro-->
<div class="section1">
        <div class="container">
            <h1 class="title_homepage">Rejoignez une grande famille !</h1>
            <p class="paragraphe">Generation Connect place l'humain au centre de l'entreprise. La personne humaine est au coeur de notre société.
                Nous attachons une importance  primordiale à nos clients mais également à nos employés. </br> Un employé épanoui dans
                sa société produit un travail de qualité. </br> </br>

                Ce travail de  qualité se répercute de façon positive sur nos clients.
                C'est la raison pour laquelle tous nos candidats doivent passer un entretien avant de rejoindre notre équipe et
                suivre une formation sur les compétences sociales que nous offrons.</p>
        </div>
    </div>

<!-- Fonctionnement-->
<div class="section2">
        <div class="container">
            <h1 class="title_homepage">Un fonctionnement extrêmement simple</h1>
            <div class="row">
                <div class="col-lg-2 text-center col-centered">
                    <h4>1. Inscrivez-vous</h4>
                    <span class="fa-stack fa-4x circle-icon">
                        <i class="fas colour fa-user-plus fa-stack-1x icon_color" aria-hidden="true"></i>
                    </span>
                </div>
                <div class="col-lg-2 text-center col-centered">
                    <h4>2. Formez-vous</h4>
                    <span class="fa-stack fa-4x circle-icon">
                        <i class="fas colour fa-graduation-cap fa-stack-1x icon_color" aria-hidden="true"></i>
                    </span>
                </div>
                <div class="col-lg-2 text-center col-centered">
                    <h4>3. Tendez la main</h4>
                    <span class="fa-stack fa-4x circle-icon">
                        <i class="fas colour fa-hand-holding-heart fa-stack-1x icon_color" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
        </div>
 </div>

<!-- Pré-requis-->
<div class="section1">
    <div class="container prerequis">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="title-requis">Nous vous proposons</h1>
                    <ul>
                        <li>Une formation aux compétences sociales</li>
                        <li>Une formation aux services «Les Juniors au service des Seniors»</li>
                        <li>Une activité adaptée à vos disponibilités</li>
                        <li>Une activité rémunérée</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <h1 class="title-requis">Pré-requis pour postuler</h1>
                    <ul>
                        <li>Être majeur(e)</li>
                        <li>Casier judiciaire vierge</li>
                        <li>Connaissances et compétences pour les services proposés</li>
                        <li>Un intérêt pour valoriser vos connaissances auprès d’autres générations</li>
                    </ul>
                </div>
            </div>
        </div>
 </div>


<!-- Postulation-->
<div class="section2">
    <div class="container">
        <h1 class="title_homepage">Je veux rejoindre Generation Connect</h1>
        <p class="paragraphe">Si vous remplissez tous les pré-requis nécessaires pour devenir membre de Generation Connect, postulez en cliquant sur le lien ci-dessous.</p>
        <a href="{{url('Jform')}}"> <button id="btn-postulation" type="button" class="btn btn-primary">Postuler</button></a>
    </div>
</div>
@endsection
