@extends('layouts.layoutAdmin')
@section('main')
<!--
<div class="container">
    <h2>Se connecter</h2>
    <form class="form-horizontal" role="form" action="{{ url('/api/v1/login') }}" id="login-form">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="col-md-4 col-lg-4" for="email">Email:</label>
            <div class="col-md-4 col-lg-4">
                <input type="email" class="form-control" id="email" value="{{ old('email') }}" placeholder="Enter email"
                       name="email">
                @if ($errors->has('email'))
                   <span class="help-block">
                     <strong>{{ $errors->first('email') }}</strong>
                   </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="col-md-4 col-lg-4" for="password">Password:</label>
            <div class="col-md-4 col-lg-4">
                <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
-->
<div class="text-center connexion">
    <form class="form-signin" role="form" action="{{ url('/api/v1/login') }}" id="login-form">
        {{ csrf_field() }}
        <img class="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
        <h2 class="h3 font-weight-normal title-login">Connectez-vous !</h2>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="sr-only">E-mail</label>
            <input type="email" id="email" class="form-control" value="{{ old('email') }}" name="email" placeholder="Adresse e-mail" required autofocus>
                @if ($errors->has('email'))
                   <span class="help-block">
                     <strong>{{ $errors->first('email') }}</strong>
                   </span>
                @endif
        </div>

       <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="sr-only">Mot de passe</label>
                <input type="password" id="password" class="form-control" placeholder="Mot de passe" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
        </div>
      <button class="btn btn-lg btn-primary btn-block btn-connexion-admin" type="submit">Se connecter</button>
    </form>
  </div>
@endsection

