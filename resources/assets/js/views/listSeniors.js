var tmplDefault = require("../templates/listSeniors.handlebars");
import ModelSenior from "../models/senior";


export default Backbone.View.extend({ //comme si c'était public

    events: {
        "click .btn.btnDemande": "open_modal"
    },

    open_modal: function(evt){
        var DATE_FORMAT = "Y-MM-DD HH:mm:ss";
        console.log("choose");
        var seniorId = $(evt.target).attr("data-senior-id");
        console.log(seniorId);
        let id = this.collection.get(seniorId);
        console.log(id);

        $('#modalAddForm').modal('show');
        $('#seniorAddForm').off();

        $('#modalAddForm').on('submit', (evt) => {
            evt.preventDefault();
            var startDate = moment($('#newDemande-dateFrom').val() + " " + $('#newDemande-timeFrom').val() + ":00");
            var endDate = moment($('#newDemande-dateTo').val() + " " + $('#newDemande-timeTo').val() + ":00");

            //console.log(startDate);
            //console.log(endDate);
        });
    },

    initialize: function(attrs, options, evt) {
        console.log(attrs);
        this.$el.addClass('requests');
        this.template = attrs.template || tmplDefault;
        this.listenTo(this.collection, "change add remove", this.render);
    },

    render: function() {
        //console.log("render");
        //console.log(this.collection.toJSON());
        this.$el.html(this.template({
            seniors: this.collection.toJSON() //même nom dans le template
        }));

        return this.$el;
    }
});

/**
 * Created by Marina on 12.06.2018.
 */
