/**
 * Created by Marina on 13.06.2018.
 */

var tmplDefault = require("../templates/rappelSenior.handlebars");

export default Backbone.View.extend({

    initialize: function(attrs, options, evt) {

        console.log("senior");
        console.log(attrs);

        //var seniorId = $(evt.target).attr("data-senior-id");

        //console.log(seniorId);

        this.$el.addClass('seniors');
        this.template = attrs.template || tmplDefault;
        this.listenTo(this.collection, "change add remove", this.render);
    },

    render: function() {
        //console.log("render");
        //console.log(this.collection.toJSON());
        this.$el.html(this.template({
            seniors: this.collection.toJSON()
        }));

        return this.$el;
    }
});

/**
 * Created by Marina on 13.06.2018.
 */
