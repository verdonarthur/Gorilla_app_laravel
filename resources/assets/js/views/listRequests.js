var tmplDefault = require("../templates/listRequests.handlebars");
import ModelRequestsReceived from "../models/collectionRequestsReceived";

export default Backbone.View.extend({ //comme si c'était public

    events: {
        "click a.btn.adminBtn": "act_choose"
    },

    act_choose: function(evt){
        console.log("choose");
        //var seniorId = $(evt.target).attr("data-senior-id");
        //console.log(seniorId);
        //var model = this.collection.get(seniorId);
        //console.log(model);

    },

    initialize: function(attrs, options, evt) {

        this.$el.addClass('requests');
        this.template = attrs.template || tmplDefault;
        console.log(this.collection);
        this.listenTo(this.collection, "change add remove", this.render);
    },


    render: function() {
        var createds = [];
        $(this.collection.models).each(function(){
            if(this.attributes.state == 'created')
                createds.push(this.attributes);
        });
        this.$el.html(this.template({
            requests:createds
        }));

        return this.$el;
    }

});

/**
 * Created by Marina on 12.06.2018.
 */
