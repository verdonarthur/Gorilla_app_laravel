var tmplDefault = require("../templates/seniorForm.handlebars");

export default Backbone.View.extend({

    events: {
        'submit form': 'save'
    },

    save: function (evt) {
        evt.preventDefault();

        let inputGender = this.$el.find("#genderS option:selected");
        let inputLastName = this.$el.find("#lastName");
        let inputFirstName = this.$el.find("#firstName");
        let inputBirth = this.$el.find("#birth");
        let inputStreet = this.$el.find("#street");
        let inputStreetNumber = this.$el.find("#streetNumber");
        let inputCity = this.$el.find("#city");
        let inputNPA = this.$el.find("#NPA");
        let inputPhone = this.$el.find("#phone");
        let inputEmail = this.$el.find("#email");

        let gender = inputGender.val();
        let lastName = inputLastName.val();
        let firstName = inputFirstName.val();
        let birth = inputBirth.val();
        let street = inputStreet.val();
        let streetNumber = inputStreetNumber.val();
        let city = inputCity.val();
        let NPA = inputNPA.val();
        let phone = inputPhone.val();
        let email = inputEmail.val();

        this.collection.create({

            gender: gender,
            lastName: lastName,
            firstName: firstName,
            birth: birth,
            street: street,
            streetNumber: streetNumber,
            city: city,
            NPA: NPA,
            phone: phone,
            email: email,
            password: "pomme"

        }, {
            success: function(response)
        {
            console.log(JSON.stringify(response));
            location.href = "#senior";

        },
        error: function(response)
        {
            console.log(JSON.stringify(response));
        },
            done: function(){
                location.href = "#senior";
            }
        });

    },
    initialize: function(attrs, options) {
        this.template =  tmplDefault;
    },
    render: function() {
        this.$el.html(this.template());
        return this.$el;
    }
});
