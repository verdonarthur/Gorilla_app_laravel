var tmplDefault = require("../templates/requestsWait.handlebars");

export default Backbone.View.extend({

    initialize: function(attrs, options) {

        this.$el.addClass('requestsWait');
        this.template = attrs.template || tmplDefault;
        this.listenTo(this.collection, "change add remove", this.render);
    },

    render: function() {
        this.$el.html(this.template({
            requestsWait: this.collection.toJSON(),
            sent: this.collection.isStatutSent()
        }));
        return this.$el;
    }
});
