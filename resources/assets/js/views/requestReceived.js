var tmplDefault = require("../templates/requestsReceived.handlebars");

export default Backbone.View.extend({

    events: {
        "click .btn_choose": "act_choose"
    },

    act_choose: function(evt){
        console.log("choose");
        var seniorId = $(evt.target).attr("data-senior-id");
        var model = this.collection.get(seniorId);
    },

    initialize: function(attrs, options) {

        this.$el.addClass('requestsReceived');
        this.template = attrs.template || tmplDefault;
        this.listenTo(this.collection, "change add remove", this.render);
    },

    render: function() {
        //console.log(this.collection.toJSON());
        this.$el.html(this.template({
            requestsReceived: this.collection.toJSON(),
            created: this.collection.isStatutCreated()
        }));
        return this.$el;
    }
});
