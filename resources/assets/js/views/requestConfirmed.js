var tmplDefault = require("../templates/requestsConfirmed.handlebars");

export default Backbone.View.extend({

    initialize: function(attrs, options) {

        this.$el.addClass('requestsConfirmed');
        this.template = attrs.template || tmplDefault;
        this.listenTo(this.collection, "change add remove", this.render);
    },

    render: function() {
        this.$el.html(this.template({
            requestsConfirmed: this.collection.toJSON(),
            accepted: this.collection.isStatutAccepted()
        }));
        return this.$el;
    }
});
