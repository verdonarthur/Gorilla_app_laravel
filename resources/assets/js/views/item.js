var tmplDefault = require("../templates/item.handlebars");

export default Backbone.View.extend({ //comme si c'était public

    initialize: function(attrs, options) {

        this.template = attrs.template || tmplDefault;
        this.listenTo(this.model, "change", this.render); //dès que y'a un changement sur le modèle je fais un render
    },

    render: function() {
        //console.log(this.model.toJSON()); //épure le modèle backbone pour garder les attributs
        //this.dom.text(this.model.get("title"));
        //this.$el.html(this.template({title: "test"}));
        this.$el.html(this.template(this.model.attributes));

        return this.$el;
    }

});
