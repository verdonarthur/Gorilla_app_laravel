/**
 * Created by Marina on 13.06.2018.
 */
var tmplDefault = require("../templates/listJuniors.handlebars");

export default Backbone.View.extend({

    initialize: function(attrs, options, evt) {

        this.$el.addClass('juniors');
        this.template = attrs.template || tmplDefault;
        this.listenTo(this.collection, "change add remove", this.render);
    },

    render: function() {
        //console.log("render");
        //console.log(this.collection.toJSON());
        this.$el.html(this.template({
            juniors: this.collection.toJSON()
        }));

        return this.$el;
    }
});

