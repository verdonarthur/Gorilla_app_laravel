var tmplDefault = require("../templates/menu.handlebars");

export default Backbone.View.extend({

    events: {
        'click a.nav-link': 'change'
    },

    change: function (event) {

        console.log("navigation");
        console.log(this);

        var page = $(this).attr("href");

        //console.log(page);

    },

    initialize: function(attrs, options) {

        this.template = attrs.template || tmplDefault;
        this.listenTo(this.model, "change", this.render);
    },

    render: function() {

        this.$el.html(this.template(this.model.attributes));

        return this.$el;
    }

});
