/**
 * Created by Marina on 13.06.2018.
 */

var tmplDefault = require("../templates/juniorsMatch.handlebars");

export default Backbone.View.extend({

    events:{
        "click .btn.link": "link"
    },

    link: function(evt){
        console.log("link");
        var id = $(evt.target).attr('data-juniorid');
        console.log(this.attrs.request.attributes);
        this.attrs.request.attributes.juniorId = id;
        this.attrs.request.attributes.state = 'sent';
        this.attrs.request.save().done(function() {
            location.href = "#tabBord";
        })
    },

    initialize: function(attrs) {

        this.attrs = attrs;
        console.log("attributs");
        console.log(this.attrs.request);

        this.template = attrs.template || tmplDefault;
    },

    render: function() {
        this.$el.html(this.template({
            juniors: this.collection.toJSON(),
            senior: this.attrs.senior,
            request: this.attrs.request
        }));

        return this.$el;
    }
});

