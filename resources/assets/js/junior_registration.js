var CONST = require("./const.js");
var JUNIOR_DATA = CONST.JUNIOR_DATA;
var REQUEST_DATA = CONST.REQUEST_DATA;
var AVAILABILITY_DATA = CONST.AVAILABILITY_DATA;
var SKILL_DATA = CONST.SKILL_DATA;


exports.loadJuniorRegistration = function (WEBSITE_URL, URL_API)
{
    console.log("registrationjunior");

    $("#birthJ").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });


    //$("#phoneJ").intlTelInput();

    $('#registrationFormJ').on('submit', function (event) {

        event.preventDefault();
        var form_data = $(this).serialize();
        console.log(form_data);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhrFields: {
                withCredentials: true
            }
        });
        $.ajax({
            method: "POST",
            url: $(this).attr('action'),
            data: {
                gender: $("#genderJ option:selected").val(),
                lastName: $('#lastNameJ').val(),
                firstName: $('#firstNameJ').val(),
                birth: $('#birthJ').val(),
                street: $('#streetJ').val(),
                streetNumber: $('#streetNumberJ').val(),
                NPA: $('#NPAJ').val(),
                city: $('#cityJ').val(),
                phone: $('#phoneJ').val(),
                email: $('#emailJ').val(),
                password: $('#passwordJ').val()
            },
            dataType: "json",
            success: function success(data) {
                console.log(data);
            },
            error: function error(data) {
                console.log(data);
            }

        }).done(function (data) {

            console.log($('#emailJ').val());
            console.log($('#passwordJ').val());
            var login = require('./login.js');
            var logResult = login.login($('#emailJ').val(), $('#passwordJ').val(), URL_API);
            logResult.done(data => {
                var ls = require("./jsonStorage.js");
                localUserStorage = new ls.storageJSON('USER');
                var user = data;
                localUserStorage.setItem('userID', user.id);
                var urlGetByID = URL_API + 'junior/getByUserId/' + user.id;
                var getJuniorId = $.getJSON(urlGetByID, function (data) {
                    var juniorID = data.id;
                    localUserStorage.setItem('juniorId', juniorID);
                    localUserStorage.setItem('groupName', user.groups[0].name);
                    localUserStorage.setItem('address', user.address);
                    window.location.href = WEBSITE_URL + "Jconfirmation";
                });
            });
        }).fail(function (data) {

            console.log(data);
            $.each(data.responseJSON.error, function (key, value) {

                console.log(value[0]);
                console.log(key);
                var input = '#registrationForm input[name=' + key + ']';
                console.log(input);
                $(input + '+span').text(value["0"]).css('font-size', '0.9rem');
            });
        });
    }
    );
}

exports.loadJuniorAvailability = function (WEBSITE_URL, URL_API)
{
    console.log("availabilityjunior");
    $(".fa-times").click(function () {
        $(this).toggleClass("fa-times");
        $(this).toggleClass("fa-check");
    });
    $(".fa-check").click(function () {
        $(this).toggleClass("fa-check");
        $(this).toggleClass("fa-times");
    });

    var infoJunior = JSON.parse(localStorage.getItem('USER'));
    console.log(infoJunior);
    var juniorID = infoJunior.juniorId;
    var addressId = infoJunior.address.id;
    console.log(juniorID);
    $("#sendAvailabilities").click(function ()
    {
        var checkedDispo = $(".fa-check");
        var availabilities = {};
        checkedDispo.each(function (index)
        {
            var splitValue = $(this).attr("title").split(" ");
            var day = splitValue[0];
            var hour = splitValue[1];
            var to;
            var from;

            if (hour === "am")
            {
                from = "08:00:00";
                to = "12:00:00";
            } else
            {
                from = "13:00:00";
                to = "18:00:00";
            }

            availabilities[index] = {
                from: from,
                to: to,
                day: day,
                juniorId: juniorID,
                addressId: addressId,
            }

        });


        var postAllAvail = $.each(availabilities, function (index) {
            console.log($(this)[0]);
            console.log($(this)[0].day);
            $.ajax({
                method: "POST",
                url: URL_API + 'availability',
                dataType: "json",
                data: {
                    day: $(this)[0].day,
                    from: $(this)[0].from,
                    to: $(this)[0].to,
                    juniorId: juniorID,
                    addressId: addressId,
                },
                success: function (data)
                {
                    console.log(juniorID + " updated");
                },
                error: function (data) {
                    console.log(data);
                }
            })

        });
        $.when(postAllAvail).done(function (data) {
            window.location.href = WEBSITE_URL + "Jcompetences";
        });

    });

}


exports.loadJuniorConfirmation = function (WEBSITE_URL, URL_API)
{
    console.log("confirmationJunior");
}

exports.loadJuniorCompetences = function (WEBSITE_URL, URL_API)
{
    var SKILL_TEMPLATE = $(".template_skill").clone().removeClass("template");
    $("#template_skill").empty();

    var allSkill = $.getJSON(SKILL_DATA);
    $.when(allSkill)
            .done(function (data) {

                var allSkillObj = allSkill.responseJSON;
                console.log(allSkillObj);

                var allSkillsName = $.each(allSkillObj, function (index)
                {
                    console.log($(this)[0].name);
                    var skill_dom = SKILL_TEMPLATE.clone();

                    skill_dom.find(".skill_name").text($(this)[0].name);
                    skill_dom.find(".skill_name").attr('id', $(this)[0].id);
                    $("#template_skill").append(skill_dom);


                });
                $(".pro-chx").click(function () {
                    console.log("lala");
                    $(this).toggleClass("checkedSkill");
                    $(this).toggleClass("btn-primary");
                });
            }).fail(function (data) {
        console.log(data);
    });

    console.log("competencesJunior");

    $("#sendSkills").click(function ()
    {
        var checkedSkills = $(".checkedSkill");
        var tabIDSkills = [];
        checkedSkills.each(function (index)
        {
            var idCheckedSkill = $(this).attr('id');

            tabIDSkills.push(idCheckedSkill);

        });
        console.log(tabIDSkills);
        var infoJunior = JSON.parse(localStorage.getItem('USER'));
        var juniorID = infoJunior.juniorId;

        $.ajax({
            method: "PUT",
            url: URL_API + 'junior/' + juniorID + '/addSkills',
            data: {
                skills: tabIDSkills,
            },
            success: function (data)
            {
                console.log("sucessssss");
            },
            error: function (data) {
                console.log(data);
            }
        }).done(function (data) 
        {
            console.log("lala");
            window.location.href = WEBSITE_URL + "Jsummary";
        });


    });

}

exports.loadJuniorSummary = function (WEBSITE_URL, URL_API)
{
    console.log("Récapitulatif Junior");
    var infoJunior = JSON.parse(localStorage.getItem('USER'));
        var juniorID = infoJunior.juniorId;
        var juniorData;
        var skills;
    $.ajax({
        type: "GET",
        url: URL_API + "junior/"+juniorID,
    }).done(function (data) {
        console.log(data);
        juniorData=data;
            var source   = $("#api-junior-summary").html();
            var template = Handlebars.compile(source);
            $("#recapHandlebars").html(template(juniorData));
    });
    
    var mySkills = $.getJSON(JUNIOR_DATA + "/getMySkills");
    $.when(mySkills)
        .done(function (allSkillResponse, mySkillsResponse) {
            console.log(mySkills.responseJSON);
        skills = mySkills.responseJSON;
        var source2   = $("#api-skills").html();
        
            var template2 = Handlebars.compile(source2);
            $("#skills-handlebar").html(template2(skills));
        });
        
        
        var myDispo = $.getJSON(AVAILABILITY_DATA, function (getAvailabilities) {
        var availabilities = getAvailabilities;
        var source3   = $("#api-dispos").html();
            var template3 = Handlebars.compile(source3);
            $("#dispos-handlebar").html(template3(availabilities));
    });

    }

