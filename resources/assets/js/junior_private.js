var CONST = require("./const.js");
var JUNIOR_DATA = CONST.JUNIOR_DATA;
var REQUEST_DATA = CONST.REQUEST_DATA;
var AVAILABILITY_DATA = CONST.AVAILABILITY_DATA;
var SKILL_DATA = CONST.SKILL_DATA;
var INTERVENTION_DATA = CONST.INTERVENTION_DATA;
exports.loadJuniorPrivate = function () {
    var REQUEST_TEMPLATE = $(".template_request").clone().removeClass("template");
    var REQUEST_ACCEPTED_TEMPLATE = $(".template_request_accepted").clone().removeClass("template");
    var REQUEST_DONE_TEMPLATE = $(".template_request_done").clone().removeClass("template");
    var SKILL_TEMPLATE = $(".template_skill").clone().removeClass("template");
    var SKILL_ALL_TEMPLATE = $(".template_all_skill").clone().removeClass("template");
    var availabilities = [];

    $("#requestList").empty();
    $("#requestList_accepted").empty();
    $("#requestList_done").empty();
    $("#template_skill").empty();
    $("#template_all_skill").empty();
    var juniorId = -1;

    var infoJunior = JSON.parse(localStorage.getItem('USER'));

    //Arthur's Logic
    var urlGetByID = JUNIOR_DATA + '/getByUserId/' + infoJunior.userID;
    $.getJSON(urlGetByID, function (data) {
        juniorId = data.id;
        // SUPER DIVISION JUNIOR_DATA / juniorId
        $.ajax({
            method: "GET",
            dataType: "json",
            url: JUNIOR_DATA + "/" + juniorId,
        }).done(data => {
            var junior = data;
            var source = $("#api-junior").html();
            var template = Handlebars.compile(source);
            $("#profilHandlebar").html(template(junior));
        }).fail(data => {
            console.log(data);
            $("#profilHandlebar").html(data.responseJSON);
        });

    });
    menuHandler();

    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })


    //modif profile ***************************
    $('#modificationFormJ').on('submit', function (event) {
        event.preventDefault();
        //console.log(juniorId);
        $.ajax({
            method: $(this).attr('method'),
            url: '/gorilla/api/v1/junior/' + juniorId,
            data: {
                lastName: $('#lastNameJ').val(),
                firstName: $('#firstNameJ').val(),
                street: $('#streetJ').val(),
                streetNumber: $('#streetNumberJ').val(),
                NPA: $('#NPAJ').val(),
                city: $('#cityJ').val(),
                phone: $('#phoneJ').val(),
                email: $('#emailJ').val(),
                birth: '1995-06-30',
                gender: 'F',
                password: 'pomme'
            },

            dataType: "json",
            success: function success(data) {
                //console.log(data);
            },
            error: function error(data) {
                //console.log(data);
            }

        }).done(function (data) {

            //console.log(data);
        }).fail(function (data) {

            //console.log(data);
            $.each(data.responseJSON.error, function (key, value) {

                //console.log(value[0]);
                //console.log(key);

                var input = '#modificationForm input[name=' + key + ']';
                //console.log(input);

                $(input + '+span').text(value["0"]).css('font-size', '0.9rem');
            });
        });
    });
    //requêtes ******************************************
    $.getJSON(REQUEST_DATA, function (data) {
        $.each(data, function (i, entry) {
            switch (entry['state']) {
                case "sent":
                    var request_dom = REQUEST_TEMPLATE.clone();

                    request_dom.find(".request_description").text(entry.description);
                    request_dom.find(".request_startDate").text(entry.startDate);
                    request_dom.find(".request_seniorName").text(entry.senior.user.firstName);
                    request_dom.find(".btn-accepte").attr('id', entry.id);
                    request_dom.find(".btn-rapport").attr('id', entry.id);


                    $("#requestList").append(request_dom);

                    request_dom.sort(function (a, b) {
                        var dateA = a.startDate;
                        var dateB = b.startDate;
                        var convertDateA = new Date($.trim(dateA));
                        var convertDateB = new Date($.trim(dateB));
                    });
                    break;
                case "accepted":
                    var request_accepted_dom = REQUEST_ACCEPTED_TEMPLATE.clone();

                    request_accepted_dom.find(".request_description").text(entry.description);
                    request_accepted_dom.find(".request_startDate").text(entry.startDate);
                    request_accepted_dom.find(".request_seniorName").text(entry.senior.user.firstName);
                    request_accepted_dom.find("#btnRemplirRapport").attr("requestId",entry.id);
                    request_accepted_dom.find("#btnRemplirRapport").attr("juniorId",entry.juniorId);
                    request_accepted_dom.find("#btnRemplirRapport").attr("seniorId",entry.seniorId);

                    $("#requestList_accepted").append(request_accepted_dom);

                    request_accepted_dom.sort(function (a, b) {
                        var dateA = a.startDate;
                        var dateB = b.startDate;
                        var convertDateA = new Date($.trim(dateA));
                        var convertDateB = new Date($.trim(dateB));
                    });
                    break;
                case "done":
                    var request_done_dom = REQUEST_DONE_TEMPLATE.clone();
                    request_done_dom.find(".request_description").text(entry.description);
                    request_done_dom.find(".request_startDate").text(entry.startDate);
                    request_done_dom.find(".request_seniorName").text(entry.senior.user.firstName);

                    $("#requestList_done").append(request_done_dom);

                    request_done_dom.sort(function (a, b) {
                        var dateA = a.startDate;
                        var dateB = b.startDate;
                        var convertDateA = new Date($.trim(dateA));
                        var convertDateB = new Date($.trim(dateB));
                    });
                    break;
            }
        });
    });

    //Requêtes Accepted**************************

    $("#tabBord").on("click", ".btn-accepte", function (e) {
        /* $.getJSON(REQUEST_DATA, function(data){
            //console.log(data);
            var theRequest = data+"/"+this.id;
            console.log(theRequest);
        }) */
        $.ajax({
            method: "GET",
            dataType: "json",
            url: REQUEST_DATA + "/" + this.id,
        }).done(data => {
            var req = data;
            req.state = 'accepted';
            $.ajax({
                method: "PUT",
                dataType: "json",
                data:req,
                url: REQUEST_DATA + "/" + this.id,
            }).done(data =>{
                //$(e.trigger).closest(".template_request")
                window.location.reload();
            }).fail(data=>console.log(data));

        }).fail(data => {
            console.log(data);
        });
    });

    //Rapport d'intervention**************************
    $("#tabBord").on("click", ".btn-rapport", function (e) {
        console.log("ok");
    });

    //Disponnibilités**************************
    $.getJSON(AVAILABILITY_DATA, function (getAvailabilities) {
        availabilities = getAvailabilities;
        $("#addressList").append($('<option/>', {
            text: infoJunior.address.street + "/" + infoJunior.address.city,
        }));
        $(availabilities).each(function (index, value) {
            var fromSub = value.from.substr(0, value.from.length - 3);
            var toSub = value.to.substr(0, value.to.length - 3);
            $('#' + value.day + '_from').attr('idBd', value.id);
            $('#' + value.day + '_from').val(fromSub);
            $('#' + value.day + '_from').attr("disabled", false);
            $('#' + value.day + '_to').attr("disabled", false);
            console.log($('#' + value.day + '_to').parent().parent().parent().find('.'+value.day+'.conge').attr('checked',false));
            // console.log($('#' + value.day + '_from').parent().parent().find('td'));
            // $('#' + value.day + '_from').parent().find('input').attr('checked','checked')
            $("#" + value.day + '_to').val(toSub);
        })
    })
    $("#formAvailability").change(function () {
        $("#submitAvailabilities").attr('disabled', false);
    })
    $('#tableAvailability').on('change','.conge',function(event)
    {
        $(this).parent().parent().find('[type=time]').attr('disabled',!$(this).parent().parent().find('input').attr('disabled'));
        //$(this).parent().parent().find('input').attr('disabled',!$(this).parent().parent().find('input').attr('disabled'));
    })
    $('#formAvailability').submit(function (event) {
        event.preventDefault();
        var method = "PUT";
        var allGood= true;

        $("#tableAvailability").children('tbody').children('tr').each(function(index,value) //Parcours les jours de la semaine
        {
            var from = $('#'+value.id+'_from').val();
            var to = $('#'+value.id+'_to').val();
            var url = AVAILABILITY_DATA;

            //console.log($('#'+value.id+'_from').attr('disabled')+"------------------"+$('#'+value.id+'_from').attr('idbd'));
            if($('#'+value.id+'_from').attr('disabled') && $('#'+value.id+'_from').attr('idbd'))// Désactivé avail. éxistante
            {
                method = 'DELETE';
                url =AVAILABILITY_DATA+'/'+ $('#'+value.id+'_from').attr('idbd');
                console.log(method);
            }else if(!$('#'+value.id+'_from').attr('idbd') && !$('#'+value.id+'_from').attr('idbd')) //creation
            {
                method = "POST";
                url = AVAILABILITY_DATA;
                console.log(method);
            } else
            {
                method = "PUT";
                url = AVAILABILITY_DATA+'/'+ $('#'+value.id+'_from').attr('idbd');
                console.log(method);
            }
        $.ajax({
            url: url,
            type: method,
            dataType: "json",
            data: {
                day: value.id,
                from: ""+$('#'+value.id+'_from').val()+":00",
                to: ""+$('#'+value.id+'_to').val()+":00",
                juniorId: juniorId,
                addressId: infoJunior.address.id,
            },
            success: function(data) {
                console.log(value.id + "updated");
    },
            error: function (data) {
                console.log(data);
                allGood = false;
            }
        });
        })
        if(allGood)
        {
            location.reload();
            changePage("#dipsonibilite");
        }

    })
    //Compétences ******************************************
    var allSkill = $.getJSON(SKILL_DATA);
    var mySkills = $.getJSON(JUNIOR_DATA + "/getMySkills");

    $.when(allSkill, mySkills)
        .done(function (allSkillResponse, mySkillsResponse) {
            var arrayAllSkill = allSkillResponse[0];
            var mySkillsBd = mySkillsResponse[0];
            var arrayMySkill = [];
            $(mySkillsBd).each(function (index, mySkill) {
                arrayMySkill[index] = mySkill.name;
            })
            $(arrayAllSkill).each(function (index, skill) {
                if ($.inArray(skill.name, arrayMySkill) != -1) {
                    var skill_dom = SKILL_TEMPLATE.clone();
                    skill_dom.find(".skill_name").text(skill.name);
                    $("#template_skill").append(skill_dom);
                } else {
                    var skill_all_dom = SKILL_ALL_TEMPLATE.clone();
                    skill_all_dom.find(".skill_all_name").text(skill.name);
                    $("#template_all_skill").append(skill_all_dom);
                    //console.log(skill_all_dom);
                }
            });
        }).fail(function (data) {
            console.log(data);
        });
}
/* *
 * Gestion du clique sur un élément créé dynamiquement
 */
$('#skill').on('click', '#skillName', function (event) {
    $(this).attr('id', 'skillAllName');
    $("#template_all_skill").append(this);

})
$('#skill').on('click', '#skillAllName', function (event) {
    $(this).attr('id', 'skillName');
    $("#template_skill").append(this);
    //nouveauRapport("2018-06-14 10:00:00","2018-06-14 11:00:00",1,2,1,2,"Vraiment cool");
})
//Rapport d'intervention*************************************
$("#requestList_accepted").on("click","#btnRemplirRapport",function(){
    $("#btn-rapport").attr("requestID",$(this).attr('requestid')).attr("juniorid",$(this).attr('juniorid')).attr("seniorid",$(this).attr('seniorid'));
})
$("#RapportJ").submit( function (event) {
    event.preventDefault();

    var date = $("#newDemande-dateFrom").val();
    var from = date + " " + $("#day_from").val()+":00";
    var to = date + " " + $("#day_to").val()+":00";
    var isComplete = 1;
    var seniorId = $("#btn-rapport").attr("seniorid");
    var juniorId = $("#btn-rapport").attr("juniorid");
    var requestId = $("#btn-rapport").attr("requestid");
    var report = $("#newDemande-description").val()+"";
    nouveauRapport(from,to,isComplete,seniorId,juniorId,requestId,report);
    $.ajax({
        method: "GET",
        dataType: "json",
        url: REQUEST_DATA + "/" + requestId,
    }).done(data => {
        var req = data;
        console.log(req);
    req.state = 'done';
    $.ajax({
        method: "PUT",
        dataType: "json",
        data:req,
        url: REQUEST_DATA + "/" + requestId,
    }).done(data =>{
        window.location.reload();
}).fail(data=>console.log(data));

}).fail(data => {
        console.log(data);
});

})
function nouveauRapport(from,to,isComplete,seniorId,juniorId,requestId,report)
{
    console.log(requestId);
    $.ajax({
        url: INTERVENTION_DATA,
        type: "POST",
        dataType: "json",
        data: {
            from: from,
            to: to,
            isComplete: isComplete,
            seniorId: seniorId,
            juniorId: juniorId,
            requestId: requestId,
            report: report,
        },
        success: function(data) {
            console.log(data);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

//Menu*****************************
function menuHandler() {

    $("nav.mainNavJunior a.nav-link").on("click", function (event) {
        
        var page = $(this).attr("href");

        changePage(page);

        return false;
    });

    $("nav.mainNavJunior a.nav-link:first").trigger("click");

    $(window).on("popstate", function (event) {
        var id = location.hash; //Return the anchor part of a URL

        if ($(id).length == 0) {
            changePage("#tabBord");
        } else {
            changePage(id);
        }
    });

    $(window).trigger("popstate");
}

function changePage(page) {

    $("div.app .page_junior").each(function () {

        $(this).hide();
        if ("#" + $(this).attr('id') == page) {
            $(this).show();
        }
    });

    $("nav.mainNavJunior a.nav-link").removeClass("active");
    $("a[href='" + page + "'").addClass("active");
}