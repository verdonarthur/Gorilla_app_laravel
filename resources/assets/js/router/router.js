import ModelMenu from "../models/menu";
import ModelRequestsReceived from "../models/collectionRequestsReceived";
import ModelRequestsConfirmed from "../models/collectionRequestsConfirmed";
import ModelRequestsWait from "../models/collectionRequestsWait";
import ModelRequests from "../models/collectionRequests";
import ModelSeniors from "../models/collectionSeniors";
import ModelJuniors from "../models/collectionJuniors";
import ModelSenior from "../models/senior";

import ViewRequestReceived from "../views/requestReceived";
import ViewRequestConfirmed from "../views/requestConfirmed";
import ViewRequestWait from "../views/requestWait";
import ViewRequests from "../views/listRequests";
import ViewSeniors from "../views/listSeniors";
import ViewAddSenior from "../views/seniorForm";
import ViewJuniors from "../views/listJuniors";
import ViewJuniorsMatch from "../views/juniorsMatch";
import ViewRappelSenior from "../views/rappelSenior";
import collectionSeniors from "../models/collectionSeniors";
import ViewSeniorAddForm from "../views/modAddForm";


/*var tmplMenu = require ("./templates/menu.handlebars");
var tmplRequestReceived = require ("./templates/requestsReceived.handlebars");
var tmplRequestConfirmed = require ("./templates/requestsConfirmed.handlebars");*/

//let menu = new ModelMenu();

let requestsReceived = new ModelRequestsReceived();
let requestsConfirmed = new ModelRequestsConfirmed();
let requestsWait = new ModelRequestsWait();
let requests = new ModelRequests();
let seniors = new ModelSeniors();
let juniors = new ModelJuniors();
let juniorsMatch = new ModelJuniors();

let viewRequestReceived = new ViewRequestReceived({
    collection: requestsReceived
});

let viewRequestConfirmed = new ViewRequestConfirmed({
    collection: requestsConfirmed
});

let viewRequestWait = new ViewRequestWait({
    collection: requestsWait
});

let viewRequests = new ViewRequests({
    collection: requests
});

let viewSeniors = new ViewSeniors({
    collection: seniors
});

let viewFormSenior = new ViewAddSenior({
    collection: seniors
});

let viewJuniors = new ViewJuniors({
    collection: juniors
});


function emptyAll() {
    $("#stat1").empty();
    $("#stat2").empty();
    $("#stat3").empty();
    $("#requestsSenior").empty();
    $("#senior").empty();
    $("#newSenior").empty();
    $("#junior").empty();
    $("#juniorsMatch").empty();
}

requestsReceived.fetch();
requestsConfirmed.fetch();
requests.fetch();
requestsWait.fetch();
seniors.fetch();
juniors.fetch();

export default Backbone.Router.extend({
    routes: {
        "": "tabBord",
        "tabBord": "tabBord",
        "senior": "senior",
        "newSenior": "newSenior",
        "junior": "junior",
        "juniorsMatch/:requestid": "juniorsMatch"
    },

    tabBord: function () {
        emptyAll();
        viewRequestReceived.render().appendTo("#stat1");
        viewRequestConfirmed.render().appendTo("#stat2");
        viewRequestWait.render().appendTo("#stat3");
        viewRequests.render().appendTo("#requestsSenior");
    },

    senior: function () {
        emptyAll();
        //var senior = seniors.find(function (senior) { return senior.attributes.id == request.attributes.seniorId; });

        let viewSeniorAddForm = new ViewSeniorAddForm();

        viewSeniors.render().appendTo("#senior");
        viewSeniorAddForm.render().appendTo("body");
    },

    newSenior: function () {
        emptyAll();

        viewFormSenior.render().appendTo("#newSenior");
    },

    junior: function () {
        emptyAll();

        viewJuniors.render().appendTo("#junior");
    },
    juniorsMatch: function (requestid) {
        emptyAll();
        var request = requests.find(function(request){return request.attributes.id == requestid});
        var senior = seniors.find(function (senior) { return senior.attributes.id == request.attributes.seniorId; });

        let viewJuniorsMatch = new ViewJuniorsMatch({
            collection: juniors,
            request:request,
            senior: senior
        });
        console.log(viewJuniorsMatch);
        viewJuniorsMatch.render().appendTo("#juniorsMatch");
    }

})


