import backbone from "backbone";

import adminRouteur from "./router/router.js";

$(function(){

    let admin = new adminRouteur();
    Backbone.history.start();
    admin.navigate("tabBord",true);

})



/*
import Backbone from "Backbone"

import ModelMenu from "./models/menu";
import ModelRequestsReceived from "./models/collectionRequestsReceived";
import ModelRequestsConfirmed from "./models/collectionRequestsConfirmed";
import ModelRequestsWait from "./models/collectionRequestsWait";
import ModelRequests from "./models/collectionRequests";


import ViewItem from "./views/item";
import ViewMenu from "./views/menu";
import ViewRequestReceived from "./views/requestReceived";
import ViewRequestConfirmed from "./views/requestConfirmed";
import ViewRequestWait from "./views/requestWait";
import ViewRequests from "./views/listRequests";

var tmplMenu = require ("./templates/menu.handlebars");
var tmplRequestReceived = require ("./templates/requestsReceived.handlebars");
var tmplRequestConfirmed = require ("./templates/requestsConfirmed.handlebars");

let menu = new ModelMenu();

let requestsReceived = new ModelRequestsReceived();
let requestsConfirmed = new ModelRequestsConfirmed();
let requestsWait = new ModelRequestsWait();
let requests = new ModelRequests();

let viewMenu = new ViewMenu({
    model: menu,
    template: tmplMenu
});

$(function(){

    console.log("test");
    viewMenu.render().appendTo("#adminMainNav");

    let viewRequestReceived = new ViewRequestReceived({
        collection: requestsReceived
    });

    let viewRequestConfirmed = new ViewRequestConfirmed({
        collection: requestsConfirmed
    });

    let viewRequestWait = new ViewRequestWait({
        collection: requestsWait
    });

    let viewRequests = new ViewRequests({
        collection: requests
    });


    viewRequestReceived.render().appendTo("#stat1");
    viewRequestConfirmed.render().appendTo("#stat2");
    viewRequestWait.render().appendTo("#stat3");
    viewRequests.render().appendTo("#requestsSenior");

    requestsReceived.fetch();
    requestsConfirmed.fetch();
    requests.fetch();
    requestsWait.fetch();

})

*/
