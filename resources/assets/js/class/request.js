CONST = require('../const.js');
var URL_API = CONST.URL_API;

function Request(description, startDate, endDate, skillsJuniorIds, type) {
    this.description = description;
    this.state = "created";
    this.startDate = startDate;
    this.endDate = endDate;
    this.skillsJuniorIds = skillsJuniorIds;
    this.type = type;

}
Request.prototype.getObject = function () {
    var request = {
        description: this.description,
        state: this.state,
        startDate: this.startDate,
        endDate: this.endDate,
        skillsJuniorIds: this.skillsJuniorIds,
        type: this.type,
    };

    if (typeof this.requestId !== 'undefined')
        request.requestId = this.requestId;

    return request;
}
Request.prototype.save = function () {
    return $.ajax({
        type: "POST",
        data: this.getObject(),
        url: URL_API + "request"
    });
}

module.exports = {Request: Request};