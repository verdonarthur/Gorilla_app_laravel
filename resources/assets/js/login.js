function login(username, password, urlAPI) {
    return $.ajax({
        type: "POST",
        url: urlAPI + "login",
        data: {
            email: username,
            password: password
        },
        xhrFields: {
            withCredentials: true
        }
    });
}
function logout(urlAPI){
    return $.ajax({
        type: "GET",
        url: urlAPI + "logout",
        xhrFields: {
            withCredentials: true
        }
    });
}

exports.login = function (username, password, urlAPI) {
    return login(username, password, urlAPI);
}

exports.logout = function(urlAPI){
    return logout(urlAPI);
}

exports.loadPageLogin = function (WEBSITE_URL, URL_API) {
    $('#login-form').submit(function (e) {
        e.preventDefault();
        
        localStorage.clear();
        var email = $("#email", this).val();
        var password = $("#password", this).val();
        console.log("username :" + email, "password :" + password);


        var logResult = login(email, password, URL_API);

        logResult.done(data => {
            var ls = require("./jsonStorage.js");

            localUserStorage = new ls.storageJSON('USER');
            $("#email", this).removeClass('is-invalid');
            $("#password", this).removeClass('is-invalid');

            var HOMEPAGES = {
                'Junior': { 'homepage': WEBSITE_URL + "Jprivate" },
                'Senior': { 'homepage': WEBSITE_URL + "SprofilPrivate" },
                'Sec. Mem.': { 'homepage': WEBSITE_URL + "Adashboard" },
            };

            var user = data;
            console.log(data);
            localUserStorage.setItem('userID', user.id);
            localUserStorage.setItem('groupName', user.groups[0].name);
            localUserStorage.setItem('address',user.address);
            var homepageToRedirect = HOMEPAGES[user.groups[0].name];
            window.location.href = homepageToRedirect.homepage;
        });
        logResult.fail(data => {
            $("#email", this).addClass('is-invalid');
            $("#password", this).addClass('is-invalid');
        });
    });
}



