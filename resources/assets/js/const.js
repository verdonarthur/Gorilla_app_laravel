var WEBSITE_URL = "http://pingouin.heig-vd.ch/gorilla/";

exports.WEBSITE_URL = WEBSITE_URL;
exports.URL_API = WEBSITE_URL + "api/v1/";
exports.JUNIOR_DATA = WEBSITE_URL+"api/v1/junior";
exports.REQUEST_DATA = WEBSITE_URL+"api/v1/request";
exports.SKILL_DATA = WEBSITE_URL+"api/v1/skill";
exports.SENIOR_BACKEND = WEBSITE_URL+"SprofilPrivate";
exports.JUNIOR_BACKEND = WEBSITE_URL+"Jprivate";
exports.JUNIOR_BACKEND = WEBSITE_URL+"SprofilPrivate";
exports.AVAILABILITY_DATA = WEBSITE_URL+"api/v1/availability";
exports.INTERVENTION_DATA = WEBSITE_URL+"api/v1/intervention";
