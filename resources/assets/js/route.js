/********************** APP CONST ***************************/
CONST = require('./const.js');
var WEBSITE_URL = CONST.WEBSITE_URL;
var URL_API = CONST.URL_API;
var JUNIOR_DATA = CONST.JUNIOR_DATA;
var REQUEST_DATA = CONST.REQUEST_DATA;
var SKILL_DATA = CONST.SKILL_DATA;
//var USERS = new JsonStorage("USERS");

/***************** HELPER FUNCTION *************************/
function isWantedPage(pageName) {
    var url = window.location.pathname;
    return url.indexOf(pageName) !== -1;
}


/******************* WHEN LOAD PAGE FINISH *****************/
$(function () {


    // test if the page has 'Connexion in his name'

    if (isWantedPage('Connexion')) {
        // load the function exported in login.js
        var login = require('./login.js');

        // execute the loadLogin function in the file login.js
        login.loadPageLogin(WEBSITE_URL, URL_API);
    }
    if (isWantedPage('Jprivate')) {

        var juniorPrivate = require('./junior_private.js');
        // execute the loadLogin function in the file login.js
        juniorPrivate.loadJuniorPrivate();
    }
    if (isWantedPage('SprofilPrivate')) {
        var privateSenior = require('./senior_private.js');

        privateSenior.loadSeniorPrivate(URL_API);
    }


    if (isWantedPage('Sformulaire')) {
        var privateSenior = require('./register.js');

        privateSenior.loadRegistration(WEBSITE_URL, URL_API);
    }
    if (isWantedPage('Jform')) {
        var juniorform = require('./junior_registration.js');

        juniorform.loadJuniorRegistration(WEBSITE_URL, URL_API);
    }
    if (isWantedPage('Jconfirmation')) {
        var juniorConf = require('./junior_registration.js');

        juniorConf.loadJuniorConfirmation(WEBSITE_URL, URL_API);
    }
    if (isWantedPage('Javaibility')) {
        var juniorConf = require('./junior_registration.js');

        juniorConf.loadJuniorAvailability(WEBSITE_URL, URL_API);
    }
    if (isWantedPage('Jcompetences')) {
        var juniorComp = require('./junior_registration.js');

        juniorComp.loadJuniorCompetences(WEBSITE_URL, URL_API);
    }
    if (isWantedPage('Jsummary')) {
        var juniorSummary = require('./junior_registration.js');

        juniorSummary.loadJuniorSummary(WEBSITE_URL, URL_API);
    }

});
