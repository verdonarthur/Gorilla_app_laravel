CONST = require('./const.js');
var URL_API = CONST.URL_API;
var DATE_FORMAT = "Y-MM-DD HH:mm:ss";

var requests = require('./class/request.js');
var Request = requests.Request;

var ls = require("./jsonStorage.js");
var localUserStorage = new ls.storageJSON('USER');

window.Handlebars.registerHelper('select', function (value, options) {
    var $el = $('<select />').html(options.fn(this));
    $el.find('[value="' + value + '"]').attr({ 'selected': 'selected' });
    return $el.html();
});

window.Handlebars.registerPartial('simpleInput',
    '<div class="form-group ">' +
    '<label for="{{fieldName}}" class="">{{fieldNameFR}}</label>' +
    '<div class="">' +
    '<input type="text" name="{{fieldName}}" class="form-control" id="{{fieldName}}" value="{{ fieldValue}}" placeholder="{{placeholder}}" {{#if required}} required{{/if}}>' +
    '</div>' + '</div>');

window.Handlebars.registerHelper('getRequestStateFR', function (stateRequest) {
    switch (stateRequest) {
        case "created":
            return "En cours d'attribution";
        case "sent":
            return "En attente d'acceptation";
        case "accepted":
            return "Acceptée";
        case "not accepted":
            return "Refusée";
        case "done":
            return "Terminée";
    }
});

function loadSenior(userID) {
    return $.ajax({
        type: "GET",
        url: URL_API + "senior/findByUserID/" + userID
    });
}

function loadSeniorRequest(seniorID) {
    return $.ajax({
        type: "GET",
        url: URL_API + "request" //+ userID
    });
}

function loadSkills() {
    return $.ajax({
        type: "GET",
        url: URL_API + "skill"
    });
}

function updateSenior(data, idSenior) {
    return $.ajax({
        type: "PUT",
        data: data,
        url: URL_API + "senior/" + idSenior
    });
}

function saveNewRequest() {
    var startDate = moment($('#newDemande-dateFrom').val() + " " + $('#newDemande-timeFrom').val() + ":00");
    var endDate = moment($('#newDemande-dateTo').val() + " " + $('#newDemande-timeTo').val() + ":00");

    if ($('#newDemande-description').val().length > 1000) {
        $("#newDemande-step4-errors").append($('<div class="alert alert-danger" role="alert">Déscription trop longue</div>'));
        return;
    }

    skills = [];
    $('input[name=newDemande-skills]').each(function () {
        if ($(this).is(':checked'))
            skills.push($(this).val())
    });

    //simple request
    if (startDate.dayOfYear() == endDate.dayOfYear()) {
        var request = new Request($('#newDemande-description').val(),
            startDate.format(DATE_FORMAT),
            endDate.format(DATE_FORMAT),
            skills,
            'simple'
        );
    }
    // multiple request
    else if (startDate.week() < endDate.week() && startDate.day() <= endDate.day()) {
        var nbrMinute = ((endDate.hour() * 60 + endDate.minute()) - (startDate.hour() * 60 + startDate.minute()));

        var request = new Request($('#newDemande-description').val(),
            startDate.format(DATE_FORMAT),
            startDate.add(nbrMinute, 'm').format(DATE_FORMAT),
            $('#newDemande-skills').val(),
            'multiple'
        );
    } else {
        $("#newDemande-step4-errors").append($('<div class="alert alert-danger" role="alert">Erreurs dans les dates</div>'));
        return;
    }

    request.save().done(data => {
        if (request.type == 'multiple') {
            var nbrWeek = endDate.week() - startDate.week();
            for (i = 0; i < nbrWeek; i++) {
                var childRequest = request;
                childRequest.startDate = moment(request.startDate).add(1, 'w').format(DATE_FORMAT);
                childRequest.endDate = moment(request.endDate).add(1, 'w').format(DATE_FORMAT);
                childRequest.requestId = data.id;
                childRequest.type = 'simple';
                childRequest.save().done(data => {
                    if (data.endDate = endDate.format(DATE_FORMAT)) {
                        window.location.href = CONST.SENIOR_BACKEND;
                    }
                }).fail(data => console.log("FAIL : " + data.responseJSON));
            }
        } else {
            window.location.href = CONST.SENIOR_BACKEND;
        }
    }).fail(data => {
        $("#newDemande-step4").text(JSON.stringify(data.responseJSON));
    });
}

function updateProfil() {
    var userID = localUserStorage.getItem('userID');
    var user = {};
    $("#profil-registrationForm input,select").each(function (i) {
        user[$(this).attr('name')] = $(this).val();
    });

    loadSenResult = loadSenior(userID);
    loadSenResult.done(data => {
        var updtSenRes = updateSenior(user, data.id);
        updtSenRes.done(data => {
            var source = $("#tmp-request-selectSkill").html();
            var template = Handlebars.compile(source);
            $("#newDemande-skills").html(template(data));
        }).fail(data => {
            $("#profil-registrationForm input,select").each(function (i) {
                if (data.responseJSON.error[$(this).attr('name')] !== undefined) {
                    $(this).addClass('is-invalid');
                }
            });
        });
    });
}

function makeSummaryRequest() {
    var startDate = moment($('#newDemande-dateFrom').val() + " " + $('#newDemande-timeFrom').val());
    var endDate = moment($('#newDemande-dateTo').val() + " " + $('#newDemande-timeTo').val());

    skills = [];
    $('.newDemande-skills').each(function () {
        if ($("input", this).is(':checked')) {
            var badge = $("<span class='badge badge-primary m-2 p-2'></span>");
            badge.text($(this).text().trim());
            console.log("badge");
            $("#step4-summary-skills").append(badge);
        }
    });

    var description = $('#newDemande-description').val();
    $('#step4-summary-description').text("Déscription : " + (description.length > 700 ? description.substr(0, 700) + "..." : description));
    $('#step4-summary-date').text("Du " + startDate.format(DATE_FORMAT) + " au " + endDate.format(DATE_FORMAT));
}

function nextStepNewRequest() {
    var step = Number($('#newDemande').attr('data-step'));
    $("#newDemande-previous").prop('disabled', false);
    if (step <= 4) {
        $('#newDemande').attr('data-step', step + 1);

        $("#newDemande .progress-bar").attr("aria-valuenow", step + 1).width(25 * (step + 1) + "%").html(step + 1);

        $("#newDemande-step" + (step + 1)).toggleClass('d-none');
        $("#newDemande-step" + step).toggleClass('d-none');
        if (step + 1 == 4) {
            $("#newDemande-next").text("Confirmer");
            $("#newDemande-next").addClass("btn-success");
            $("#newDemande-next").removeClass("btn-primary");
            $("#newDemande-next").off("click");
            $("#newDemande-next").on("click", saveNewRequest);

            $("#newDemande-step4-summary li").empty()
            makeSummaryRequest();
        }
    }
}

function previousStepNewRequest() {
    var step = Number($('#newDemande').attr('data-step'));
    if (step > 1) {
        if (step == 4) {
            $("#newDemande-next").text("Suivant");
            $("#newDemande-next").removeClass("btn-success");
            $("#newDemande-next").addClass("btn-primary");
            $("#newDemande-next").off("click");
            $("#newDemande-next").on("click", nextStepNewRequest);
        }
        Number($('#newDemande').attr('data-step', step - 1));

        $("#newDemande .progress-bar").attr("aria-valuenow", step - 1).width(25 * (step - 1) + "%").html(step - 1);

        $("#newDemande-step" + (step - 1)).toggleClass('d-none');
        $("#newDemande-step" + step).toggleClass('d-none');
    }
    if (step == 2) {
        $("#newDemande-previous").prop('disabled', true);
    }
}

exports.loadSeniorPrivate = function () {
    $.fn.datepicker.setDefaults({
        format: 'yyyy-mm-dd',
        autoHide:true
    })


    /******************DATA LOAD********************/
    var userID = localUserStorage.getItem('userID');
    var loadSenResult = loadSenior(userID);
    loadSenResult.done(data => {
        console.log(data);

        var source = $("#tmp-profil-senior").html();
        var template = Handlebars.compile(source);
        $("#profil-info").html(template(data));
    });
    loadSenResult.fail(data => $("#profil-info").text(JSON.stringify(data.responseJSON)));

    loadSenResult = loadSeniorRequest();
    loadSenResult.done(data => {
        console.log(data);
        var source = $("#tmp-request-requestCards").html();
        var template = Handlebars.compile(source);
        $(data).each(i => {
            switch (data[i].state) {
                case "created":
                case "sent":
                    $("#demande-created-cards").append(template(data[i]));
                    break;
                case "accepted":
                    $("#demande-accepted-cards").append(template(data[i]));
                    break;
                case "not accepted":
                case "done":
                    $("#demande-done-cards").append(template(data[i]));
                    break;
            }

        });
    });

    loadSenResult = loadSkills();
    loadSenResult.done(data => {
        console.log(data);
        var source = $("#tmp-request-selectSkill").html();
        var template = Handlebars.compile(source);
        $("#newDemande-skills").html(template(data));
    });
    loadSenResult.fail(data => {
        console.log(data);
    })

    /******************** BIND EVENTS ************************/
    $("#newDemande-next").on('click', nextStepNewRequest);
    $("#newDemande-previous").on('click', previousStepNewRequest);
    $("#profil-btnSaveProfil").on('click', updateProfil);
    $("#newDemande-dateFrom").on('change', function () {
        $("#newDemande-dateTo").val($(this).val());
    });
    $("input[name=newDemande-isSimpleOrMultiple]").on('change', function () {
        $("#newDemande-dateTo").fadeToggle();
        $("#newDemande-dateFrom").off('change');

        if ($(this).val() == 'simple') {
            $("#newDemande-dateTo").val($("#newDemande-dateFrom").val());
            $("#newDemande-dateFrom").on('change', function () {
                $("#newDemande-dateTo").val($(this).val());
            });
        }
    })


    /******************** Load GUI COMPONENT ****************/
    $("#newDemande-previous").prop('disabled', true);
    $("#newDemande-dateTo").toggle();

    $('#newDemande-timeFrom,#newDemande-timeTo').timepicker({
        timeFormat: 'H:mm',
        interval: 30,
        minTime: '8',
        maxTime: '8:00pm',
        defaultTime: '8',
        startTime: '8:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('#newDemande-dateFrom,#newDemande-dateTo').datepicker();
    $('input[value=simple]').prop("checked", true);

}

