import Master from "./master";
import request from "./request";
var CONST = require("../const.js");


export default Backbone.Collection.extend({

    initialize:function(attrs, option){
    },

    model: request,
    url:CONST.URL_API+"request",

    isStatutAccepted: function(){
        var compteur = 0;
        this.each(function (requestsConfirmed) {
            if (requestsConfirmed.get("state") == 'accepted') {
                compteur++;
            };
        });
        return compteur;
    }
});
