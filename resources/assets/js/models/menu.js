/**
 * Created by Marina on 02.05.2018.
 */
import master from "./master";

export default master.extend({

    defaults: {

        entries: [
            {
                label: "Tableau de bord",
                url: "#tabBord"
            },
            {
                label: "Senior",
                url: "#senior"
            },
            {
                label: "Nouveau senior",
                url: "#newSenior"
            },
            {
                label: "Junior",
                url: "#junior"
            }
        ]
    },

    addEntry: function(entry){
        this.get("entries").push(entry);
        this.trigger("change");
    }
});
/**
 * Created by Marina on 12.06.2018.
 */
