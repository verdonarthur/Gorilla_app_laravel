import Master from "./master";

export default Backbone.Model.extend({
    //urlRoot: 'http://localhost/gorilla/api/v1/senior',
    defaults: {
      id: null,
      userId:null
    }
});