/**
 * Created by Marina on 23.05.2018.
 */
import Master from "./master";
import senior from "./senior";
var CONST = require("../const.js");


export default Backbone.Collection.extend({

    initialize:function(attrs, option){

    },

    model: senior,
    url:CONST.URL_API+"senior",

});
