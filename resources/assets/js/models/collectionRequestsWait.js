/**
 * Created by Marina on 23.05.2018.
 */
import Master from "./master";
import request from "./request";
var CONST = require("../const.js");


export default Backbone.Collection.extend({

    initialize:function(attrs, option){
        //console.log(this.toJSON());
    },

    model: request,
    url:CONST.URL_API+"request",

    isStatutSent: function () {
        var compteur = 0;
        this.each(function (requestsReceived) {
            if (requestsReceived.get("state") == 'sent') {
                compteur++;
            };
        });
        return compteur;
    }

});
