import Master from "./master";
import request from "./request";
var CONST = require("../const.js");


export default Backbone.Collection.extend({

    initialize:function(attrs, option){
        //console.log(this.toJSON());
    },

    model: request,
    url:CONST.URL_API+"request",

    isStatutCreated: function(requests){
            return requests.get('state') == "created";
    }

});
