exports.loadRegistration = function (WEBSITE_URL, URL_API){

    console.log("registration");

    $("#birth").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });

    $('#registrationForm').on('submit', function (event) {

        event.preventDefault();

        var form_data = $(this).serialize();

        console.log(form_data);
console.log("helo");
        console.log(event);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhrFields: {
                withCredentials: true
            }
        });

        $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: {
                    gender: $("#gender option:selected").val(),
                    lastName: $('#lastName').val(),
                    firstName: $('#firstName').val(),
                    birth: $('#birth').val(),
                    street: $('#street').val(),
                    streetNumber: $('#streetNumber').val(),
                    NPA: $('#NPA').val(),
                    city: $('#city').val(),
                    phone: $('#phone').val(),
                    email: $('#email').val(),
                    password: $('#password').val()
                },
                dataType: "json",
                success: function success(data) {
                    console.log("success");
                    console.log(data);
                }
            })
            .done(function (data) {
                console.log("Call to next part of form.");
                console.log(data);
                window.location.href = WEBSITE_URL+"Sconfirmation";
            })

            .fail(function (data) {
                console.log("fail");
                console.log(data); 
                $.each(data.responseJSON.error, function (key, value) {

                    console.log(value[0]);
                    console.log(key);

                    var input = '#registrationForm input[name=' + key + ']';
                    console.log(input);

                    $(input + '+span').text(value["0"]).css('font-size','0.9rem');
                });
            });
    });
}
