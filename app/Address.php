<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends BaseModel
{
    use SoftDeletes;
    protected $fillable = ['street', 'streetNumber', 'NPA', 'city', 'userId'];
    protected $errors;

    /**
     * Definition des regles de validation pour une nouvelle adresse
     *
     */
    public static $rules
        = [
            'id'           => 'exists:addresses|sometimes|required',
            'street'       => 'required|string|max:200',
            'streetNumber' => 'required|string',
            'NPA'          => 'required|integer|digits:4',
            'city'         => 'required|string',
            'userId'       => 'exists:users,id|sometimes|required',
        ];

    /**
     * Relation avec NPA
     *
     */
    public function NPA()
    {
        return $this->belongsTo('App\NPA', 'NPAId');
    }

    /**
     * Relation avec AvailabilityJunior
     *
     */
    public function availabilityJuniors()
    {
        return $this->hasMany('App\AvailabilityJunior', 'addressId');
    }

    /**
     * Relation avec User
     *
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'userId');
    }

    /**
     * Méthode permettant de valider les contraintes d'intégrité
     *
     */
    public function validateConstraint($data)
    {
        $adresses = Address::where([
            ['street', $data['street']],
            ['streetNumber', $data['streetNumber']],
            ['NPA', $data['NPA']],
            ['city', $data['city']],
            ['userId', $data['userId']],
        ])->get();

        if (isset($this->id)) {
            if (isset($this->userId) && $data['userId'] != $this->userId) {
                if ($adresses->count() > 0) {


                    $this->errors = "This address already exists for this user";
                    return false;
                }
            }
        }


        return true;
    }
}