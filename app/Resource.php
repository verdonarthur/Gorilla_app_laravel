<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    /**
     * Definition des regles de validation pour une nouvelle ressource
     *
     */
    public static $rules
        = [
            'id'   => 'exists:resources|sometimes|required',
            'name' => 'required|string',
        ];

    public function groups()
    {
        return $this
                ->belongsToMany('App\Resource', 'roles', 'groupId', 'resourceId')
                ->withTimestamps()
                // on spécifie que le pivot se fait sur la colonne "role" de la table de jointure.
                ->withPivot('role');
    }
}