<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class that creates Junior objects. This is the model for Junior.
 * The Junior object corresponds to a person in the group Junior.
 * This class extends BaseModel that allows to check the validity of the inputs to create or update
 * such an object.
 * BaseModel extends model.
 */
class Junior extends BaseModel
{
    protected $fillable = ['isEmployed', 'inscriptionStep', 'userId'];
    protected $errors;
    use SoftDeletes;

    /**
     * Definition of the rules for validation of a possible new Junior.
     */
    public static $rules
        = [
            'id'              => 'exists:juniors|sometimes|required',
            'isEmployed'      => 'boolean',
            'inscriptionStep' => 'in:"1","2","3","4","5"',
            'userId'          => 'exists:users,id|sometimes|required',
        ];

    public function availabilityJuniors()
    {
        return $this->hasMany('App\AvailabilityJunior', 'juniorId');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'userId');
    }

    public function skillsJuniors()
    {
        return $this->belongsToMany('App\SkillsJunior', 'juniors_skillsJuniors',
            'juniorId', 'skillsJuniorId');
    }

    public function requests()
    {
        return $this->hasMany('App\Request', 'juniorId');
    }

    public function interventions()
    {
        return $this->hasMany('App\Intervention', 'juniorId');
    }

    /**
     * Method that validates the validity constraints.
     * param: $data : the datas from the client.
     * Validity constraint(s):
     *      1 : if User is a Junior, Then User is neither a Senior nor a SecretariatMember
     */
    public function validateConstraint($data)
    {

        $secretariat = SecretariatMember::where('userId', $data['userId'])
            ->count();
        $senior = Senior::where('userId', $data['userId'])->count();
        $junior = 0;
        if (!isset($this->id)) {
            $junior = Junior::where('userId', $data['userId'])->count();
        }

        if ($secretariat > 0 || $senior > 0 || $junior > 0) {
            $this->errors = "The user is already used as another user.";
            return false;
        }
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }
}