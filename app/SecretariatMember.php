<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class that creates SecretariatMember objects. This is the model for SecretariatMember.
 * The SecretariatMember object corresponds to a person in the group Secretariat member.
 * This class extends BaseModel that allows to check the validity of the inputs to create or update
 * such an object.
 * BaseModel extends model.
 */
class SecretariatMember extends BaseModel
{
    use SoftDeletes;
    protected $table = 'secretariatMembers';
    protected $fillable = ['userId'];

    /**
     * Definition of the rules for validation of a possible new secretariatMember.
     */
    public static $rules
        = [
            'id'     => 'exists:secretariatMembers|sometimes|required',
            'userId' => 'exists:users,id|sometimes|required',
        ];

   
    public function user()
    {
        return $this->belongsTo('App\User', 'userId');
    }

    /**
     * Method that validates the validity constraints.
     * param: $data : the datas from the client.
     * Validity constraint(s):
     *      1 : If  the User is a secretariatMember, THEN it is neither a junior, nor a senior
     */
    public function validateConstraint($data)
    {
        $secretariat = 0;
        if (!isset($this->id)) {
            $secretariat = SecretariatMember::where('userId', $data['userId'])
                ->count();
        }
        $senior = Senior::where('userId', $data['userId'])->count();
        $junior = Junior::where('userId', $data['userId'])->count();

        if ($secretariat > 0 || $senior > 0 || $junior > 0) {
            $this->errors = "The user is already used as another user.";
            return false;
        }
        return true;
    }

}