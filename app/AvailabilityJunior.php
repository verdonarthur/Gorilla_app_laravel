<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class AvailabilityJunior extends BaseModel
{
    use SoftDeletes;
    protected $table = 'availabilityJuniors';
    protected $fillable
        = [
            'from',
            'to',
            'day',
            'juniorId',
            'addressId',
        ];

    /**
     * data's validations rules
     *
     * @var array
     */
    public static $rules
        = [
            'id'        => 'exists:availabilityJuniors|sometimes|required',
            'from'      => 'required|date_format:"H:i:s"|before:to',
            'to'        => 'required|date_format:"H:i:s|after:from',
            'addressId' => 'exists:addresses,id|sometimes|required',
            'day'       => 'required|in:"monday","tuesday","wednesday","thursday","friday","saturday","sunday"',
            'juniorId'  => 'exists:juniors,id|sometimes|required',
        ];

    /**
     * relationship with Address
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo('App\Address', 'addressId');
    }

    /**
     * relationship with Junior
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function junior()
    {
        return $this->belongsTo('App\Junior', 'juniorId');
    }

    /**
     * @param $data
     *      Validating integrity constraint and non-existency of the object(based on business id)
     *      integrity constraint:
     *      1 : there are maximum two availabilityJunior by day for one junior
     *      2 : from and to from a specific day must not overlap another from and to
     *
     * @return bool
     */
    protected function validateConstraint($data)
    {
        if (!isset($this->id)) {
            if (self::where('day', $data['day'])
                    ->where('juniorId', $data['juniorId'])
                    ->where('addressId', $data['addressId'])->count() >= 2
            ) {
                $this->errors
                    = "there are maximum two availabilityJunior by day for one junior";
                return false;
            }
            $this->id = -1;
        } else {
            if (self::where('day', $data['day'])
                    ->where('juniorId', $data['juniorId'])
                    ->where('addressId', $data['addressId'])
                    ->where('id', '<>', $this->id)->count() >= 2
            ) { //le count est différent en cas d'update
                $this->errors
                    = "there are maximum two availabilityJunior by day for one junior";
                return false;
            }
        }

        // from and to from a specific day must not overlap another from and to
        $startTime = $data['from'];
        $endTime = $data['to'];

        $eventsCount = self::where(function ($query) use (
            $startTime,
            $endTime
        ) {
            $query->where(function ($query) use ($startTime, $endTime) {
                $query->where('from', '>=', $startTime)
                    ->where('to', '<', $startTime);
            })
                ->orWhere(function ($query) use ($startTime, $endTime) {
                    $query->where('from', '<', $endTime)
                        ->where('to', '>=', $endTime);
                })
                ->orwhere(function ($query) use ($startTime, $endTime) {
                    $query->where(function ($query) use ($startTime, $endTime) {
                        $query->where('from', '<=', $startTime)
                            ->where('to', '>=', $startTime);
                    })->where(function ($query) use ($startTime, $endTime) {
                        $query->where('from', '<=', $endTime)
                            ->where('to', '<=', $endTime);
                    });
                });
        })
            ->where('day', $data['day'])
            ->where('juniorId', $data['juniorId'])
            ->where('addressId', $data['addressId'])
            ->where('id','<>',$this->id)
            ->count();


        //$queries = \DB::getQueryLog();
        //dd($queries);
        if ($eventsCount > 0) {
            $this->errors
                = "from and to from a specific day must not overlap another from and to";
            return false;
        }
        return true;
    }
}
