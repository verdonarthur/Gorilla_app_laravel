<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * Definition des regles de validation pour un nouveau groupe
     *
     */
    public static $rules
        = [
            'id'   => 'exists:groups|sometimes|required',
            'name' => 'required|string',
        ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'group_user', 'groupId',
            'userId')->withTimestamps();
    }

    public function resources()
    {
        return $this->belongsToMany('App\Resource', 'roles', 'groupId',
                'resourceId')->withTimestamps()->withPivot('role');
    }

    public function can($role, $resourceName)
    {
        $resources = $this->resources()
            ->where("name", $resourceName)
            ->wherePivot("role", $role);

        foreach ($resources as $resource) {
            if ($resource->pivot->role == $role && $resourceName == $resource->name) {
                return true;
            }
        }
        return false;
    }
}