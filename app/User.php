<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Validator;

class User extends Authenticatable
{

    use Notifiable;
    use SoftDeletes;
    protected $fillable = [
        'firstName',
        'lastName',
        'email',
        'password',
        'phone',
        'gender',
        'birth',
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];
    protected $errors;

    public static $rules
    = [
        'id' => 'exists:users|sometimes|required',
        'email' => 'required|unique:users|string',
        'lastName' => 'required|string',
        'firstName' => 'required|string',
        // regex: definit le format pour un numéro de téléphone suisse
        'phone' => array(
            'required',
            // V1 :'regex:/(\\+41)\\s(\\d{2})\\s(\\d{3})\\s(\\d{2})\\s(\\d{2})/',
            'regex:/(\+41)\s[0-9]{2}\s[0-9]{3}\s[0-9]{2}\s[0-9]{2}$/',
        ),
        'password' => 'required|string',
        'birth' => 'required|date|before: 18 years ago',
        'gender' => 'required|in:"F", "M"',
        'addressId' => 'exists:addresses,id|sometimes|required',
    ];

    public function address()
    {
        return $this->hasOne('App\Address', 'userId');
    }

    public function junior()
    {
        return $this->hasOne('App\Junior', 'userId');
    }

    public function senior()
    {
        return $this->hasOne('App\Senior', 'userId');
    }

    public function secretariatMember()
    {
        return $this->hasOne('App\SecretariatMember', 'userId');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group', 'group_user', 'userId',
            'groupId')->withTimestamps();
    }

    public function validate($data)
    {
        if (isset($this->id)) {

            self::$rules['email'] = array(
                'required',
                'email',
                'unique:users,email,' . $this->id,
                'string',
            );
        }

        $v = Validator::make($data, self::$rules);

        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }

        if (!$this->validateConstraint($data)) {
            return false;
        }

        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    protected function validateConstraint($data)
    {
        return true;
    }

    public function isInGroup($groupName)
    {
        if ($this->groups()->where('name', $groupName)->count() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function hasRole($roleLabel, $ressource)
    {

        $groups = $this->groups;
        foreach ($groups as $group) {
            if ($group->can($roleLabel, $ressource)) {
                return true;
            }
        }

        return false;
    }
}
