<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SkillsJunior extends BaseModel
{
    use SoftDeletes;

    protected $table = 'skillsJuniors';
    
    protected $fillable = ['name'];
    
    protected $dates = ['deleted_at','updated_at','created_at'];

    /**
     * data's validations rules
     *
     * @var array
     */
    public static $rules
        = [
            'id'   => 'exists:skillsJuniors,id|sometimes|required',
            'name' => 'required|string',
        ];

    /**
     * relationship with Request
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function requests()
    {
        return $this->hasMany('App\Request');
    }

    /**
     * relationship with Junior
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function juniors()
    {
        return $this->belongsToMany('App\Junior', 'Junior_SkillsJunior',
            'juniorId', 'skillsJuniorId');
    }

    /**
     * @param $data
     * Validating integrity constraint and non-existency of the object(based on business id)
     * business id : name
     *
     * @return bool
     */
    protected function validateConstraint($data)
    {
        $skillJunior = SkillsJunior::where(['name' => $data['name']])->count();
        if ($skillJunior > 0) {
            $this->errors = "Id métier déjà éxistant";
            return false;
        }
        return true;
    }
}
