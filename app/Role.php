<?php

namespace App;

class Role
{
    const CREATE = 'C';
    const READ = 'R';
    const UPDATE = 'U';
    const DELETE = 'D';      
    
    const ALL = 'CRUD';
    
}
