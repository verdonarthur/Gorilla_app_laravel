<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class defining an intervention
 */
class Intervention extends BaseModel
{
    use SoftDeletes;
    protected $errors;
    protected $fillable = [
        'from',
        'to',
        'isComplete',
        'isValidatedBySenior',
        'seniorId',
        'juniorId',
        'requestId',
        'interventionPriceId',
        'ratingJunior',
        'ratingSenior',
        'commentsRatingJunior',
        'commentsRatingSenior',
        'report',
        'interventionTotalPrice'];
    public static $rules = [
        'id' => 'exists:interventions,id|sometimes|required',
        'ratingJunior' => 'numeric',
        'commentsRatingJunior' => 'String',
        'ratingSenior' => 'numeric',
        'commentsRatingSenior' => 'String',
        'from' => 'required|date|date_format:Y-m-d H:i:s',
        'to' => 'required|date|date_format:Y-m-d H:i:s',
        'report' => 'String',
        'isComplete' => 'required|boolean',
        'interventionTotalPrice' => 'required|numeric',
        'isValidatedBySenior' => 'required|boolean',
        'requestId' => 'exists:requests,id|required',
        'seniorId' => 'exists:seniors,id|required',
        'juniorId' => 'exists:juniors,id|required',
        'interventionPriceId' => 'exists:interventionPrices,id|required',
    ];

    public function request()
    {
        return $this->belongsTo('App\Request', 'requestId');
    }

    public function senior()
    {
        return $this->belongsTo('App\Senior', 'seniorId');
    }

    public function interventionPrice()
    {
        return $this->belongsTo('App\InterventionPrice', 'interventionPriceId');
    }

    public function junior()
    {
        return $this->belongsTo('App\Junior', 'juniorId');
    }

    /**
     * Method used to validate the constraint of the class diagram
     *
     * constraint linked to intervention :
     *      1 : IF an Intervention is linked to a request Then the intervention's Senior and intervention's Junior are the same that the request ....
     *      2 : If 'startDate' is different of 'from' OR 'endDate' is different of 'to' THEN 'report' is obligatory
     * @param array $data
     */
    protected function validateConstraint($data)
    {
        if (!isset($this->id)) {
            $intervention = Intervention::where('requestId', $data['requestId'])->get();
            if ($intervention->count() > 0) {
                $this->errors = "Id metier deja existant";
                return false;
            }
        }

        /*
         * Constraint : 1
         */
        $request = Request::find($data['requestId']);
        if ($data['requestId'] && ($data['seniorId'] != $request->seniorId
            || $data['juniorId'] != $request->juniorId)) {
            $this->errors = "IF an Intervention is linked to a request Then the intervention's Senior and intervention's Junior are the same that the request ";
            return false;
        }

        /*
         * Constraint : 2
         */
        if (($data['from'] != $request->startDate || $data['to'] != $request->endDate)
            && !isset($data['report'])) {
            $this->errors = "If 'startDate' is different of 'from' OR 'endDate' is different of 'to' THEN 'report' is obligatory";
            return false;
        }

        return true;
    }
}
