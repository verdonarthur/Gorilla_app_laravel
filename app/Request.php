<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Request extends BaseModel
{
    use SoftDeletes;

    protected $fillable
        = [
            'description',
            'state',
            'startDate',
            'endDate',
            'type',
            'seniorId',
            'requestId',
            'juniorId'
        ];

    protected $dates = ['deleted_at', 'updated_at', 'created_at'];

    /**
     * data's validations rules
     *
     * @var array
     */
    public static $rules
        = [
            'id'          => 'exists:requests,id|sometimes|required',
            'description' => 'required|string',
            'state'       => 'required|in:"created","sent","accepted","not accepted","done"',
            'startDate'   => 'required|date|date_format:Y-m-d H:i:s',
            'endDate'     => 'required|date|date_format:Y-m-d H:i:s',
            'type'        => 'required|in:"simple","multiple"',
            'seniorId'    => 'exists:seniors,id|required',
            'requestId'   => 'exists:requests,id|sometimes',
            'juniorId'    => 'exists:juniors,id|sometimes',
        ];

    /**
     * relationship with Intervention
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function intervention()
    {
        return $this->hasOne('App\Intervention');
    }

    /**
     * relationship with SkillsJunior
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function skillsJuniors()
    {
        return $this->belongsToMany('App\SkillsJunior',
            'requests_skillsJuniors',
            'requestId', 'skillsJuniorId');
    }

    /**
     * relationship with Junior
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function junior()
    {
        return $this->belongsTo('App\Junior', 'juniorId');
    }

    /**
     * relationship with Senior
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function senior()
    {
        return $this->belongsTo('App\Senior', 'seniorId');
    }

    /**
     * relationship with Request
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function request()
    {
        return $this->belongsTo('App\Request', 'requestId');
    }

    /**
     * Validating integrity constraint and non-existency of the object(based on business id)
     * Business id : startDate, endDate, has been send by(seniorId)
     * integrity constraint :
     *      1 : 'StartDate' must be before 'EndDate'
     *      2 : If the Request has a parent request THEN the 'StartDate' must be after the parent request's 'StartDay'
     *      3 : 'StartDate' and 'EndDate' must not be more than 8 hours
     *      4 : If 'state' is not 'created' THEN a Junior is obligatory linked to the request.
     *      5 : If 'state' is 'done' THEN a intervention is complete about this request
     *
     * @return a boolean
     */
    protected function validateConstraint($data)
    {
        //id métier
        if (!isset($this->id)) {
            $this->id = -1;
        }
        $request = Request::where([
            'startDate' => $data['startDate'],
            'endDate'   => $data['endDate'],
            'seniorId'  => $data['seniorId'],
        ])->where('id', '!=', $this->id)->count();
	
        if ($request > 0) {
            $this->errors = "Id metier deja existant";
            return false;
        }
        // 1
        if ($data['startDate'] >= $data['endDate']) {
            $this->errors = "'StartDate' must be before 'EndDate'";
            return false;
        }
        // 2
        if (isset($data['requestId'])) {
            $parentRequest = self::find($data['requestId']);
            if ($parentRequest->startDay > $data['startDate']) {
                $this->errors
                    = "If the Request has a parent request THEN the 'StartDate' must be after the parent request's 'StartDay'";
                return false;
            }
        }
        // 3
        $startDate = strtotime($data['startDate']);
        $endDate = strtotime($data['endDate']);
        $interval = $endDate - $startDate;

        if ($interval > 8 * 60 * 60) {
            $this->errors
                = "'StartDate' and 'EndDate' must not be more than 8 hours";
            return false;
        }

        // 4
        if ($data['state'] != 'created' && !isset($data['juniorId'])) {
            $this->errors
                = "If 'state' is not 'created' THEN a Junior is obligatory linked to the request";
            return false;
        }
        //5
        if($data['state'] = 'done')
        {
            $interventionPerformed = Intervention::where('requestId',$this->id)->where('isComplete',true);
            if(empty($interventionPerformed))
            {
                $this->errors = "If 'state' is 'done' THEN a intervention is complete about this request";
                return false;
            }
        }

        return true;
    }
}
