<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class that creates InterventionPrice objects. This is the model for InterventionPrice.
 * The InterventionPrice object corresponds to a price of one or many interventions, price by hour,
 * with a description
 * This class extends BaseModel that allows to check the validity of the inputs to create or update
 * such an object.
 * BaseModel extends model.
 */
class InterventionPrice extends BaseModel
{

    use SoftDeletes;
    protected $table        = 'interventionPrices';
    protected $fillable     = ['priceByHour', 'description', 'datePriceSet'];
    protected $dates        = ['deleted_at', 'updated_at', 'created_at'];

     /**
     * Definition of the rules for validation of a possible new interventionPrice.
     */
    protected static $rules = [
        'id' => 'exists:interventionsPrices|sometimes|required',
        'priceByHour' => 'required|numeric|gt:0',
        'description' => 'required|String',
        'datePriceSet' => 'required|date',
    ];

    public function interventions()
    {
        return $this->hasMany('App\Interventiions');
    }

    /**
     * Method that validates the validity constraints
     * param: $data : the datas from the client.
     * Validity constraint(s):
     * none juste check the unicity-> there cannot be many intervention with the same price
     * and the same date at which this price was set
     */
    protected function validateConstraint($data)
    {
        $interventionPrice = InterventionPrice::where([
                'priceByHour' => $data['priceByHour'],
                'datePriceSet' => $data['datePriceSet']
            ])->count();

        if ($interventionPrice > 0) {
            $this->errors = "Id métier déjà éxistant";
            return false;
        }

        return true;
    }
}