<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends BaseModel
{
    use SoftDeletes;
    protected $errors;
    protected $fillable = [
        'amount',
        'isAutomated',
        'date',
        'seniorId'];
    protected $dates = [
        'deleted_at',
        'updated_at',
        'created_at'];
    public static $rules = [
        'id' => 'exists:payments,id|sometimes|required',
        'amount' => 'required|numeric|gt:0',
        'date' => 'required|date',
        'isAutomated' => 'required|boolean',
        'seniorId' => 'exists:seniors,id|sometimes|required',
    ];

    public function senior()
    {
        return $this->belongsTo('App\Senior', 'seniorId');
    }

    /**
     * Method used to validate the constraint of the class diagram
     *
     * @param array $data
     **/
    public function validateConstraint($data)
    {
        $payments = Payment::where([
            ['amount', $data['amount']],
            ['date', $data['date']],
            ['seniorId', $data['seniorId']],
        ])->get();

        if ($payments->count() > 0) {
            $this->errors = "This payment already exists for this senior";
            return false;
        }

        return true;
    }
}
