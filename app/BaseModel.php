<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use Validator;

abstract class BaseModel extends Model
{
    protected static $rules = array();

    protected $errors;

    public function validate($data)
    {
        $v = Validator::make($data, static::$rules);
        
        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }

        if (!$this->validateConstraint($data)) {
            return false;
        }

        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    abstract protected function validateConstraint($data);
}
