<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class defining a senior
 */
class Senior extends BaseModel
{

    use SoftDeletes;
    protected $errors;
    protected $fillable = [
        'remainingBalance',
        'subscriptionPlan',
        'inscriptionStep',
        'userId'];
    public static $rules = [
        'id' => 'exists:seniors|sometimes|required',
        'remainingBalance' => 'required|numeric',
        'subscriptionPlan' => 'required|in:"gold","silver","bronze","none"',
        'inscriptionStep' => 'required|in:"1","2","3","4","5"',
        'userId' => 'exists:users,id|sometimes|required',
    ];

    public function payments()
    {
        return $this->hasOne('App\Payment', 'seniorId');
    }

    public function interventions()
    {
        return $this->hasMany('App\Intervention', 'seniorId');
    }

    public function requests()
    {
        return $this->hasMany('App\Request', 'seniorId');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'userId');
    }

    /**
     * Method used to validate the constraint of the class diagram
     * constraint linked to senior :
     *      1 : If  the User is a Senior, THEN it is neither a junior, nor a SecretariatMember
     *      2 : the remainingBalance must be the Total of the Payment minus the Total of the Intervention validated cost
     * 
     * @param array $data
     */
    public function validateConstraint($data)
    {
        $secretariat = SecretariatMember::where('userId', $data['userId'])->count();
        $senior = 0;
        if (!isset($this->id)) {
            $senior = Senior::where('userId', $data['userId'])->count();
        }

        $junior = Junior::where('userId', $data['userId'])->count();

        if ($secretariat > 0 || $senior > 0 || $junior > 0) {
            $this->errors = "The user is already used as another user.";
            return false;
        }

        if ($this->id) {
            $totalAmountPayments = Payment::where('seniorId', $this->id)->sum('amount');
            $totalIntenventionCosts = Intervention::where('seniorId', $this->id)->sum('interventionTotalPrice');
            $remainingBalance = $totalAmountPayments - $totalIntenventionCosts;
            
            if ($data['remainingBalance'] != $remainingBalance) {
                $this->errors = "The remaining balance is wrong.";
                return false;
            }
        }

        return true;
    }
}
