<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RightsChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //$arr = explode(".", \Request::route()->getName(), 2);
        $getAction = \Request::route()->getAction();
        if (!isset($getAction['controller'])) {
            return $next($request);
        }

        $controllerAction = class_basename($getAction['controller']);
        list($controller, $action) = explode('@', $controllerAction);
        $resName = strtolower(str_replace('Controller', '', $controller));

        $groups = \App\Group::where('name', 'All')->get();

        if (Auth::check()) {
            $user = Auth::user();
            $groups = $groups->merge($user->groups);
        }

        if (!$this->rightResourceChecker($groups, $resName, $request->method())) {
            return response()->json('Access denied', 403);
        }

        return $next($request);
    }

    private function rightResourceChecker($userGroups, $resourceName, $method)
    {
        if (\App\Resource::where('name', $resourceName)->count() < 1) {
            return true;
        }

        $MethodToCRUD = array('POST' => 'C', 'PUT' => 'U', 'GET' => 'R', 'DELETE' => 'D');
        $crudMethod = $MethodToCRUD[$method];
        $right = '';

        foreach ($userGroups as $group) {
            $role = $group->resources()->where('name', $resourceName)->first();
            if (isset($role->pivot->role)) {
                $right = $right . $role->pivot->role;
            }

        }

        $right = array_unique(str_split($right));

        return in_array($crudMethod, $right) !== false;

    }
}
