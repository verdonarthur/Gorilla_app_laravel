<?php
/**
 * Created by PhpStorm.
 * User: Sebastien
 * Date: 30.05.2018
 * Time: 12:34
 */

namespace App\Http\Controllers;

use App\SecretariatMember;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class controller of the secretariatMember.
 * This class contains the methods that correspond to the CRUD functions.
 * extends controller.
 */
class SecretariatMemberController extends Controller
{

    /**
     * Display a listing of the secretariatMembers
     * @return all the secretariatMember objects, with their linked user.
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->isInGroup('Sec. Mem.')) {
            return SecretariatMember::where('userId', $user->id)->with('user')->first();
        }
        $secretariatMembers = SecretariatMember::with('user')
            ->get();
        return $secretariatMembers;
    }

    /**
     * Store a newly created secretariatMember in storage.
     *
     * @param  \Illuminate\Http\Request $request:
     * contains all the datas from the client
     * Those datas must contain:
     * email, lastName, firstName, phone, password, birth, gender
     *
     * @return \Illuminate\Http\Response:
     * the newly created secretariatMember, with its linked user
     */
    public function store(Request $request)
    {

        // Récupération des inputs pertinents
        if (!$request->has([
                'email',
                'lastName',
                'firstName',
                'phone',
                'password',
                'birth',
                'gender',
            ])
        ) {
            return response()->json(['error' => 'empty request'], 400);
        }
        $inputsUser = $request->only('email', 'lastName', 'firstName', 'phone',
            'password', 'birth', 'gender');

        $newUser = new User();
        if (!$newUser->validate($inputsUser)) {
            return response()->json(['error' => $newUser->errors()], 400);
        }
        DB::beginTransaction();
        try {
            $newUser = User::create($inputsUser);

            $inputsSecretariat['userId'] = $newUser->id;
            $newSecretariat              = new SecretariatMember();
            if (!$newSecretariat->validate($inputsSecretariat)) {
                return response()->json(['error' => $newSecretariat->errors()],
                        400);
            }
            $newSecretariat = SecretariatMember::create($inputsSecretariat);

            $groupId = \App\Group::where('name', 'Sec. Mem.')->first();
            $newUser->groups()->attach($groupId->id);

            DB::commit();
        } catch (\Exception $e) {
            // Si une erreur survient, on "annule" la craetion
            DB::rollback();
            // On envoie un message d'erreur et on redirige ver le formulaire, avec les inputs
            return response()->json(['error' => $e], 403);
        }
        return $newSecretariat->with('user')->where('id', $newSecretariat->id)
                ->get();
    }

    /**
     * Display the specified secretariatMember.
     *
     * @param  int $id: the id of the sercretariatMember that is looked for.
     *
     * @return \Illuminate\Http\Response:
     * the secretariatMember object with its linked user
     *
     * A secretariatMember can only show its own infos.
     */
    public function show($id)
    {
        $user = Auth::user();
        if ($user->isInGroup('Sec. Mem.')) {
            if ($user->id != $id) {
                return response()->json(['access denied'], 403);
            }
        }
        $toShow = SecretariatMember::find($id);
        if (empty($toShow)) {
            return response()->json(['error' => 'not found']);
        }
        return $toShow->with('user')->where('id', $toShow->id)->get();
    }

    /**
     * Remove the specified secretariatMember from storage, and its linked user.
     *
     * @param  int $id: the id of the secretariatMember to delete
     *
     * @return a http code 200 if well deleted. Else a 400 bad request. 
     */
    public function destroy($id)
    {
        $secretariatToDelete = SecretariatMember::find($id);
        if (empty($secretariatToDelete)) {
            return response()->json(['error' => 'user not fund']);
        }
        $userToDelete = User::find($secretariatToDelete->userId);
        if (empty($userToDelete)) {
            return response()->json(['error' => 'element missing to perform destroy']);
        }
        DB::beginTransaction();
        try {
            $secretariatToDelete->delete();
            $userToDelete->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 400);
        }
        return response()->json([''], 200);
    }

    /**
     * update a secretariatMember in storage and its linked user.
     *
     * @param  \Illuminate\Http\Request $request, $id
     * contains all the datas from the client and the id of the secretariatMember to update
     * Those datas must contain:
     * email, lastName, firstName, phone, password, birth, gender
     *
     * @return \Illuminate\Http\Response:
     * the newly updated secretariatMember, with its linked user
     */
    public function update(Request $request, $id)
    {
        if (!$request->has([
                'email',
                'lastName',
                'firstName',
                'phone',
                'password',
                'birth',
                'gender',
            ])
        ) {
            return response()->json(['error' => 'empty request'], 400);
        }

        $inputsUser = $request->only('email', 'lastName', 'firstName', 'phone',
            'password', 'birth', 'gender');


        $secretariat = \App\SecretariatMember::find($id);
        if (empty($secretariat)) {
            return response()->json(['error' => 'secretariat not found'], 404);
        }
        $inputSecretariat['userId'] = $secretariat->userId;
        if (!$secretariat->validate($inputSecretariat)) {
            return response()->json(['error' => $secretariat->errors()], 400);
        }

        $user = \App\User::find($secretariat->userId);
        if (empty($user)) {
            return response()->json(['error' => 'user not found'], 404);
        }
        if (!$user->validate($inputsUser)) {
            return response()->json(['error' => $user->errors()], 400);
        }


        DB::beginTransaction();
        try {
            $user->update($inputsUser);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e], 403);
        }
        return $secretariat->with('user')->get();
    }
}