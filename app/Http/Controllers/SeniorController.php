<?php
/**
 * API Controller for senior linked services.
 *
 * This controller manage a basic CRUD system.
 */

namespace App\Http\Controllers;

use App\Address;
use App\Senior;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SeniorController extends Controller
{
    /**
     * return all the senior in DB in JSON format with the user information
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->isInGroup('Senior') || $user->isInGroup('Junior')) {
            return response()->json(['forbidden'], 403);
        }

        return \App\Senior::with('user', 'user.address')->get();

    }

    /**
     * Store a new senior in the DB.
     *
     * The needed field are 'email', 'lastName', 'firstName',
     * 'phone', 'password', 'birth', 'gender', 'street', 'streetNumber', 'NPA', 'city'
     *
     * The created senior is return in a JSON format.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Intervention
     */
    public function store(Request $request)
    {
        if (!$request->has('email') || !$request->has('lastName') || !$request->has('firstName')
            || !$request->has('phone') || !$request->has('password') || !$request->has('birth')
            || !$request->has('gender') || !$request->has('street') || !$request->has('streetNumber')
            || !$request->has('NPA') || !$request->has('city')) {
            return response()->json(['error' => 'empty request'], 400);
        }

        $inputsUser = $request->only('email', 'lastName', 'firstName',
            'phone', 'password', 'birth', 'gender');
        $inputsAddress = $request->only('street', 'streetNumber', 'NPA', 'city');

        $senior = new Senior();
        $senior->remainingBalance = 0;
        $senior->subscriptionPlan = "none";
        $senior->inscriptionStep = 1;

        $user = new \App\User();
        if (!$user->validate($inputsUser)) {
            return response()->json(['error' => $user->errors()], 400);
        }
        $inputsUser['password'] = bcrypt($request->get('password'));

        DB::beginTransaction();
        try {

            $user = User::create($inputsUser);
            $inputsAddress['userId'] = $user->id;
            $address = new Address();

            if (!$address->validate($inputsAddress)) {
                return response()->json(['error' => $address->errors()], 400);
            }
            $address->fill($inputsAddress);
            $user->address()->save($address);
            $groupSeniorId = \App\Group::where('name', 'Senior')->first();
            $user->groups()->attach($groupSeniorId->id);
            $senior->userId = $user->id;
            $senior->save();

            DB::commit();

            return \App\Senior::with('user.address')->where('seniors.id',
                $senior->id)->first();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => "error"], 400);
        }
    }

    /**
     * give a senior in JSON format with the user information by the ID asked
     *
     * @param  int $id
     * @return \App\Senior
     */
    public function show($id)
    {
        $senior = \App\Senior::find($id);
        if (!isset($senior)) {
            return response()->json('Not found', 404);
        }

        $user = Auth::user();

        if ($user->isInGroup('Senior') && $user->senior()->first()->id != $id) {
            return response()->json('forbidden', 403);
        }

        return \App\Senior::with('user', 'user.address')->where('seniors.id', $id)->first();
    }

    /**
     * Update a senior in the DB.
     *
     * The needed field are 'email', 'lastName', 'firstName',
     * 'phone', 'birth', 'gender', 'street', 'streetNumber', 'NPA', 'city',
     *
     *
     * The updated senior is return in a JSON format.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$request->has('email') || !$request->has('lastName') || !$request->has('firstName')
            || !$request->has('phone') || !$request->has('birth')
            || !$request->has('gender') || !$request->has('street') || !$request->has('streetNumber')
            || !$request->has('NPA') || !$request->has('city')) {
            return response()->json(['error' => 'empty request'], 400);
        }

        $user = Auth::user();

        if ($user->isInGroup('Senior') && $user->senior()->first()->id != $id) {
            return response()->json('forbidden', 403);
        }

        $inputsUser = $request->only('email', 'lastName', 'firstName',
            'phone', 'birth', 'gender');
        $inputsUser['password'] = $request->query('password', $user->password);
        
        $inputsSenior['remainingBalance'] = $request->query('remainingBalance', $user->senior()->first()->remainingBalance);
        $inputsSenior['subscriptionPlan'] = $request->query('subscriptionPlan', $user->senior()->first()->subscriptionPlan);
        $inputsSenior['inscriptionStep'] = $request->query('inscriptionStep', $user->senior()->first()->inscriptionStep);
        
        $inputsAddress = $request->only('street', 'streetNumber', 'NPA', 'city');


        $seniorToUpdate = Senior::find($id);

        $inputsAddress['userId'] = $seniorToUpdate->userId;

        $userToUpdate = User::find($seniorToUpdate->userId);

        $address = Address::where('userId', $seniorToUpdate->userId)->first();

        $addressToUpdate = Address::find($address->id);

        if (empty($seniorToUpdate) || empty($userToUpdate) || empty($addressToUpdate)) {
            return response()->json(['error' => 'not found'], 404);
        }

        $inputsSenior['userId'] = $seniorToUpdate->userId;
        
        if (!$seniorToUpdate->validate($inputsSenior)) {
            return response()->json(['error' => $seniorToUpdate->errors()], 400);
        }

        if (!$userToUpdate->validate($inputsUser)) {
            return response()->json(['error' => $userToUpdate->errors()], 400);
        }

        if (!$addressToUpdate->validate($inputsAddress)) {
            return response()->json(['error' => $addressToUpdate->errors()], 400);
        }

        DB::beginTransaction();
        try {
            $userToUpdate->update($inputsUser);
            $addressToUpdate->update($inputsAddress);
            $seniorToUpdate->update($inputsSenior);

            DB::commit();

            return self::show($id);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => "error"], 400);
        }
    }

    /**
     * Delete a senior by his ID
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seniorToDelete = Senior::find($id);
        if (empty($seniorToDelete)) {
            return response()->json(['error' => 'not found'], 404);
        }

        $userToDelete = User::find($seniorToDelete->userId);
        $addressToDelete = \App\Address::where('userId',
            $seniorToDelete->userId)->first();

        if (empty($userToDelete) || empty($addressToDelete)) {
            return response()->json(['error' => 'element missing to perform destroy'], 404);
        }

        DB::beginTransaction();
        try {
            $userToDelete->delete();
            $seniorToDelete->delete();
            $addressToDelete->delete();

            DB::commit();
            return response()->json([''], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e], 403);
        }
    }

    /**
     * give a senior in JSON format with the user information by the User ID asked
     *
     * @param  int $id
     * @return \App\Senior
     */
    public function findByUserID($id)
    {
        if (!\App\User::find($id)) {
            return response()->json("Not found", 404);
        }

        $senior = \App\Senior::where('userId', $id)->first();
        if (!$senior) {
            return response()->json("Not found", 404);
        }

        return $this->show($senior->id);
    }
}
