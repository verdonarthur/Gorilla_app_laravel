<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Login a user
     * @param \App\Request need the email and the password
     * @return void the information of the user logged or 'bad login'
     */
    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        if (!Auth::attempt([
            'email' => $email,
            'password' => $password,
        ])) {
            return response()->json(['error'=>'bad login'], 400);
        }

        $user = \App\User::with(['groups','address'])->where('email', $email)->first();
        return response()->json($user, 200);
    }


    /**
     * logout the user currently logged
     */
    public function logout()
    {
        Auth::logout();
        return view('homepage');
    }
}
