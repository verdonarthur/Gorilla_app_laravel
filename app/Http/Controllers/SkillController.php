<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;

class SkillController extends Controller
{
    /**
     * listing the skills
     *
     * @return void the skills
     */
    public function index()
    {
        return \App\SkillsJunior::all();
    }

    /**
     * retriving a skill
     *
     * @param int $id
     * @return void the skill asked for
     */
    public function show($id)
    {
        $skill = \App\SkillsJunior::find($id);
        return empty($skill) ? response()->json(['error' => 'not found'], 404)
        : $skill;
    }

    /**
     * inserting a new skill
     *
     * @param \App\Request needed : name
     * @return void the skill just created
     */
    public function store(Request $request)
    {
        if (!$request->has('name')) {
            return response()->json(['error' => 'empty request'], 400);
        }

        $input = $request->only('name');

        $skill = new \App\SkillsJunior();
        if (!$skill->validate($input)) {
            return response()->json(['error' => $skill->errors()], 400);
        }

        $skill = \App\SkillsJunior::create($input);

        return $skill->where('id', $skill->id)->first();
    }

    /**
     * update a skill in the DB
     * @param \App\Request $request -> needed: name
     * @param int $id the id of the skill
     * @return the skill just updated
     */
    public function update(Request $request, $id)
    {
        if (!$request->has('name')) {
            return response()->json(['error' => 'empty request'], 400);
        }

        $input = $request->only('name');

        $skill = \App\SkillsJunior::find($id);
        if (empty($skill)) {
            return response()->json(['error' => 'not found'], 404);
        }

        if (!$skill->validate($input)) {
            return response()->json(['error' => $skill->errors()], 400);
        }

        \App\SkillsJunior::find($id)->update($input);

        return \App\SkillsJunior::find($id);
    }

    /**
     * id of the skill
     * soft deleting a skill
     * @param int $id
     * @return void http 200
     */
    public function destroy($id)
    {
        $skill = \App\SkillsJunior::find($id);
        if (empty($skill)) {
            return response()->json(['error' => 'not found'], 404);
        }

        $skill->delete();
        return response()->json([''], 200);
    }
}
