<?php
namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * API Controller for payment linked services.
 *
 * This controller manage a basic CRUD system.
 */
class PaymentController extends Controller
{

    /**
     * return all the payment in DB in JSON format with the linked senior
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->isInGroup('Senior')) {
            return Payment::with('senior.user.address')
                ->where('id', $user->senior()->first()->id)->get();
        } else {
            return Payment::with('senior.user.address')->get();
        }
    }

    /**
     * Store a new payment in the DB.
     *
     * The needed field are amount, isAutomated, seniorId
     *
     * The created payment is return in a JSON format with linked senior
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Payment
     */
    public function store(Request $request)
    {
        if (!$request->has('amount')
            || !$request->has('isAutomated')
            || !$request->has('seniorId')) {
            return response()->json(['error' => 'empty request'], 400);
        }
        $senior = \App\Senior::find($request->seniorId);
        if (empty($senior)) {
            return response()->json(['error' => 'not found'], 404);
        }

        $inputsPayment = $request->only('amount', 'isAutomated',
            'seniorId');
        $inputsPayment['date'] = date('Y-m-d H:i:s');
        $payment = new \App\Payment();

        if (!$payment->validate($inputsPayment)) {
            return response()->json(['error' => $payment->errors()], 400);
        }

        $payment = \App\Payment::create($inputsPayment);

        return $payment->with('senior.user.address')
            ->where('payments.id', $payment->id)->first();
    }

    /**
     * give a payment in JSON format with the senior
     * information by the ID asked
     *
     * @param  int $id
     * @return \App\Payment
     */
    public function show($id)
    {
        $payment = \App\Payment::find($id);
        if (!isset($payment)) {
            return response()->json('Not found', 404);
        }

        $user = Auth::user();
        if ($user->isInGroup('Senior')) {
            $payment = Payment::with('senior.user.address')
                ->where('id', $user->senior()->first()->id)
                ->where('payments.id',
                    $id)->get();

            if ($payment->count() < 1) {
                return response()->json('Not found', 404);
            }

            return $payment;
        } else {
            return Payment::with('senior.user.address')->where('payments.id',
                $id)->get();
        }
    }

    /**
     * Update a payment in the DB.
     *
     * The needed field are amount, isAutomated, seniorId
     *
     * The created intervention is return in a JSON format.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $id
     * @return \App\Payment
     */
    public function update(Request $request, $id)
    {
        if (!$request->has('amount')
            || !$request->has('isAutomated')
            || !$request->has('seniorId')) {
            return response()->json(['error' => 'empty request'], 400);
        }

        $senior = \App\Senior::find($request->seniorId);
        if (empty($senior)) {
            return response()->json(['error' => 'not found'], 404);
        }

        $inputsPayment = $request->only('amount', 'isAutomated',
            'seniorId');
        $inputsPayment['date'] = date('Y-m-d H:i:s');
        $paymentToUpdate = Payment::find($id);

        if (!$paymentToUpdate->validate($inputsPayment)) {
            return response()->json(['error' => $paymentToUpdate->errors()],
                400);
        }

        $paymentToUpdate->update($inputsPayment);

        return self::show($id);
    }

    /**
     * Delete a payment by his ID
     *
     * @param  int $id
     * @return \App\Intervention
     */
    public function destroy($id)
    {
        $paymentToDelete = Payment::find($id);
        if (empty($paymentToDelete)) {
            return response()->json(['error' => 'not found'], 404);
        }

        $paymentToDelete->delete();

        return response()->json([''], 200);
    }
}
