<?php

namespace App\Http\Controllers;

use App\Address;
use App\Junior;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class controller of the junior.
 * This class contains the methods that correspond to the CRUD functions.
 * extends controller.
 */
class JuniorController extends Controller
{

    /**
     * Display a listing of the junior
     *
     * @return void all the junior objects, with their linked user and address
     */
    public function index()
    {
        $userCo = Auth::user();

        if ($userCo->isInGroup('Junior') || $userCo->isInGroup('Senior')) {
            return response()->json(['error' => 'forbidden'], 403);
        }
        $juniors = \App\Junior::with('user.address')->get();

        return $juniors;
    }

    /**
     * Store a newly created Junior in storage.
     * contains all the datas from the client
     * Those datas must contain:
     * email, lastName, firstName, phone, password, birth, gender, 'street',
     * 'NPA', 'city', 'streetNumber',
     *
     * @param  \Illuminate\Http\Request $request :
     *
     * @return \Illuminate\Http\Response the newly created Junior, with its linked user and address
     */
    public function store(Request $request)
    {
        // Récupération des inputs pertinents
        if (!$request->has([
                'email',
                'lastName',
                'firstName',
                'phone',
                'password',
                'birth',
                'gender',
                'street',
                'NPA',
                'city',
                'streetNumber',
            ])
        ) {
            return response()->json(['error' => 'empty request'], 400);
        }

        $inputsUser                      = $request->only('email', 'lastName',
            'firstName', 'phone', 'password', 'birth', 'gender');
        $inputsAddress                   = $request->only('street',
            'streetNumber', 'NPA', 'city');
        $inputsjunior['isEmployed']      = 0;
        $inputsjunior['inscriptionStep'] = "1";

        $newUser = new User();
        if (!$newUser->validate($inputsUser)) {
            return response()->json(['error' => $newUser->errors()], 400);
        }

        $inputsUser['password'] = bcrypt($request->get('password'));
        DB::beginTransaction();
        try {
            $newUser = User::create($inputsUser);

            $inputsjunior['userId'] = $newUser->id;

            $newjunior = new Junior();

            if (!$newjunior->validate($inputsjunior)) {
                return response()->json(['error' => $newjunior->errors()], 400);
            }

            $newjunior = Junior::create($inputsjunior);

            $inputsAddress['userId'] = $newUser->id;
            $newAddress              = new Address();
            if (!$newAddress->validate($inputsAddress)) {
                return response()->json(['error' => $newAddress->errors()], 400);
            }
            Address::create($inputsAddress);

            $groupJuniorId = \App\Group::where('name', 'Junior')->first();

            $newUser->groups()->attach($groupJuniorId->id);

            DB::commit();
        } catch (\Exception $e) {

            // Si une erreur survient, on "annule" la creation
            DB::rollback();
            return response()->json(['error' => "error"], 400);
        }
        return \App\Junior::with('user.address', 'user.groups')->where('juniors.id',
                $newjunior->id)->first();
    }

    /**
     * Display the specified Junior.
     *
     * A Junior can only show its own infos.
     *
     * @param  int $id : the id of the Junior that is looked for.
     *
     * @return \Illuminate\Http\Response the Junior object with its linked user and address
     */
    public function show($id)
    {
        $junior = Junior::find($id);
        if (empty($junior)) {
            return response()->json(['error' => 'not found'], 404);
        }

        $userCo = Auth::user();

        if ($userCo->isInGroup('Junior')) {
            if ($userCo->id != $junior->userId) {
                return response()->json(['error' => 'forbidden'], 403);
            }
        }

        return $junior->with('user.address')->where('id', $junior->id)->first();
    }

    /**
     * update a Junior in storage and its linked user.
     *
     * contains all the datas from the client and the id of the Junior to update
     * Those datas must contain:
     * email, lastName, firstName, phone, password, birth, gender, street,
     * 'NPA', 'city', 'streetNumber'
     *
     * @param  \Illuminate\Http\Request $request , $id
     *
     * @return \Illuminate\Http\Response  the newly updated Junior, with its linked user and address
     */
    public function update(Request $request, $id)
    {
        $junior = \App\Junior::find($id);
        if (empty($junior)) {
            return response()->json(['error' => 'junior not found'], 404);
        }

        $userCo = Auth::user();
        if ($userCo->id != $junior->userId) {
            return response()->json(['error' => 'forbidden'], 403);
        }

        if (!$request->has([
                'email',
                'lastName',
                'firstName',
                'phone',
                'password',
                'birth',
                'gender',
                'street',
                'NPA',
                'city',
                'streetNumber',
            ])
        ) {
            return response()->json(['error' => 'empty request'], 400);
        }

        if (!$this->isUserCanAccessSpecialField($request)) {
            return response()->json('forbidden', 403);
        }

        $inputsUser    = $request->only('email', 'lastName', 'firstName',
            'phone', 'password', 'birth', 'gender');
        $inputsAddress = $request->only('street', 'streetNumber', 'NPA', 'city');

        if ($request->has('isEmployed')) {
            $inputJunior['isEmployed'] = $request->get('isEmployed');
        }
        if ($request->has('inscriptionStep')) {
            $inputJunior['inscriptionStep'] = $request->get('inscriptionStep');
        }

        $inputJunior['userId'] = $junior->userId;
        if (!$junior->validate($inputJunior)) {
            return response()->json(['error' => $junior->errors()], 400);
        }

        $user = \App\User::find($junior->userId);
        if (empty($user)) {
            return response()->json(['error' => 'user not found'], 404);
        }
        if (!$user->validate($inputsUser)) {
            return response()->json(['error' => $user->errors()], 400);
        }

        $address = \App\Address::where('userId', $junior->userId)->first();

        if (empty($address)) {
            return response()->json(['error' => 'address not found'], 404);
        }
        $inputsAddress['userId'] = $junior->userId;
        if (!$address->validate($inputsAddress)) {
            return response()->json(['error' => $address->errors()], 400);
        }

        DB::beginTransaction();
        try {
            $user->update($inputsUser);
            $address->update($inputsAddress);
            $junior->update($inputJunior);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e], 403);
        }
        return $junior->with('user.address')->where('id', $junior->id)->get();
    }

    /**
     * Remove the specified Junior from storage, and its linked user and address.
     *
     * @param  int $id : the id of the Junior to delete
     *
     * @return void a http code 200 if well deleted. Else a 400 bad request.
     */
    public function destroy($id)
    {
        $juniorToDelete = Junior::find($id);
        if (empty($juniorToDelete)) {
            return response()->json(['error' => 'user not fund']);
        }
        $userCo = Auth::user();
        if ($userCo->id != $juniorToDelete->userId) {
            return response()->json(['error' => 'forbidden'], 403);
        }

        $userToDelete    = User::find($juniorToDelete->userId);
        $addressToDelete = \App\Address::where('userId', $juniorToDelete->userId)->first();
        if (empty($userToDelete) || empty($addressToDelete)) {
            return response()->json(['error' => 'element missing to perform destroy']);
        }
        DB::beginTransaction();
        try {
            $juniorToDelete->delete();
            $userToDelete->delete();
            $addressToDelete->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 400);
        }
        return response()->json([''], 200);
    }

    private function isUserCanAccessSpecialField(Request $request)
    {
        $forbiddenFieldJunior = ['isEmployed', 'inscriptionStep'];
        $forbiddenFieldSenior = ['isEmployed', 'inscriptionStep'];

        $user = Auth::user();
        if ($user->isInGroup('Senior')) {
            foreach ($forbiddenFieldSenior as $field) {
                if ($request->has($field)) {
                    return false;
                }
            }
        }

        if ($user->isInGroup('Junior')) {
            foreach ($forbiddenFieldJunior as $field) {
                if ($request->has($field)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * listing of the skills  mastered by the junior
     * @param $juniorId
     */
    public function getMySkills()
    {
        $user   = Auth::user();
        $junior = Junior::where('userId', $user->id)->with('skillsJuniors')->first();
        if (empty($junior) || !$user->isInGroup('Junior')) {
            return response()->json(['error' => 'getMySkills : user not found']);
        }
        return $junior->skillsJuniors;
    }

    public function getByUserId($userId)
    {
        $user   = User::find($userId);
        $junior = Junior::where('userId', $userId)->first();
        if (empty($user) || empty($junior)) {
            return response()->json(['error, getByUserId' => 'wrong Id']);
        }
       return $junior;
    }

    public function addSkills(Request $request, $id)
    {
      
        $user   = Auth::user();
        $skills = $request->get('skills');
        
        $junior = Junior::where('userId', $user->id)->first();
        
        
            $junior->skillsJuniors()->sync($skills);
      
        return $junior;
    }
}