<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use \Illuminate\Http\Request;

/**
 * Class controller of the interventionPrice.
 * This class contains the methods that correspond to the CRUD functions.
 * extends controller.
 */
class PriceController extends Controller
{

    /**
     * Display a listing of all the intervention Prices
     * @return all the interventionPrice objects.
     * a Seniro can only get the last price in date.
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->isInGroup('Senior')) {
            return \App\InterventionPrice::orderBy('datePriceSet', 'desc')
                ->first();
        }
        return \App\InterventionPrice::all();
    }

    /**
     * Store a newly created interventionPrice in storage.
     *
     * contains all the datas from the client
     * Those datas must contain:
     * priceByHour, description
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response the newly created interventionPrice
     */
    public function store(Request $request)
    {
        if (!$request->has('priceByHour') || !$request->has('description')) {
            return response()->json(['error' => 'empty request'], 400);
        }

        $input = $request->only('priceByHour', 'description');
        $input['datePriceSet'] = date('Y-m-d H:i:s');

        $price = new \App\InterventionPrice();
        if (!$price->validate($input)) {
            return response()->json(['error' => $price->errors()], 400);
        }

        $price = \App\InterventionPrice::create($input);

        return $price;
    }

    /**
     * Display the specified interventionPrice.
     *
     * @param  int $id: the id of the interventionPrice that is looked for.
     * @return \Illuminate\Http\Response the interventionPrice object
     */
    public function show($id)
    {
        $user = Auth::user();
        if ($user->isInGroup('Senior')) {
            $actualPrice = \App\InterventionPrice::orderBy('datePriceSet',
                'desc')->first();
            if ($actualPrice->id != $id) {
                return response()->json(['access denied'], 403);
            }
        }
        $price = \App\InterventionPrice::find($id);
        return empty($price) ? response()->json(['error' => 'not found'], 404)
        : $price;
    }

    /**
     * contains all the datas from the client, and the id of the price to update
     *
     * @param \Illuminate\Http\Request $request, $id
     * @return void a json response, a price cannot be updated, only stored or displayed.
     */
    public function update(Request $request, $id)
    {
        return response()->json(['error' => 'can\'t update a price'], 400);
    }

    /**
     * the id of the price to delete
     *
     * @param int $id
     * @return void a json response, a price cannot be deleted, only stored or displayed.
     */
    public function destroy($id)
    {
        return response()->json(['error' => 'can\'t remove a price'], 400);
    }
}
