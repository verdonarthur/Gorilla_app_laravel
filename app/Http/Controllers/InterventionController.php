<?php

namespace App\Http\Controllers;

use App\Intervention;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * API Controller for intervention linked services.
 *
 * This controller manage a basic CRUD system.
 */
class InterventionController extends Controller
{
    /**
     * return all the intervention in DB in JSON format with the linked junior, senior, request
     * and interventionPrice
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $intervention = Intervention::with(['junior.user.address', 'senior.user.address',
            'request', 'interventionPrice'])->get();
        return $intervention;
    }

    /**
     * give an intervention in JSON format with the with the linked junior, senior, request
     * and interventionPrice by the ID asked
     *
     * @param  int $id
     * @return \App\Intervention
     */
    public function show($id)
    {
        $intervention = Intervention::find($id);
        if (empty($intervention)) {
            return response()->json(['error' => 'not found'], 404);
        }
        $intervention = Intervention::with(['junior.user.address', 'senior.user.address',
            'request', 'interventionPrice'])->where('interventions.id', $id)->first();

        return $intervention;
    }

    /**
     * Store a new intervention in the DB.
     *
     * The needed field are 'from', 'to', 'isComplete', 
     * 'seniorId', 'juniorId', 'requestId'
     *
     * The optional field are ratingJunior, ratingSenior, 
     * commentsRatingJunior, commentsRatingSenior, report
     * 
     * The created intervention is return in a JSON format.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Intervention
     */
    public function store(Request $request)
    {
        if (!$request->has(['from', 'to', 'isComplete', 'seniorId', 'juniorId', 'requestId'])) {
            return response()->json(['error' => 'empty request'], 400);
        }

        if (!$this->isUserCanAccessSpecialField($request)) {
            return response()->json('forbidden', 403);
        }

        $inputsIntervention = $request->only('from', 'to', 'isComplete', 'seniorId',
            'juniorId', 'requestId');

        if ($request->has('ratingJunior')) {
            $inputsIntervention['ratingJunior'] = $request->get('ratingJunior');
        }

        if ($request->has('ratingSenior')) {
            $inputsIntervention['ratingSenior'] = $request->get('ratingSenior');
        }

        if ($request->has('commentsRatingJunior') && !$request->has('ratingJunior')) {
            return response()->json(['error' => 'empty request'], 400);
        } elseif ($request->has('commentsRatingJunior')) {
            $inputsIntervention['commentsRatingJunior'] = $request->get('commentsRatingJunior');
        }

        if ($request->has('commentsRatingSenior') && !$request->has('ratingSenior')) {
            return response()->json(['error' => 'empty request'], 400);
        } elseif ($request->has('commentsRatingSenior')) {
            $inputsIntervention['commentsRatingSenior'] = $request->get('commentsRatingSenior');
        }

        if ($request->has('report')) {
            $inputsIntervention['report'] = $request->get('report');
        }

        /*
        * TODO : Optimize the calculation code below
        */
        $actualPrice = \App\InterventionPrice::orderBy('datePriceSet', 'desc')->first();
        $inputsIntervention['interventionPriceId'] = $actualPrice->id;
        $priceByHour = $actualPrice->priceByHour;
        $from = strtotime($request->get('from'));
        $to = strtotime($request->get('to'));
        $hours = ($to - $from) / 60 / 60;

        if ($hours < 0) {
            return response()->json(['error' => "Your intervention cannot have a starting hour later than an end hour."],
                400);
        }
        $totalPrice = $hours * $priceByHour;

        $inputsIntervention['interventionTotalPrice'] = $totalPrice;

        $inputsIntervention['isValidatedBySenior'] = false;

        $intervention = new Intervention();
        if (!$intervention->validate($inputsIntervention)) {
            return response()->json(['error' => $intervention->errors()], 400);
        }

        try {
            $intervention = Intervention::create($inputsIntervention);
        } catch (\Exception $e) {
            return response()->json(['error' => "error during save", 'description' => $e->getMessage()],
                400);
        }

        return Intervention::with(['junior.user.address', 'senior.user.address',
            'request', 'interventionPrice'])->where('interventions.id',
            $intervention->id)->first();
    }

    /**
     * Update an intervention in the DB.
     *
     * The needed field are 'from', 'to', 'isComplete', 
     * 'isValidatedBySenior', 'seniorId', 'juniorId', 'requestId'
     *
     * The optional field are ratingJunior, ratingSenior, 
     * commentsRatingJunior, commentsRatingSenior, report
     * 
     * The created intervention is return in a JSON format.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $id
     * @return \App\Intervention
     */
    public function update(Request $request, $id)
    {
        if (!$request->has(['from', 'to', 'isComplete', 'isValidatedBySenior', 'seniorId',
            'juniorId', 'requestId'])) {
            return response()->json(['error' => 'empty request'], 400);
        }

        if (!$this->isUserCanAccessSpecialField($request)) {
            return response()->json('forbidden', 403);
        }
        $inputsIntervention = $request->only('from', 'to', 'isComplete',
            'isValidatedBySenior', 'seniorId', 'juniorId', 'requestId');

        if ($request->has('ratingJunior')) {
            $inputsIntervention['ratingJunior'] = $request->get('ratingJunior');
        }

        if ($request->has('ratingSenior')) {
            $inputsIntervention['ratingSenior'] = $request->get('ratingSenior');
        }

        if ($request->has('commentsRatingJunior') && !$request->has('ratingJunior')) {
            return response()->json(['error' => 'cannot have a comment on rating without rating'],
                400);
        } elseif ($request->has('commentsRatingJunior')) {
            $inputsIntervention['commentsRatingJunior'] = $request->get('commentsRatingJunior');
        }
        
        if ($request->has('commentsRatingSenior') && !$request->has('ratingSenior')) {
            return response()->json(['error' => 'cannot have a comment on rating without rating'],
                400);
        } elseif ($request->has('commentsRatingSenior')) {
            $inputsIntervention['commentsRatingSenior'] = $request->get('commentsRatingSenior');
        }

        if ($request->has('report')) {
            $inputsIntervention['report'] = $request->get('report');
        }
        
        /*
        * TODO : Optimize the calculation code below
        */
        $actualPrice = \App\InterventionPrice::orderBy('datePriceSet',
            'desc')->first();
        $inputsIntervention['interventionPriceId'] = $actualPrice->id;
        $priceByHour = $actualPrice->priceByHour;
        $from = strtotime($request->get('from'));
        $to = strtotime($request->get('to'));
        $hours = ($to - $from) / 60 / 60;

        if ($hours < 0) {
            return response()->json(['error' => "Your intervention cannot have a starting hour later than an end hour."],
                400);
        }
        $totalPrice = $hours * $priceByHour;

        $inputsIntervention['interventionTotalPrice'] = $totalPrice;

        $intervention = Intervention::find($id);

        if (empty($intervention)) {
            return response()->json(['error' => 'not found'], 404);
        }

        if (!$intervention->validate($inputsIntervention)) {
            return response()->json(['error' => $intervention->errors()], 400);
        }

        try {
            Intervention::find($id)->update($inputsIntervention);
        } catch (\Exception $e) {

            return response()->json(['error' => "error during save", 'description' => $e->getMessage()],
                400);
        }

        return self::show($id);
    }

    /**
     * Delete an intervention by his ID
     *
     * @param  int $id
     * @return \App\Intervention
     */
    public function destroy($id)
    {
        $intervention = Intervention::find($id);
        if (empty($intervention)) {
            return response()->json(['error' => 'not found'], 404);
        }

        $intervention->delete();

        return response()->json([''], 200);
    }

    private function isUserCanAccessSpecialField(Request $request)
    {
        $forbiddenFieldJunior = [
            'ratingSenior', 
            'commentsRatingSenior', 
            'interventionPriceId', 
            'isValidatedBySenior', 
            'interventionTotalPrice'
        ];
        $forbiddenFieldSenior = [
            'ratingJunior', 
            'commentsRatingJunior', 
            'interventionPriceId', 
            'interventionTotalPrice'
        ];

        $user = Auth::user();
        if ($user->isInGroup('Senior')) {
            foreach ($forbiddenFieldSenior as $field) {
                if ($request->has($field)) {
                    return false;
                }

            }
        }

        if ($user->isInGroup('Junior')) {
            foreach ($forbiddenFieldJunior as $field) {
                if ($request->has($field)) {
                    return false;
                }

            }
        }

        return true;
    }
}
