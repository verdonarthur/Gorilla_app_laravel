<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Illuminate\Http\Request;

class RequestController extends Controller
{
    /**
     * listing the requests
     * juniors and seniors can only access to their own requests
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->isInGroup('Junior')) {
            $request = \App\Request::with([
                'junior.user.address',
                'senior.user.address',
                'request',
                'skillsJuniors',
            ])->where('juniorId',
                $user->junior()->first()->id)->get();
            return $request;
        }
        if ($user->isInGroup('Senior')) {
            $request = \App\Request::with([
                'junior.user.address',
                'senior.user.address',
                'request',
                'skillsJuniors',
            ])->where('seniorId',
                $user->senior()->first()->id)->get();
            return $request;
        }
        $request = \App\Request::with([
            'junior.user.address',
            'senior.user.address',
            'request',
            'skillsJuniors',
        ])->get();
        return $request;
    }

    /**
     * retrieve a request
     * juniors and seniors can only access to their own requests
     *
     * @param int $id
     *
     * @return void the request asked for with informations about : the junior concerned, the senior concerned, the skills concerned
     */
    public function show($id)
    {
        $request = \App\Request::find($id)->load([
            'junior.user.address',
            'senior.user.address',
            'request',
            'skillsJuniors',
        ]);
        $user = Auth::user();
        if ($user->isInGroup('Junior')) {
            if ($request->junior()->first()->user()->first()->id != $user->id) {
                return response()->json(['access denied'], 403);
            }
        }
        if ($user->isInGroup('Senior')) {

            if ($request->senior()->first()->user()->first()->id != $user->id) {
                return response()->json(['access denied'], 403);
            }
        }
        return empty($request) ? response()->json(['error' => 'not found'], 404) : $request;
    }

    /**
     * Store a new Request in the DB.
     *
     * The needed field are 'description', 'state', 'startDate', 'endDate', 'type', 'seniorId'
     * optionnal: 'juniorId','requestId'
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void the request just created
     */
    public function store(Request $request)
    {
        $input = $this->requestToValidInput($request);
        if (is_null($input)) {
            return response()->json(['error' => 'empty request'], 400);
        }

        if ($request->has('skillsJuniorIds')) {
            $skillsIds = $request->get('skillsJuniorIds');
        }

        $req = new \App\Request();
        if (!$req->validate($input)) {
            return response()->json(['error' => $req->errors()], 400);
        }

        DB::beginTransaction();
        try {
            /*if ($input['type'] === 'multiple') {
            $startDate = new Carbon($input['startDate']);
            $endDate =  new Carbon($input['endDate']);
            dd($startDate);
            $req = \App\Request::create($input);

            } else {*/

            $req = \App\Request::create($input);

            if (!empty($skillsIds)) {
                $req->skillsJuniors()->sync($skillsIds);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => "error during save",
                'description' => $e->getMessage(),
            ], 400);
        }

        return $req;
    }

    /**
     * Store a new Request in the DB.
     *
     * The needed field are : 'description', 'state', 'startDate', 'endDate', 'type', 'seniorId'
     *
     * @id: id of the request
     * optionnal: 'juniorId','requestId'
     *
     * juniors can only changed 'state'
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void the request just updated
     */
    public function update(Request $request, $id)
    {

        $req = \App\Request::find($id);
        if (empty($req)) {
            return response()->json(['error' => 'not found'], 404);
        }
        $user = Auth::user();
        if ($user->isInGroup('Junior')) {
            if ($req->junior()->first()->user()->first()->id != $user->id) {
                return response()->json(['access denied'], 403);
            }
            $state = $request['state'];
            $request = array_replace($request->toArray(), $req->toArray());
            if (empty($request['requestId'])) {
                unset($request['requestId']);
            }
            $request['state'] = $state;
            $request = new \Illuminate\Http\Request($request);
        }

        $input = $this->requestToValidInput($request);
        if (is_null($input)) {
            return response()->json(['error' => 'empty request'], 400);
        }

        if ($request->has('skillsJuniorIds')) {
            $skillsIds = $request->get('skillsJuniorIds');
        }

        if (!$req->validate($input)) {
            if (!empty($skillsIds)) {
                $req->skillsJuniors()->sync($skillsIds);
                return \App\Request::find($id);
            }
            return response()->json(['error' => $req->errors()], 400);
        }

        DB::beginTransaction();
        try {
            \App\Request::find($id)->update($input);
            if (!empty($skillsIds)) {
                $req->skillsJuniors()->sync($skillsIds);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => "error during save",
                'description' => $e->getMessage(),
            ], 400);
        }

        return \App\Request::find($id);

    }

    /**
     * soft deleting a request
     *
     * @param int $id
     *
     * @return void http 200
     */
    public function destroy($id)
    {
        $req = \App\Request::find($id);
        if (empty($req)) {
            return response()->json(['error' => 'not found'], 404);
        }

        $req->delete();

        return response()->json([''], 200);
    }

    private function requestToValidInput(Request $request)
    {
        if (!$request->has('description', 'state', 'startDate', 'endDate',
            'type')
        ) {
            return null;
        }

        $input = $request->only('description', 'state', 'startDate', 'endDate',
            'type');

        $user = Auth::user();

        if (!$user->isInGroup('Senior') && !$request->has('seniorId')) {
            return null;
        }

        if (!$request->has('seniorId')) {
            $input['seniorId'] = $user->senior()->first()->id;
        }else{
            $input['seniorId'] = $request->get('seniorId');
        }

        if ($request->has('juniorId')) {
            $input['juniorId'] = $request->get('juniorId');
        }
        if ($request->has('requestId') && $request->get('requestId')!=null) {
            $input['requestId'] = $request->get('requestId');
        }
        return $input;
    }

}
