<?php

namespace App\Http\Controllers;

use App\Address;
use App\AvailabilityJunior;
use App\Junior;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AvailabilityController extends Controller
{
    /**
     * listing of the availabilities
     * juniors can only consult their own availabilities
     *
     * @return \Illuminate\Http\JsonResponse the availabilities with informations about : the junior concerned, the address concerned
     */
    public function index()
    {
        $userCo = Auth::user();

        if ($userCo->isInGroup('Senior')) {
            return response()->json(['error' => 'forbidden'], 403);
        }
        //Si le junior logged demande à voir toutes ces dispos, il verra uniquement les siennes
        if ($userCo->isInGroup('Junior')) {
            $junior = Junior::where('userId', $userCo->id)->first();
            $availabilitiesJunior = AvailabilityJunior::with('junior',
                'address')->where('juniorId',
                $junior->id)->get();

            if ($availabilitiesJunior->count() == 0) {
                return response()->json('You do not have any availability registered');
            }
            return $availabilitiesJunior;
        }

        $availabilities = AvailabilityJunior::with('junior', 'address')->get();
        return $availabilities;
    }

    /**
     * retrieving an availability
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse the availability asked for
     */
    public function show($id)
    {
        $userCo = Auth::user();

        if ($userCo->isInGroup('Senior')) {
            return response()->json(['error' => 'forbidden'], 403);
        }

        $availability = AvailabilityJunior::find($id);
        if (empty($availability)) {
            return response()->json(['error' => 'availability inconnue dans la BD'],
                403);
        }

        //Si le junior logged demande à voir une dispo d un autre junior, il est arreté
        if ($userCo->isInGroup('Junior')) {
            $junior = Junior::where('userId', $userCo->id)->first();
            if ($availability->juniorId != $junior->id) {
                return response()->json(['error' => 'You cannot access availability of another junior'],
                    403);
            }
        }

        return $availability->with('junior', 'address')
            ->where('id', $availability->id)->get();
    }

    /**
     * inserting a new availability
     * 
     * @param \App\Request needed : 'from','to','day','juniorId','addressId'
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $userCo = Auth::user();

        //les senior n ont aucun droit sur la creation d une dispo
        if ($userCo->isInGroup('Senior') || $userCo->isInGroup('Sec. Mem.')) {
            return response()->json(['error' => 'forbidden'], 403);
        }

        if (!$request->has([
            'from',
            'to',
            'day',
            'juniorId',
            'addressId',
        ])
        ) {
            return response()->json(['error' => 'empty request'], 400);
        }

        $juniorConcerned = Junior::find($request['juniorId']);
        if (empty($juniorConcerned)) {
            return response()->json(['error' => 'junior innexistant'], 400);
        }
        $addressConcerned = Address::find($request['addressId']);
        if (empty($addressConcerned)) {
            return response()->json(['error' => 'adresse innexistante'], 400);
        }

        //si le junior tente dentrer les dispo d un autre junior
        if ($userCo->isInGroup('Junior')) {
            if ($juniorConcerned->userId != $userCo->id) {
                            return response()->json(['error' => 'forbidden'], 403);
            }
        }

        $inputsAvailability = $request->only('from', 'to', 'day', 'juniorId',
            'addressId');


        $availability = new AvailabilityJunior();
        if (!$availability->validate($inputsAvailability)) {
            return response()->json(['error' => $availability->errors()]);
        }

        DB::beginTransaction();
        try {
            $availability = AvailabilityJunior::create($inputsAvailability);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e]);
        }

        return $availability->with('address', 'junior')
            ->where('id', $availability->id)->get();
    }

    /**
     * updating an availability
     *
     * @param \App\Request needed : 'from','to','day','juniorId','addressId'
     * @param int $id
     * @return \Illuminate\Http\JsonResponse the availability just updated with informations about: the junior concerned, the address concerned
     */
    public function update(Request $request, $id)
    {

        $userCo = Auth::user();

        //les senior et les membres du secretariat n ont aucun droit sur la modification d une dispo
        if ($userCo->isInGroup('Senior') || $userCo->isInGroup('Sec. Mem.')) {
            return response()->json(['error' => 'forbidden'], 403);
        }

        $availabilityToUpdate = AvailabilityJunior::find($id);
        if (empty($availabilityToUpdate)) {
            return response()->json(['error' => "Availability introuvable"]);
        }
        if (!$request->has([
            'from',
            'to',
            'day',
            'juniorId',
            'addressId',
        ])
        ) {
            return response()->json(['error' => 'empty request'], 400);
        }

        $juniorConcerned = Junior::find($request['juniorId']);
        if (empty($juniorConcerned)) {
            return response()->json(['error' => 'junior innexistant'], 400);
        }
        $addressConcerned = Address::find($request['addressId']);
        if (empty($addressConcerned)) {
            return response()->json(['error' => 'adresse innexistante'], 400);
        }
        //si le junior tente de modifier les dispo d un autre junior
        if ($userCo->isInGroup('Junior')) {
            if ($juniorConcerned->userId != $userCo->id) {
                return response()->json(['error' => 'forbidden'], 403);
            }
        }

        $inputsAvailability = $request->only('from', 'to', 'day', 'juniorId',
            'addressId');

        if (!$availabilityToUpdate->validate($inputsAvailability)) {
            return response()->json(['error' => $availabilityToUpdate->errors()],
                400);
        }

        DB::beginTransaction();
        try {
            $availabilityToUpdate->update($inputsAvailability);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e], 403);
        }
        return $availabilityToUpdate->with('junior', 'address')
            ->where('id', $availabilityToUpdate->id)->get();
    }

    /**
     * soft deleting an availability
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse http 200
     */
    public function destroy($id)
    {
        $userCo = Auth::user();

        if ($userCo->isInGroup('Senior') || $userCo->isInGroup('Sec. Mem.')) {
            return response()->json(['error' => 'forbidden'], 403);
        }

        $availabilityToDestroy = AvailabilityJunior::find($id);
        if (empty($availabilityToDestroy)) {
            return response()->json(['error' => 'availability introuvable']);
        }

        $junior = Junior::find($availabilityToDestroy->juniorId);
        //si le junior tente de modifier les dispo d un autre junior
        if ($userCo->isInGroup('Junior')) {

            if ($junior->userId != $userCo->id) {
                return response()->json(['error' => 'forbidden'], 403);
            }
        }


        DB::beginTransaction();
        try {
            $availabilityToDestroy->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e]);
        }
        return response()->json([''], 200);
    }
}